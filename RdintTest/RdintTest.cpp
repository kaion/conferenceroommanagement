// RdintTest.cpp : 定義主控台應用程式的進入點。
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../RDINT/RDINT_DEF.h"
#include "../RDINT/RDINT.h"


uint8_t g_comport = 3;

int main(int32_t argc, char *argv[])
{
    uint8_t uid[11], atqa[2], sak, p_data[16 * 3] = {0}, key[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
    int32_t ret = 0;
    //char *user_mail = "kenny@program.com.tw";
    char *user_mail = "kenny@";
    int32_t len = strlen(user_mail), pos = 0;
    int32_t i = 0;

    if (argc > 1)
    {
        g_comport = atoi(argv[1]);
    }
    //ret = RDINT_NFC_OpenReader(g_comport, 115200, NULL, 0, 0, NULL, 0x01);
    //ret = RDINTsys_OpenReader(g_comport, 19200, NULL, 1, 0, NULL);
    ret = RDINTsys_OpenReader(g_comport, 115200, NULL, 1, 0, NULL);
    if (ret != 0)
    {
        printf("open reader error.\n");
        return 1;
    }

    {
        RDINT_DeviceBlinkControl(g_comport, GREEN_ON | SOUND_ON, 0, 2, 0, 2);
        return 0;
    }

    do {
        ret = RDINT_GetUID(g_comport, uid);
        //ret = RDINT_OpenCard(g_comport, 1, uid, atqa, &sak);
        printf("%d\n", ret);
    } while (ret != 0);
    ret = RDINT_OpenCard(g_comport, 1, uid, atqa, &sak);

    for (i = 0; i < 3; i++)
    {
        ret = RDINT_WriteMifareOneBlock(g_comport, 0, 0, 0, 4 + i, key, p_data);
        if (ret != 0)
        {
            return 0;
        }
    }

    i = 0;
#if 0
    while (len >= pos)
    {
        if (len < (pos + 16))
        {
            memset(p_data, 0, 16);
            memcpy(p_data, user_mail + pos, len - pos);
            ret = RDINT_WriteMifareOneBlock(g_comport, 0, 0, 0, 4 + i, key, p_data);
            if (ret != 0)
            {
                return 0;
            }
            break;
        }

        memset(p_data, 0, 16);
        memcpy(p_data, user_mail + pos, 16);
        ret = RDINT_WriteMifareOneBlock(g_comport, 0, 0, 0, 4 + i, key, p_data);
        if (ret != 0)
        {
            return 0;
        }
        pos += 16;
        i++;
    }
#else
    while (len >= pos)
    {        
        memset(p_data, 0, 16);
        memcpy(p_data, user_mail + pos, (len < (pos + 16)) ? (len - pos) : 16);
        ret = RDINT_WriteMifareOneBlock(g_comport, 0, 0, 0, 4 + i, key, p_data);
        if (ret != 0)
        {
            return 0;
        }
        pos += 16;
        i++;
    }
#endif
    memset(p_data, 0, sizeof(p_data));
    for (i = 0; i < 3; i++)
    {
        ret = RDINT_ReadMifareOneBlock(g_comport, 0, 0, 0, 4 + i, key, p_data + i * 16);
        if (ret != 0)
        {
            return 0;
        }
    }

    ret = RDINTsys_CloseReader(g_comport);
    return 0;
}

