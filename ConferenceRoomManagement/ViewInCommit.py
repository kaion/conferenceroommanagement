# encoding: utf-8

import sys

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import datetime
import time
import tkMessageBox as MessageBox
from threading import Timer
import gc

import ViewBooking
import ViewConfirm as MsgBox
import Model_GoogleApi as Google
import Model_Setting as Setting
import Model_RDINT as RDINT
import Model_Gpio as Gpio
import Model_Log as Log


def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = ViewInCommit (root)
    root.mainloop()

w = None
def createViewInCommit(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = ViewInCommit (w)
    return (w, top)

def destroyViewInCommit():
    global w
    w.destroy()
    w = None


class ViewInCommit:
    def __init__(self, top=None):
        ###self.parent = top.nametowidget(top.winfo_parent())
        self.root = top
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = 'red'  # X11 color: 'gray85'
        _fgcolor = 'white'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font14 = "-family {Microsoft JhengHei UI} -size 14 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font16 = "-family {Microsoft JhengHei UI} -size 16 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font12 = "-family {Microsoft JhengHei UI} -size 12 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font10 = "-family {Microsoft JhengHei UI} -size 9 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font9 = "-family {Microsoft JhengHei UI} -size 9 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        font20 = "-family {Microsoft JhengHei UI} -size 20 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        #font36_btn = tkFont.Font(family='Microsoft JhengHei UI', size=36, weight='bold')
        font36 = "-family {Microsoft JhengHei UI} -size 36 -weight bold"

        self.style_btn = ttk.Style()
        self.style_btn.configure('my.TButton', font=('Microsoft JhengHei UI', 36, 'bold'))

        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("800x480+551+200")
        top.title("會議室管理(會議中)")
        top.configure(background=_bgcolor)
        top.configure(highlightbackground=_fgcolor)
        top.configure(highlightcolor="black")

        #top.option_add('*Dialog.msg.font', font36)



        self.TPanedwindow1 = ttk.Panedwindow(top, orient="vertical")
        self.TPanedwindow1.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0)

        self.TPanedwindow1_p1 = ttk.Frame(height=130)
        self.TPanedwindow1.add(self.TPanedwindow1_p1)
        self.TPanedwindow1_p2 = ttk.Frame()
        self.TPanedwindow1.add(self.TPanedwindow1_p2)
        #self.__funcid0 = self.TPanedwindow1.bind('<Map>', self.__adjust_sash0)

        self.lblDateTime = ttk.Label(self.TPanedwindow1_p1)
        self.lblDateTime.place(relx=0.80, rely=0.08, height=25, width=150, y=-12)

        self.lblDateTime.configure(background=_bgcolor)
        self.lblDateTime.configure(foreground=_fgcolor)
        self.lblDateTime.configure(font=font16)
        self.lblDateTime.configure(relief=FLAT)
        self.lblDateTime.configure(anchor=NE)
        self.lblDateTime.configure(text='''Date Time''')

        self.btnBefore = Label(self.TPanedwindow1_p1)
        self.btnBefore.bind('<Button-1>', lambda e: self.btnBefore_Click())
        self.btnBefore.place(relx=0.33, rely=0.54, height=65, width=80, y=-14)
        self.btnBefore.configure(takefocus="")
        self.btnBefore.configure(text=u'<<')
        self.btnBefore['font'] = font36
        self.btnBefore['bg'] = _bgcolor
        self.btnBefore['fg'] = _fgcolor
        self.btnBefore['borderwidth'] = 0

        self.btnAfter = Label(self.TPanedwindow1_p1)
        self.btnAfter.bind('<Button-1>', lambda e: self.btnAfter_Click())
        self.btnAfter.place(relx=0.56, rely=0.54, height=65, width=80, y=-14)
        self.btnAfter.configure(takefocus="")
        self.btnAfter.configure(text=u'>>')
        self.btnAfter['font'] = font36
        self.btnAfter['bg'] = _bgcolor
        self.btnAfter['fg'] = _fgcolor
        self.btnAfter['borderwidth'] = 0

        self.btnToday = Label(self.TPanedwindow1_p1)
        self.btnToday.bind('<Button-1>', lambda e: self.btnToday_Click())
        self.btnToday.place(relx=0.43, rely=0.54, height=65, width=108, y=-14)
        self.btnToday.configure(takefocus="")
        self.btnToday.configure(text=u'今日')
        self.btnToday['font'] = font36
        self.btnToday['bg'] = _bgcolor
        self.btnToday['fg'] = _fgcolor
        self.btnToday['borderwidth'] = 0

        '''
        #self.btnBefore = ttk.Button(self.TPanedwindow1_p1, command=self.showViewBooking)
        self.btnBefore = Button(self.TPanedwindow1_p1, command=self.btnBefore_Click)
        self.btnBefore.place(relx=0.29, rely=0.54, height=65, width=80, y=-14)
        self.btnBefore.configure(takefocus="")
        self.btnBefore.configure(text=u'<<')
        #self.btnBefore['style'] = 'my.TButton'
        self.btnBefore['font'] = font36
        self.btnBefore['bg'] = _bgcolor
        self.btnBefore['fg'] = _fgcolor
        self.btnBefore['borderwidth'] = 0

        #self.btnAfter = ttk.Button(self.TPanedwindow1_p1)
        self.btnAfter = Button(self.TPanedwindow1_p1, command=self.btnAfter_Click)
        self.btnAfter.place(relx=0.52, rely=0.54, height=65, width=80, y=-14)
        self.btnAfter.configure(takefocus="")
        self.btnAfter.configure(text=u'>>')
        #self.btnAfter['style'] = 'my.TButton'
        self.btnAfter['font'] = font36
        self.btnAfter['bg'] = _bgcolor
        self.btnAfter['fg'] = _fgcolor
        self.btnAfter['borderwidth'] = 0

        #self.btnToday = ttk.Button(self.TPanedwindow1_p1)
        self.btnToday = Button(self.TPanedwindow1_p1, command=self.btnToday_Click)
        self.btnToday.place(relx=0.39, rely=0.54, height=65, width=108, y=-14)
        self.btnToday.configure(takefocus="")
        self.btnToday.configure(text=u'今日')
        #self.btnToday['style'] = 'my.TButton'
        self.btnToday['font'] = font36
        self.btnToday['bg'] = _bgcolor
        self.btnToday['fg'] = _fgcolor
        self.btnToday['borderwidth'] = 0
        '''

        self.lblRoomName = ttk.Label(self.TPanedwindow1_p1)
        self.lblRoomName.place(relx=0.33, rely=0.10, height=49, width=269, y=-12)

        self.lblRoomName.configure(background=_bgcolor)
        self.lblRoomName.configure(foreground=_fgcolor)
        self.lblRoomName.configure(font=font20)
        self.lblRoomName.configure(relief=FLAT)
        self.lblRoomName.configure(anchor=CENTER)
        self.lblRoomName.configure(text='''   ''')
        '''
        self.lstBooking = Listbox(self.TPanedwindow1_p2, borderwidth=0)
        self.lstBooking.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0, y=-0, h=0)
        #self.lstBooking.place(relx=0.0, rely=0.0, relheight=0.91, relwidth=1.0, y=-12, h=12)
        self.lstBooking.configure(background="black")
        self.lstBooking.configure(disabledforeground="#a3a3a3")
        self.lstBooking.configure(font=font36)
        self.lstBooking.configure(foreground="white")
        self.lstBooking.configure(highlightbackground="#d9d9d9")
        self.lstBooking.configure(highlightcolor="black")
        self.lstBooking.configure(selectbackground="black")
        self.lstBooking.configure(selectforeground="white")
        self.lstBooking.configure(selectborderwidth=0)
        self.lstBooking.configure(width=800)
        self.lstBooking.bind('<B1-Motion>', lambda e, s=self: s.lstBooking_Select(e.y))
        self.lstBooking.bind('<Button-1>', lambda e, s=self: s.lstBooking_Select(e.y))
        '''
        self.lstBooking = Label(self.TPanedwindow1_p2)
        self.lstBooking.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0, y=-0, h=0)
        self.lstBooking.configure(background="black")
        self.lstBooking.configure(font=font36)
        self.lstBooking.configure(foreground="white")
        self.lstBooking.configure(relief=FLAT)
        self.lstBooking.configure(anchor=NW)
        self.lstBooking.configure(justify=LEFT)
        self.lstBooking.configure(width=800)

        '''
        #self.btnFinish = ttk.Button(self.TPanedwindow1_p2, command = self.btnFinish_Click)
        self.btnFinish = Button(self.TPanedwindow1_p2, command = self.btnFinish_Click)
        self.btnFinish.place(relx=0.0, rely=0.66, height=130, width=400, y=-12)
        self.btnFinish.configure(takefocus="")
        self.btnFinish.configure(text=u'結束')
        #self.btnFinish['style'] = 'my.TButton'
        self.btnFinish['font'] = font36
        self.btnFinish['bg'] = _bgcolor
        self.btnFinish['fg'] = _fgcolor
        self.btnFinish['borderwidth'] = 0

        #self.btnExtend = ttk.Button(self.TPanedwindow1_p2, command = self.btnExtend_Click)
        self.btnExtend = Button(self.TPanedwindow1_p2, command = self.btnExtend_Click)
        self.btnExtend.place(relx=0.50, rely=0.66, height=130, width=400, y=-12)
        self.btnExtend.configure(takefocus="")
        self.btnExtend.configure(text=u'再延10分鐘')
        #self.btnExtend['style'] = 'my.TButton'
        self.btnExtend['font'] = font36
        self.btnExtend['bg'] = _bgcolor
        self.btnExtend['fg'] = _fgcolor
        self.btnExtend['borderwidth'] = 0
        '''
        self.btnFinish = Label(self.TPanedwindow1_p2)
        self.btnFinish.bind('<Button-1>', lambda e: self.btnFinish_Click())
        self.btnFinish.place(relx=0.0, rely=0.66, height=130, width=400, y=-12)
        self.btnFinish.configure(takefocus="")
        self.btnFinish.configure(text=u'結束')
        self.btnFinish['font'] = font36
        self.btnFinish['bg'] = _bgcolor
        self.btnFinish['fg'] = _fgcolor
        self.btnFinish['borderwidth'] = 0

        self.btnExtend = Label(self.TPanedwindow1_p2)
        self.btnExtend.bind('<Button-1>', lambda e: self.btnExtend_Click())
        self.btnExtend.place(relx=0.50, rely=0.66, height=130, width=400, y=-12)
        self.btnExtend.configure(takefocus="")
        self.btnExtend.configure(text=u'再延10分鐘')
        self.btnExtend['font'] = font36
        self.btnExtend['bg'] = _bgcolor
        self.btnExtend['fg'] = _fgcolor
        self.btnExtend['borderwidth'] = 0

        self.m_calendar = None
        try:
            self.m_calendar = Google.GoogleCalendar()
        except:
            ViewBooking.ViewBooking(self.root)
            return

        self.m_dir = None
        try:
            self.m_dir = Google.GoogleDirectory()
        except:
            self.m_dir = None
            ViewBooking.ViewBooking(self.root)
            return

        self.job_id = None

        self.attendees = None
        self.extend_time = 0
        self.now_event = None
        self.m_alarm_stop = True

        self.m_day_sel = 0
        self.m_day_sel_delay = 0
        self.m_day_sel_event = None

        self.m_coming_delay = 0

        #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.gmtime())
        #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.localtime())
        
        self.lblDateTime['text'] = ''
        self.showViewBooking()
        
        '''
        '%02d-%02d %02d:%02d:%02d' % (
            time.tm_year, time.tm_mon, time.tm_mday, hour, min, sec)
        '''

        #self.job_id = self.root.after(ms=1000, func=self.showViewBooking)



    def __adjust_sash0(self, event):
        paned = event.widget
        pos = [130, ]
        i = 0
        for sash in pos:
            paned.sashpos(i, sash)
            i += 1
        paned.unbind(u'<map>', self.__funcid0)
        del self.__funcid0

    def showViewBooking(self):
        data = []
        user_mail = u''
        user_name = u''
        
        v = RDINT.openCard()
        if v[u'ret'] == 0:
            i = 0
            while i < 3:
                v = RDINT.readMifareOneBlock(Setting.MifareKeyType, 4 + i, Setting.MifareKey)
                if v[u'ret'] != 0:
                    break
                data.extend(v[u'data'])
                i += 1
            i = 0
            for d in data:
                if d == 0:
                    break
                i += 1

            user_name = u''.join(map(chr, data[:i]))
            user_mail = user_name + Setting.AccountExtension
            Log.error(u'user name: ' + user_name[:-1])
            if not (self.attendees is None):
                successed = False
                for attendee in self.attendees:
                    if user_mail == attendee[u'email']:
                        Gpio.set(Setting.Gpio_LongTermDevices, True)
                        RDINT.controlDeviceBlink(RDINT.GREEN_ON | RDINT.SOUND_ON, 0, 2, 0, 2)
                        Log.error(u'account: ' +  user_mail)

                        self.lblRoomName[u'text'] = user_name[:-1] + u' 請進入'
                        for user in self.m_users:
                            if user[u'primaryEmail'] == user_mail:
                                self.lblRoomName[u'text'] = user[u'name'][u'fullName'] + u' 請進入'
                                Log.error(self.lblRoomName[u'text'])
                                self.m_coming_delay = 3
                                break

                        successed = True
                        break

                if successed == False:
                    RDINT.controlDeviceBlink(RDINT.RED_ON | RDINT.SOUND_ON, 5, 0, 0, 5)
            else:
                RDINT.controlDeviceBlink(RDINT.RED_ON | RDINT.SOUND_ON, 5, 0, 0, 5)

        if self.m_coming_delay == 0:
            self.lblRoomName[u'text'] = u'   '
        else:
            self.m_coming_delay -= 1

        if self.m_day_sel_delay > 0:
            self.m_day_sel_delay -= 1
            if self.m_day_sel_delay == 1:
                self.lblDateTime[u'text'] = u''
                self.m_day_sel = 0

        tm = time.localtime()
        if self.lblDateTime[u'text'] != u'':
            self.lblDateTime[u'text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime[u'text'] = time.strftime(u'%m/%d %H:%M', time.localtime())
            if time.localtime().tm_sec > 0:
                self.job_id = self.root.after(ms=1000, func=self.showViewBooking)
                return
        else:
            self.lblDateTime[u'text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime[u'text'] = time.strftime(u'%m/%d %H:%M', time.localtime())

        gc.collect()

        self.attendees = None

        #self.lstBooking.delete(0, END)
        self.lstBooking[u'text'] = u''
        try:
            self.m_users = self.m_dir.getUsers()
            self.m_events = self.m_calendar.getCalendars(Setting.CalendarId)
        except:
            ViewBooking.ViewBooking(self.root)
            return

        '''
        room_name = self.m_events[u'summary'].split('-')
        self.lblRoomName[u'text'] = room_name[-1]
        '''

        events = []
        for event in self.m_events[u'items']:
            sst = None
            if u'date' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if sst.date() != datetime.datetime.now().date():
                continue

            t = None; i = 0
            for evt in events:
                if u'date' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')
                
                if t > sst:
                    break
                i += 1
                
            events.insert(i, event)
        
        for event in events:
            if u'summary' in event.keys():
                Log.error(event[u'summary'])
            else:
                Log.error(u'無主題')

            st = None; et = None; user_name = ""
            if u'date' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if u'date' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            now = datetime.datetime.now()
            if now < st:
                continue
                #pass
                #ViewBooking.ViewBooking(self.root)
                #return
            eet = et
            if self.extend_time > 0:
                eet += datetime.timedelta(minutes = self.extend_time)

            if now > eet:
                continue

            if self.now_event != event:
                Gpio.set(Setting.Gpio_LongTermDevices, False)
                Gpio.stopMusic()
                self.m_alarm_stop = False
                self.now_event = event


            for user in self.m_users:
                if user[u'primaryEmail'] == event[u'creator'][u'email']:
                    user_name = user[u'name'][u'fullName']
                    break

            summary = u''
            if u'summary' in event.keys():
                summary = event[u'summary']
            else:
                summary = u'--------'

            self.m_day_sel_event = event

            #self.lstBooking.insert(END, u'主題　　：' + summary)
            self.lstBooking[u'text'] += u'主題　　：' + summary + u'\n'
            #self.lstBooking.insert(END, u'發起人　：' + user_name)
            self.lstBooking[u'text'] += u'發起人　：' + user_name + u'\n'
            if self.extend_time > 0:
                #self.lstBooking.insert(END, u'結束時間：' + et.strftime('%H:%M') + u' 再延' + str(self.extend_time) + u'分鐘')
                self.lstBooking[u'text'] += u'結束時間：' + et.strftime('%H:%M') + u' 再延' + str(self.extend_time) + u'分鐘'
            else:
                #self.lstBooking.insert(END, u'結束時間：' + et.strftime('%H:%M'))
                self.lstBooking[u'text'] += u'結束時間：' + et.strftime('%H:%M')
            #print(t.strftime('end: %Y/%m/%d %H:%M:%S'))
            self.attendees = event[u'attendees']
            self.job_id = self.root.after(ms=1000, func=self.showViewBooking)

            if (eet - now) < datetime.timedelta(minutes = 10):
                if self.m_alarm_stop == False:
                    Gpio.playMusic()
            else:
                Gpio.stopMusic()

            return

        Gpio.stopMusic()
        Gpio.set(Setting.Gpio_LongTermDevices, False)
        ViewBooking.ViewBooking(self.root)
        #self.job_id = self.root.after(ms=1000, func=self.showViewBooking)

    def btnExtend_Click(self):
        if self.extend_time >= 60:
            return

        events = []
        for event in self.m_events[u'items']:
            sst = None
            if u'date' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            #print(sst.timetuple().tm_yday)
            if sst.timetuple().tm_yday != time.localtime().tm_yday:
                continue

            t = None; i = 0
            for evt in events:
                if u'date' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')
                
                if t > sst:
                    break
                i += 1
                
            events.insert(i, event)

        idx = 0
        eet = None
        for event in events:
            if u'summary' in event.keys():
                Log.error(event[u'summary'])
            else:
                Log.error(u'無主題')

            st = None; et = None; user_name = ""
            if u'date' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if u'date' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            now = datetime.datetime.now()
            if (now < st) or (now > et):
                idx += 1
                continue

            f = len(events)
            if len(events) > (idx + 1):
                evt_next = events[idx + 1]

                if u'date' in evt_next[u'start'].keys():
                    st = datetime.datetime.strptime(evt_next[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt_next[u'start'].keys():
                    st = datetime.datetime.strptime(evt_next[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

                eet = et + datetime.timedelta(minutes = self.extend_time + 10)
                
                if eet > st:
                    return
            elif len(events) == (idx + 1):
                eet = et + datetime.timedelta(minutes = self.extend_time + 10)
            else:
                return

            self.m_alarm_stop = False
            self.extend_time += 10
            '''
            self.lstBooking.delete(END)
            self.lstBooking.insert(END, u'結束時間：' + et.strftime('%H:%M') + u' 再延' + str(self.extend_time) + u'分鐘')
            '''

            for user in self.m_users:
                if user[u'primaryEmail'] == event[u'creator'][u'email']:
                    user_name = user[u'name'][u'fullName']
                    break

            summary = u''
            if u'summary' in event.keys():
                summary = event[u'summary']
            else:
                summary = u'--------'

            self.lstBooking[u'text'] = u''
            self.lstBooking[u'text'] += u'主題　　：' + summary + u'\n'
            self.lstBooking[u'text'] += u'發起人　：' + user_name + u'\n'
            self.lstBooking[u'text'] += u'結束時間：' + et.strftime('%H:%M') + u' 再延' + str(self.extend_time) + u'分鐘'

            Gpio.stopMusic()
            #Gpio.set(Setting.Gpio_Alarm, False)

    def btnFinish_Click(self):
        self.root.after_cancel(self.job_id)

        confirm = MsgBox.showConfirm(u'是否結束會議?', self.attendees)

        if confirm: # Yes
            self.m_alarm_stop = True
            Gpio.stopMusic()

            self.root.after_cancel(self.job_id)
            Gpio.set(Setting.Gpio_LongTermDevices, False)
            ViewBooking.ViewBooking(self.root, self.now_event)

        elif confirm is None: # Cancel
            self.lblDateTime[u'text'] = u''
            self.lblRoomName['text'] = u'   '

            tm = time.localtime()
            self.lblDateTime[u'text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime[u'text'] = time.strftime(u'%m/%d %H:%M', time.localtime())

            self.job_id = self.root.after(ms=1000, func=self.showViewBooking)

        else:
            self.m_alarm_stop = True
            Gpio.stopMusic()
            self.lblDateTime[u'text'] = u''
            self.lblRoomName['text'] = u'   '

            tm = time.localtime()
            self.lblDateTime[u'text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime[u'text'] = time.strftime(u'%m/%d %H:%M', time.localtime())

            self.job_id = self.root.after(ms=1000, func=self.showViewBooking)
        

    def lstBooking_Select(self, y):
        self.lstBooking.selection_clear(0, END)
        return 'break'

    def listBookings(self):
        if self.m_dir is None or self.m_calendar is None:
            return

        self.m_day_sel_delay = 5

        #self.lstBooking.delete(0, END)
        self.lstBooking[u'text'] = u''

        events = []
        for event in self.m_events[u'items']:
            sst = None
            if u'date' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if sst.date() != datetime.datetime.now().date():
                continue

            

            t = None; i = 0
            for evt in events:
                if u'date' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')
                
                if t > sst:
                    break
                i += 1
                
            events.insert(i, event)

        cnt = 0
        for event in events:
            if self.m_day_sel_event == event:
                break
            cnt += 1

        index = self.m_day_sel + cnt

        if index < 0:
            #self.lstBooking.insert(END, u'無主題')
            self.lstBooking[u'text'] = u'無主題'
            return

        try:
            event = events[index]

            if u'summary' in event.keys():
                Log.error(event[u'summary'])
            else:
                Log.error(u'無主題')

            st = None; et = None; user_name = ""
            if u'date' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if u'date' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            for user in self.m_users:
                if user[u'primaryEmail'] == event[u'creator'][u'email']:
                    user_name = user[u'name'][u'fullName']
                    break

            summary = ''
            if u'summary' in event.keys():
                summary = event[u'summary']
            else:
                summary = u'--------'

            '''
            self.lstBooking.insert(END, u'主題　　：' + summary)
            self.lstBooking.insert(END, u'發起人　：' + user_name)
            self.lstBooking.insert(END, u'結束時間：' + et.strftime(u'%H:%M'))
            '''
            self.lstBooking[u'text'] += u'主題　　：' + summary + u'\n'
            self.lstBooking[u'text'] += u'發起人　：' + user_name + u'\n'
            self.lstBooking[u'text'] += u'結束時間：' + et.strftime(u'%H:%M')
        except:
            #self.lstBooking.insert(END, u'無主題')
            self.lstBooking[u'text'] = u'無主題'
            

    def btnBefore_Click(self):
        self.m_day_sel -= 1
        self.listBookings()

    def btnAfter_Click(self):
        self.m_day_sel += 1
        self.listBookings()

    def btnToday_Click(self):
        self.m_day_sel = 0
        self.listBookings()
        

    




