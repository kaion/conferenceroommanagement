from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

class GoogleCalendar(object):
    """description of class"""
    def __init__(self, *args, **kwargs):
        self.credentials = {}
        return super(GoogleCalendar, self).__init__(*args, **kwargs)

    def getCredentials(self):
        # If modifying these scopes, delete your previously saved credentials
        # at ~/.credentials/admin-directory_v1-python-quickstart.json
        SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
        CLIENT_SECRET_FILE = 'client_secret.json'
        APPLICATION_NAME = 'Directory API Python Quickstart'

        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join('./', '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'calendar-python.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def getCalendars(self, calendarId):
        if self.credentials == {}:
            self.credentials = self.getCredentials()
        http = self.credentials.authorize(httplib2.Http())
        service = discovery.build('calendar', 'v3', http=http)

        page_token = None
        return service.events().list(calendarId=calendarId, pageToken=page_token, alwaysIncludeEmail = True).execute()
        '''
        page_token = None
        while True:
            events = service.events().list(calendarId=calendarId, pageToken=page_token, alwaysIncludeEmail = True).execute()
            for event in events['items']:
                print(event['summary'])
            page_token = events.get('nextPageToken')
            if not page_token:
                break
        return events
        '''
        

class GoogleDirectory(object):
    """description of class"""
    def __init__(self, *args, **kwargs):
        self.credentials = {}
        return super(GoogleDirectory, self).__init__(*args, **kwargs)

    def getCredentials(self):
        # If modifying these scopes, delete your previously saved credentials
        # at ~/.credentials/admin-directory_v1-python-quickstart.json
        SCOPES = 'https://www.googleapis.com/auth/admin.directory.user'
        CLIENT_SECRET_FILE = 'client_secret.json'
        APPLICATION_NAME = 'Directory API Python Quickstart'

        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join('./', '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'admin-directory_v1-python.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def getUsers(self):
        """Shows basic usage of the Google Admin SDK Directory API.

        Creates a Google Admin SDK API service object and outputs a list of first
        10 users in the domain.
        """
        if self.credentials == {}:
            self.credentials = self.getCredentials()
        http = self.credentials.authorize(httplib2.Http())
        service = discovery.build('admin', 'directory_v1', http=http)

        #print('Getting the first 500 users in the domain')
        results = service.users().list(customer='my_customer', maxResults= 500,
                                       orderBy='email').execute()
        users = results.get('users', [])

        '''        
        if not users:
            print('No users in the domain.')
        else:
            print('Users:')
            for user in users:
                u = user[u'name'][u'fullName']
                m = user[u'primaryEmail']
                try:
                    print(m + '(' + u + ')')
                except:
                    print('')
        '''
        return users


