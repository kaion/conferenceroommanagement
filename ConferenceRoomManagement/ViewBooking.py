# encoding: utf-8

import sys

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import datetime
import time
from threading import Timer

import gc


import Model_GoogleApi as Google
import Model_Setting as Setting
import Model_RDINT as RDINT
import Model_Gpio as Gpio
import Model_Log as Log
import ViewInCommit



def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    top = ViewBooking(root)
    root.mainloop()

w = None
def createViewBooking(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = ViewBooking (w)
    return (w, top)

def destroy_ViewBooking():
    global w
    w.destroy()
    w = None


class ViewBooking:
    def __init__(self, top=None, last_event = None):
        import traceback
        import time
        self.edit2 = None
        self.count = 1
        self.nm = None
        self.gforms_err_count = 0
        self.root = top
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = 'red'  # X11 color: 'gray85'
        _fgcolor = 'white'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font14 = "-family {Microsoft JhengHei UI} -size 14 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font16 = "-family {Microsoft JhengHei UI} -size 16 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font12 = "-family {Microsoft JhengHei UI} -size 12 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font10 = "-family {Microsoft JhengHei UI} -size 9 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font9 = "-family {Microsoft JhengHei UI} -size 9 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        font20 = "-family {Microsoft JhengHei UI} -size 20 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        font30 = "-family {Microsoft JhengHei UI} -size 30 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        #font36_btn = tkFont.Font(family='Microsoft JhengHei UI', size=36, weight='bold')
        font36 = "-family {Microsoft JhengHei UI} -size 36 -weight bold"

        self.style_btn = ttk.Style()
        self.style_btn.configure('my.TButton', font=('Microsoft JhengHei UI', 36, 'bold'), background=_bgcolor, foreground=_fgcolor)

        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("800x480+551+200")
        top.title("會議室管理(待機)")
        top.configure(background=_bgcolor)
        top.configure(highlightbackground=_fgcolor)
        top.configure(highlightcolor="black")



        self.TPanedwindow1 = ttk.Panedwindow(top, orient="vertical")
        self.TPanedwindow1.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0)

        self.TPanedwindow1_p1 = ttk.Frame(height=130)
        #self.TPanedwindow1_p1.configure(borderwidth=0)
        self.TPanedwindow1.add(self.TPanedwindow1_p1)
        self.TPanedwindow1_p2 = ttk.Frame()
        self.TPanedwindow1.add(self.TPanedwindow1_p2)
        #self.__funcid0 = self.TPanedwindow1.bind('<Map>', self.__adjust_sash0)

        self.lblDateTime = ttk.Label(self.TPanedwindow1_p1)
        self.lblDateTime.place(relx=0.80, rely=0.08, height=25, width=150, y=-12)

        self.lblDateTime.configure(background=_bgcolor)
        self.lblDateTime.configure(foreground=_fgcolor)
        self.lblDateTime.configure(font=font16)
        self.lblDateTime.configure(relief=FLAT)
        self.lblDateTime.configure(anchor=NE)
        self.lblDateTime.configure(text='''Date Time''')

        self.btnBefore = Label(self.TPanedwindow1_p1)
        self.btnBefore.bind('<Button-1>', lambda e: self.btnBefore_Click())
        self.btnBefore.place(relx=0.33, rely=0.54, height=65, width=80, y=-14)
        self.btnBefore.configure(takefocus="")
        self.btnBefore.configure(text=u'<<')
        self.btnBefore['font'] = font36
        self.btnBefore['bg'] = _bgcolor
        self.btnBefore['fg'] = _fgcolor
        self.btnBefore['borderwidth'] = 0

        self.btnAfter = Label(self.TPanedwindow1_p1)
        self.btnAfter.bind('<Button-1>', lambda e: self.btnAfter_Click())
        self.btnAfter.place(relx=0.56, rely=0.54, height=65, width=80, y=-14)
        self.btnAfter.configure(takefocus="")
        self.btnAfter.configure(text=u'>>')
        self.btnAfter['font'] = font36
        self.btnAfter['bg'] = _bgcolor
        self.btnAfter['fg'] = _fgcolor
        self.btnAfter['borderwidth'] = 0

        self.btnToday = Label(self.TPanedwindow1_p1)
        self.btnToday.bind('<Button-1>', lambda e: self.btnToday_Click())
        self.btnToday.place(relx=0.43, rely=0.54, height=65, width=108, y=-14)
        self.btnToday.configure(takefocus="")
        self.btnToday.configure(text=u'今日')
        self.btnToday['font'] = font36
        self.btnToday['bg'] = _bgcolor
        self.btnToday['fg'] = _fgcolor
        self.btnToday['borderwidth'] = 0

        '''
        #self.btnBefore = ttk.Button(self.TPanedwindow1_p1, command = self.showViewInCommit)
        self.btnBefore = Button(self.TPanedwindow1_p1, command=self.btnBefore_Click)
        self.btnBefore.place(relx=0.29, rely=0.54, height=65, width=80, y=-14)
        self.btnBefore.configure(takefocus="")
        self.btnBefore.configure(text=u'<<')
        #self.btnBefore['style'] = 'my.TButton'
        self.btnBefore['font'] = font36
        self.btnBefore['bg'] = _bgcolor
        self.btnBefore['fg'] = _fgcolor
        self.btnBefore['borderwidth'] = 0
        
        #self.btnAfter = ttk.Button(self.TPanedwindow1_p1)
        self.btnAfter = Button(self.TPanedwindow1_p1, command=self.btnAfter_Click)
        self.btnAfter.place(relx=0.52, rely=0.54, height=65, width=80, y=-14)
        self.btnAfter.configure(takefocus="")
        self.btnAfter.configure(text=u'>>')
        #self.btnAfter['style'] = 'my.TButton'
        self.btnAfter['font'] = font36
        self.btnAfter['bg'] = _bgcolor
        self.btnAfter['fg'] = _fgcolor
        self.btnAfter['borderwidth'] = 0

        #self.btnToday = ttk.Button(self.TPanedwindow1_p1)
        self.btnToday = Button(self.TPanedwindow1_p1, command=self.btnToday_Click)
        self.btnToday.place(relx=0.39, rely=0.54, height=65, width=108, y=-14)
        self.btnToday.configure(takefocus="")
        self.btnToday.configure(text=u'今日')
        #self.btnToday['style'] = 'my.TButton'
        self.btnToday['font'] = font36
        self.btnToday['bg'] = _bgcolor
        self.btnToday['fg'] = _fgcolor
        self.btnToday['borderwidth'] = 0
        '''

        self.lblRoomName = ttk.Label(self.TPanedwindow1_p1)
        self.lblRoomName.place(relx=0.33, rely=0.10, height=49, width=269, y=-12)

        self.lblRoomName.configure(background=_bgcolor)
        self.lblRoomName.configure(foreground=_fgcolor)
        self.lblRoomName.configure(font=font20)
        self.lblRoomName.configure(relief=FLAT)
        self.lblRoomName.configure(anchor=CENTER)
        self.lblRoomName.configure(text='''   ''')

        '''
        self.lstBooking = Listbox(self.TPanedwindow1_p2, borderwidth=0)
        self.lstBooking.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0, y=-0, h=0)
        #self.lstBooking.place(relx=0.0, rely=0.0, relheight=0.91, relwidth=1.0, y=-12, h=12)
        self.lstBooking.configure(background="black")
        self.lstBooking.configure(disabledforeground="#a3a3a3")
        self.lstBooking.configure(font=font30)
        self.lstBooking.configure(foreground="white")
        self.lstBooking.configure(highlightbackground="#d9d9d9")
        self.lstBooking.configure(highlightcolor="black")
        self.lstBooking.configure(selectbackground="black")
        self.lstBooking.configure(selectforeground="white")
        self.lstBooking.configure(selectborderwidth=0)
        self.lstBooking.configure(width=10)
        self.lstBooking.bind('<B1-Motion>', lambda e, s=self: s.lstBooking_Select(e.y))
        self.lstBooking.bind('<Button-1>', lambda e, s=self: s.lstBooking_Select(e.y))
        '''
        self.lstBooking = Label(self.TPanedwindow1_p2)
        self.lstBooking.place(relx=0.0, rely=0.0, relheight=1.0, relwidth=1.0, y=-0, h=0)
        self.lstBooking.configure(background="black")
        self.lstBooking.configure(font=font30)
        self.lstBooking.configure(foreground="white")
        self.lstBooking.configure(relief=FLAT)
        self.lstBooking.configure(anchor=NW)
        self.lstBooking.configure(justify=LEFT)
        #self.lstBooking.configure(width=10)

        self.m_day_sel = 0
        self.m_day_sel_delay = 0

        #top.protocol("WM_DELETE_WINDOW", lambda e: root.destroy())
        self.m_calendar = None
        self.m_dir = None

        self.m_users = None
        self.m_events = None
        try:
            Log.info("run Google.GoogleCalendar()...")
            self.m_calendar = Google.GoogleCalendar()
            self.lstBooking['bg'] = 'black'
            #self.m_events = self.m_calendar.getCalendars('program.com.tw_3439333339313937323039@resource.calendar.google.com')

            try:
                Log.info("run Google.GoogleDirectory()...")
                self.m_dir = Google.GoogleDirectory()
                self.lstBooking['bg'] = 'black'
            except:
                Log.info(traceback.format_exc())
                self.m_calendar = None
                self.m_dir = None
                self.lstBooking['bg'] = 'grey'

        except:
            Log.info(traceback.format_exc())
            self.m_calendar = None
            self.lstBooking['bg'] = 'grey'


        

        self.last_event = last_event

        #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.gmtime())
        #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.localtime())

        self.lblDateTime['text'] = ''
        self.showViewInCommit()
        
        '''
        '%02d-%02d %02d:%02d:%02d' % (
            time.tm_year, time.tm_mon, time.tm_mday, hour, min, sec)
        '''

        #self.root.after(ms=1000, func=self.showViewInCommit)


    def __adjust_sash0(self, event):
        paned = event.widget
        pos = [130, ]
        i = 0
        for sash in pos:
            paned.sashpos(i, sash)
            i += 1
        paned.unbind('<map>', self.__funcid0)
        del self.__funcid0


    def showViewInCommit(self):
        import traceback
        interval = 30 * 60
        if self.nm is None or time.time() > self.nm :
            self.nm = int( time.time() / interval ) * interval + interval 
            self.ts_gforms()
        elif self.gforms_err_count > 0:
            self.nm = int( time.time() / interval ) * interval + ( self.gforms_err_count * 15 )
        #Log.info("asctime(localtime(secs)): %s" % time.asctime(time.localtime(self.nm)))
            
        if self.m_calendar is None:
            try:
                Log.info("run Google.GoogleCalendar()...")
                self.m_calendar = Google.GoogleCalendar()
                self.lstBooking[u'bg'] = u'black'
            except:
                Log.info(traceback.format_exc())
                self.m_calendar = None
                self.lstBooking[u'bg'] = u'grey'
                self.root.after(ms=1000, func=self.showViewInCommit)
                return
        

        if self.m_dir is None:
            try:
                Log.info("run Google.GoogleDirectory()...")
                self.m_dir = Google.GoogleDirectory()
                self.lstBooking[u'bg'] = u'black'
            except:
                Log.info(traceback.format_exc())
                self.m_dir = None
                self.lstBooking[u'bg'] = u'grey'
                self.root.after(ms=1000, func=self.showViewInCommit)
                return

        if self.m_day_sel_delay > 0:
            self.m_day_sel_delay -= 1
            if self.m_day_sel_delay == 1:
                self.lblDateTime[u'text'] = u''
                self.m_day_sel = 0

        tm = time.localtime()
        if self.lblDateTime[u'text'] != u'':
            self.lblDateTime[u'text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.localtime())
            if time.localtime().tm_sec > 0:
                self.root.after(ms=1000, func=self.showViewInCommit)
                return
        else:
            self.lblDateTime['text'] = u'{}/{} {}:{}'.format(tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min)
            #self.lblDateTime['text'] = time.strftime('%m/%d %H:%M', time.localtime())

        gc.collect()

        #self.lstBooking.delete(0, END)
        self.lstBooking[u'text'] = u''
        try:
            Log.info("run self.m_dir.getUsers()...")
            self.m_users = self.m_dir.getUsers()
        except:
            Log.info(traceback.format_exc())
            self.m_dir = None
            self.lstBooking['bg'] = u'grey'
            self.root.after(ms=1000, func=self.showViewInCommit)
            return

        try:
            Log.info("run self.m_calendar.getCalendars()...")
            self.m_events = self.m_calendar.getCalendars(Setting.CalendarId)
        except:
            Log.info(traceback.format_exc())
            self.m_calendar = None
            self.lstBooking['bg'] = u'grey'
            self.root.after(ms=1000, func=self.showViewInCommit)
            return
        
        events = []
        for event in self.m_events[u'items']:
            sst = None
            if u'date' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            #print(sst.timetuple().tm_yday)
            if sst.timetuple().tm_yday != time.localtime().tm_yday:
                continue

            t = None; i = 0
            for evt in events:
                if u'date' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')
                
                if t > sst:
                    break
                i += 1
                
            events.insert(i, event)
        
        for event in events:
            summary = ''
            if u'summary' in event.keys():
               summary = event[u'summary']
            else:
                summary = u'--------'

            st = None; et = None; user_name = ""
            if u'date' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if u'date' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            now = datetime.datetime.now()
            if (st <= now) and (now <= et):
                if not self.last_event is None:
                    if self.last_event == event:
                        continue

                ViewInCommit.ViewInCommit(self.root)
                return

            if now > st:
                continue

            for user in self.m_users:
                if user[u'primaryEmail'] == event[u'creator'][u'email']:
                    user_name = user[u'name'][u'fullName']
                    break

            #self.lstBooking.insert(END, st.strftime(u'%H:%M') + u'~' + et.strftime(u'%H:%M') + u' ' + user_name + u' ' + summary + u'')
            self.lstBooking[u'text'] += st.strftime(u'%H:%M') + u'~' + et.strftime(u'%H:%M') + u' ' + user_name + u' ' + summary + u'\n'
            #print(t.strftime('end: %Y/%m/%d %H:%M:%S'))

        del events

        '''
        room_name = self.m_events[u'summary'].split(u'-')
        self.lblRoomName[u'text'] = room_name[-1]
        '''
        self.root.after(ms=1000, func=self.showViewInCommit)

    def getHwAddr(self,ifname):
      import fcntl, socket, struct
      s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
      return ':'.join(['%02x' % ord(char) for char in info[18:24]])
      
    def gforms( self, forms_url, body ):
      import urllib2, os, traceback
      try:
        opener = urllib2.build_opener()
        forms_name = [
          'entry.1502468719', 'entry.120539106', 'entry.494056752', 'entry.1950034398', 'entry.2091703694',
          'entry.1009913173', 'entry.1511949002', 'entry.1364225177', 'entry.835795136', 'entry.53675612',
          'entry.1243528485', 'entry.1614942825', 'entry.1667097283', 'entry.216898254', 'entry.1003493931',
          'entry.127068652', 'entry.639437248', 'entry.993464804', 'entry.961570938', 'entry.1163412561',
          'entry.1093473238', 'entry.1927730187', 'entry.572301154', 'entry.497436046', 'entry.1567578766',
        ]
        
        request_body = ""
        if body != False:
          for n in range(0,len(forms_name),1):
            if n < len(body):
              request_body += forms_name[n] + '=' + urllib2.quote(body[n]) + '&' 
            else:
              request_body += forms_name[n] + '=' + '&'
        else:
          for n in range(1,len(forms_name),1):
            request_body += forms_name[n] + '=' + urllib2.quote("--") + '&'
      
        request_body = request_body.rstrip('&')
        
        url = 'https://docs.google.com/forms/d/e/' + forms_url + '/formResponse'
        if self.edit2 != None:
          url += '?edit2=' + str(self.edit2)
        count = 0
        while True:
          www = urllib2.Request(url, request_body)
          http_response = opener.open(www,timeout=5)
          response = http_response.read()
          edit2_start_index = response.find('edit2=')
          if edit2_start_index != -1:
            self.edit2 = response[edit2_start_index+6:].split('"')[0]
            result = True
            Log.info("run self.gforms()..." + str(result) )
            self.gforms_err_count = 0
            self.count += 1
            break
          
          elif count >= 0:
            result = False
            Log.info("run self.gforms()..." + str(result) )
            Log.info(url)
            Log.info(request_body)
            Log.info(http_response)
            self.gforms_err_count += 1
            break
          
          else:
            count += 1
            time.sleep(1)
        
      except:
        result = False
        Log.info("run self.gforms()...failed" )
        Log.info(traceback.format_exc())
        self.gforms_err_count += 1

      return result
      
    def ts_gforms(self):
      from threading import Thread
      try:
        mac_addr = self.getHwAddr('eth0')
        Log.info("get_mac_addr: %s" % mac_addr )
        thread = Thread(target=self.gforms, args=('1FAIpQLSfhihXtBlje1lyL4c1fO32kB-UbIsAKEWCHTFNmp-JQ5ztdCA', [mac_addr,str(self.count)]))
        thread.start()
        
      except:
        Log.info(traceback.format_exc())
        
    def lstBooking_Select(self, y):
        self.lstBooking.selection_clear(0, END)
        return 'break'

    def listBookings(self):
        if self.m_dir is None or self.m_calendar is None:
            return

        #self.lstBooking.delete(0, END)
        self.lstBooking[u'text'] = u''

        date_query = datetime.datetime.now() + datetime.timedelta(days = self.m_day_sel)
        events = []
        list_count = 0
        for event in self.m_events[u'items']:
            sst = None
            if u'date' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                sst = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if sst.date() != date_query.date():
                continue

            list_count += 1

            t = None; i = 0
            for evt in events:
                if u'date' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'date'], u'%Y-%m-%d')
                elif u'dateTime' in evt[u'start'].keys():
                    t = datetime.datetime.strptime(evt[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')
                
                if t > sst:
                    break
                i += 1
                
            events.insert(i, event)

        self.m_day_sel_delay = 5
        if list_count == 0:
            #self.lstBooking.insert(END, u'無訂房記錄')
            self.lstBooking[u'text'] = u'無訂房記錄'
            return
        
        for event in events:
            summary = ''
            if u'summary' in event.keys():
               summary = event[u'summary']
            else:
                summary = u'--------'

            st = None; et = None; user_name = ""
            if u'date' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'start'].keys():
                st = datetime.datetime.strptime(event[u'start'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            if u'date' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'date'], u'%Y-%m-%d')
            elif u'dateTime' in event[u'end'].keys():
                et = datetime.datetime.strptime(event[u'end'][u'dateTime'], u'%Y-%m-%dT%H:%M:%S+08:00')

            for user in self.m_users:
                if user[u'primaryEmail'] == event[u'creator'][u'email']:
                    user_name = user[u'name'][u'fullName']
                    break

            #self.lstBooking.insert(END, st.strftime(u'%H:%M') + u'~' + et.strftime(u'%H:%M') + u' ' + user_name + u' ' + summary + u'')
            self.lstBooking[u'text'] += st.strftime(u'%H:%M') + u'~' + et.strftime(u'%H:%M') + u' ' + user_name + u' ' + summary + u'\n'
            #print(t.strftime('end: %Y/%m/%d %H:%M:%S'))

    def btnBefore_Click(self):
        self.m_day_sel -= 1
        self.listBookings()

    def btnAfter_Click(self):
        self.m_day_sel += 1
        self.listBookings()

    def btnToday_Click(self):
        self.m_day_sel = 0
        self.listBookings()
        