import sys
import logging
import datetime


def basic_log():
    if sys.platform == 'win32':
        pass
    else:
        logging.basicConfig(level=logging.INFO, filename=u'logging.txt')

def info(msg):
    logging.info(datetime.datetime.now().strftime(u'%Y-%m-%d %H:%M:%S - ') + msg)

def error(msg):
    logging.error(datetime.datetime.now().strftime(u'%Y-%m-%d %H:%M:%S - ') + msg)

def critical(msg):
    logging.critical(datetime.datetime.now().strftime(u'%Y-%m-%d %H:%M:%S - ') + msg)
