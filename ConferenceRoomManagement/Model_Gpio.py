import os, sys
import pygame

import Model_Log as Log

played = False
    
    
def setDirect(gpio_pin, dir):
    if (gpio_pin is None) or (gpio_pin == ''):
        return 1

    in_out = None

    if dir == False:
        in_out = 'in'
    else:
        in_out = 'out'
        
    cmd = 'echo ' + in_out + ' > /dev/gpio/' + gpio_pin + '/direction'
    if sys.platform == "win32":
        Log.info(cmd)
        return 0
    return os.system(cmd)

def set(gpio_pin, hi):
    if (gpio_pin is None) or (gpio_pin == ''):
        return 1
        
    hi_lo = None
    if hi == False:
        hi_lo = '0'
    else:
        hi_lo = '1'

    cmd = 'echo ' + hi_lo + ' > /dev/gpio/' + gpio_pin + '/value'
    if sys.platform == "win32":
        Log.info(cmd)
        return 0
    return os.system(cmd)

def get(gpio_pin):
    if (gpio_pin is None) or (gpio_pin == ''):
        return False

    cmd = 'cat /dev/gpio/' + gpio_pin + '/value'

    if sys.platform == "win32":
        Log.info(cmd)
        return False

    #retval = os.system(cmd)
    p = os.popen(cmd)
    retval = p.readline().strip()
    p.close()

    del p

    if retval == '0':
        return False
    return True

def initMusic():
    global played


    pygame.init()
    pygame.mixer.init()
    played = False

def playMusic():
    global played


    if played == True: return

    pygame.mixer.music.load('pre-ending.mp3')
    pygame.mixer.music.set_volume(1.0)
    pygame.mixer.music.play(-1)
    #pygame.event.wait()
    played = True

def stopMusic():
    global played

    if played == False: return

    pygame.mixer.music.stop()
    played = False
        