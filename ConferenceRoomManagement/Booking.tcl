#############################################################################
# Generated by PAGE version 4.12
# in conjunction with Tcl version 8.6
set vTcl(timestamp) ""


if {!$vTcl(borrow)} {

set vTcl(actual_gui_bg) #d9d9d9
set vTcl(actual_gui_fg) #000000
set vTcl(actual_gui_menu_bg) #d9d9d9
set vTcl(actual_gui_menu_fg) #000000
set vTcl(complement_color) #d9d9d9
set vTcl(analog_color_p) #d9d9d9
set vTcl(analog_color_m) #d9d9d9
set vTcl(active_fg) #000000
set vTcl(actual_gui_menu_active_bg)  #d8d8d8
set vTcl(active_menu_fg) #000000
}

#############################################################################
# vTcl Code to Load User Fonts

vTcl:font:add_font \
    "-family {Microsoft JhengHei UI} -size 9 -weight normal -slant roman -underline 0 -overstrike 0" \
    user \
    vTcl:font10
vTcl:font:add_font \
    "-family {Microsoft JhengHei UI} -size 20 -weight bold -slant roman -underline 0 -overstrike 0" \
    user \
    vTcl:font9
#################################
#LIBRARY PROCEDURES
#


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top37
    global vTcl
    set base $vTcl(btop)
    if {$base == ""} {
        set base .top37
    }
    namespace eval ::widgets::$base {
        set dflt,origin 0
        set runvisible 1
    }
    set site_4_0 .top37.tPa38.p1 
    set site_4_0 $site_4_0
    set site_4_1 .top37.tPa38.p2 
    set site_4_0 $site_4_1
    namespace eval ::widgets_bindings {
        set tagslist _TopLevel
    }
    namespace eval ::vTcl::modules::main {
        set procs {
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# GENERATED GUI PROCEDURES
#

proc vTclWindow.top37 {base} {
    if {$base == ""} {
        set base .top37
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl::widgets::core::toplevel::createCmd $top -class Toplevel \
        -background {#d9d9d9} -highlightbackground {#d9d9d9} \
        -highlightcolor black 
    wm focusmodel $top passive
    wm geometry $top 800x480+551+200
    update
    # set in toplevel.wgt.
    global vTcl
    global img_list
    set vTcl(save,dflt,origin) 0
    wm maxsize $top 3844 1061
    wm minsize $top 120 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm deiconify $top
    wm title $top "會議室管理(待機)"
    vTcl:DefineAlias "$top" "FmBooking" vTcl:Toplevel:WidgetProc "" 1
    ttk::style configure TPanedwindow -background #d9d9d9
    ttk::style configure TPanedwindow.Label -background #d9d9d9
    ttk::style configure TPanedwindow.Label -foreground #000000
    ttk::style configure TPanedwindow.Label -font TkDefaultFont
    ttk::panedwindow $top.tPa38
    vTcl:DefineAlias "$top.tPa38" "TPanedwindow1" vTcl:WidgetProc "FmBooking" 1
    ttk::style configure TLabelframe.Label -background #d9d9d9
    ttk::style configure TLabelframe.Label -foreground #000000
    ttk::style configure TLabelframe.Label -font TkDefaultFont
    ttk::style configure TLabelframe -background #d9d9d9
    ttk::labelframe $top.tPa38.p1 \
        -width 580 -height 130 
    vTcl:DefineAlias "$top.tPa38.p1" "TPanedwindow1_p1" vTcl:WidgetProc "FmBooking" 1
    set site_4_0 $top.tPa38.p1
    ttk::label $site_4_0.tLa41 \
        -background {#d9d9d9} -foreground {#000000} \
        -font $::vTcl(fonts,vTcl:font10,object) -relief flat -anchor ne \
        -text {Date Time} 
    vTcl:DefineAlias "$site_4_0.tLa41" "lblDateTime" vTcl:WidgetProc "FmBooking" 1
    ttk::style configure TButton -background #d9d9d9
    ttk::style configure TButton -foreground #000000
    ttk::style configure TButton -font TkDefaultFont
    ttk::button $site_4_0.tBu42 \
        -takefocus {} -text << 
    vTcl:DefineAlias "$site_4_0.tBu42" "btnBefore" vTcl:WidgetProc "FmBooking" 1
    ttk::style configure TButton -background #d9d9d9
    ttk::style configure TButton -foreground #000000
    ttk::style configure TButton -font TkDefaultFont
    ttk::button $site_4_0.tBu43 \
        -takefocus {} -text >> 
    vTcl:DefineAlias "$site_4_0.tBu43" "btnAfter" vTcl:WidgetProc "FmBooking" 1
    ttk::style configure TButton -background #d9d9d9
    ttk::style configure TButton -foreground #000000
    ttk::style configure TButton -font TkDefaultFont
    ttk::button $site_4_0.tBu44 \
        -takefocus {} -text 今日 
    vTcl:DefineAlias "$site_4_0.tBu44" "btnToday" vTcl:WidgetProc "FmBooking" 1
    ttk::label $site_4_0.tLa45 \
        -background {#d9d9d9} -foreground {#000000} \
        -font $::vTcl(fonts,vTcl:font9,object) -relief flat -anchor center \
        -text 701會議室 
    vTcl:DefineAlias "$site_4_0.tLa45" "lblRoomName" vTcl:WidgetProc "FmBooking" 1
    place $site_4_0.tLa41 \
        -in $site_4_0 -x 690 -y 10 -width 109 -relwidth 0 -height 19 \
        -relheight 0 -anchor nw -bordermode ignore 
    place $site_4_0.tBu42 \
        -in $site_4_0 -x 280 -y 100 -width 37 -relwidth 0 -height 25 \
        -relheight 0 -anchor nw -bordermode ignore 
    place $site_4_0.tBu43 \
        -in $site_4_0 -x 410 -y 100 -width 47 -relwidth 0 -height 25 \
        -relheight 0 -anchor nw -bordermode ignore 
    place $site_4_0.tBu44 \
        -in $site_4_0 -x 319 -y 101 -width 87 -height 25 -anchor nw \
        -bordermode ignore 
    place $site_4_0.tLa45 \
        -in $site_4_0 -x 360 -y 70 -width 269 -relwidth 0 -height 49 \
        -relheight 0 -anchor center -bordermode ignore 
    $top.tPa38 add $top.tPa38.p1 
        
    ttk::style configure TLabelframe.Label -background #d9d9d9
    ttk::style configure TLabelframe.Label -foreground #000000
    ttk::style configure TLabelframe.Label -font TkDefaultFont
    ttk::style configure TLabelframe -background #d9d9d9
    ttk::labelframe $top.tPa38.p2 \
        -width 580 -height 285 
    vTcl:DefineAlias "$top.tPa38.p2" "TPanedwindow1_p2" vTcl:WidgetProc "FmBooking" 1
    set site_4_1 $top.tPa38.p2
    listbox $site_4_1.lis40 \
        -background white -disabledforeground {#a3a3a3} -font TkFixedFont \
        -foreground {#000000} -height 4 -highlightbackground {#d9d9d9} \
        -highlightcolor black -selectbackground {#c4c4c4} \
        -selectforeground black -width 10 
    .top37.tPa38.p2.lis40 configure -font TkFixedFont
    .top37.tPa38.p2.lis40 insert end text
    vTcl:DefineAlias "$site_4_1.lis40" "lstBooking" vTcl:WidgetProc "FmBooking" 1
    place $site_4_1.lis40 \
        -in $site_4_1 -x 0 -y 0 -width 0 -relwidth 1 -height 0 -relheight 1 \
        -anchor nw -bordermode ignore 
    $top.tPa38 add $top.tPa38.p2 
        
    ###################
    # SETTING GEOMETRY
    ###################
    place $top.tPa38 \
        -in $top -x 0 -y 0 -width 0 -relwidth 1 -height 0 -relheight 1 \
        -anchor nw -bordermode ignore 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}

set btop ""
if {$vTcl(borrow)} {
    set btop .bor[expr int([expr rand() * 100])]
    while {[lsearch $btop $vTcl(tops)] != -1} {
        set btop .bor[expr int([expr rand() * 100])]
    }
}
set vTcl(btop) $btop
Window show .
Window show .top37 $btop
if {$vTcl(borrow)} {
    $btop configure -background plum
}

