# encoding: utf-8

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import ViewConfirm
import Model_GoogleApi as Google
import Model_Setting as Setting
import Model_RDINT as RDINT
import Model_Gpio as Gpio
import Model_Log as Log


g_msg_result = None

def showConfirm(message, attendees):
    global g_msg_result

    g_msg_result = None
    msg = Tk()
    msg.attributes("-fullscreen", Setting.FullScreen)
    ViewConfirm(msg, message, attendees)
    msg.mainloop()

    return g_msg_result

class ViewConfirm:
    def __init__(self, top=None, message = u'', attendees = None):
        self.root = top
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = 'red'  # X11 color: 'gray85'
        _fgcolor = 'white'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font14 = "-family {Microsoft JhengHei UI} -size 14 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font16 = "-family {Microsoft JhengHei UI} -size 16 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font12 = "-family {Microsoft JhengHei UI} -size 12 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font10 = "-family {Microsoft JhengHei UI} -size 9 -weight "  \
            "normal -slant roman -underline 0 -overstrike 0"
        font9 = "-family {Microsoft JhengHei UI} -size 9 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        font20 = "-family {Microsoft JhengHei UI} -size 20 -weight bold"  \
            " -slant roman -underline 0 -overstrike 0"
        #font36_btn = tkFont.Font(family='Microsoft JhengHei UI', size=36, weight='bold')
        font36 = "-family {Microsoft JhengHei UI} -size 36 -weight bold"
        font48 = "-family {Microsoft JhengHei UI} -size 48 -weight bold"

        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("800x480+551+200")
        top.title("會議室管理(確認關閉)")
        top.configure(background=_bgcolor)
        top.configure(highlightbackground=_fgcolor)
        top.configure(highlightcolor="black")

        self.lblRoomName = ttk.Label(top)
        self.lblRoomName.place(relx=0.0, rely=0.30, height=100, width=800, y=0)

        self.lblRoomName.configure(background=_bgcolor)
        self.lblRoomName.configure(foreground=_fgcolor)
        self.lblRoomName.configure(font=font48)
        self.lblRoomName.configure(relief=FLAT)
        self.lblRoomName.configure(anchor=CENTER)
        self.lblRoomName.configure(text=message)

        '''
        self.btnExit = Button(top, command = self.btnExit_Click)
        self.btnExit.place(relx=0.0, rely=0.75, height=130, width=266, y=0)
        self.btnExit.configure(takefocus="")
        self.btnExit.configure(text=u'結束會議')
        self.btnExit['font'] = font36
        self.btnExit['bg'] = _bgcolor
        self.btnExit['fg'] = _fgcolor
        self.btnExit['borderwidth'] = 0

        self.btnWarningOff = Button(top, command = self.btnWarningOff_Click)
        self.btnWarningOff.place(relx=0.33, rely=0.75, height=130, width=266, y=0)
        self.btnWarningOff.configure(takefocus="")
        self.btnWarningOff.configure(text=u'關閉提醒')
        self.btnWarningOff['font'] = font36
        self.btnWarningOff['bg'] = _bgcolor
        self.btnWarningOff['fg'] = _fgcolor
        self.btnWarningOff['borderwidth'] = 0
        
        self.btnCancel = Button(top, command = self.btnCancel_Click)
        self.btnCancel.place(relx=0.66, rely=0.75, height=130, width=267, y=0)
        self.btnCancel.configure(takefocus="")
        self.btnCancel.configure(text=u'取消')
        self.btnCancel['font'] = font36
        self.btnCancel['bg'] = _bgcolor
        self.btnCancel['fg'] = _fgcolor
        self.btnCancel['borderwidth'] = 0
        '''

        self.btnExit = Label(top)
        self.btnExit.bind('<Button-1>', lambda e: self.btnExit_Click())
        self.btnExit.place(relx=0.0, rely=0.75, height=130, width=266, y=0)
        self.btnExit.configure(takefocus="")
        self.btnExit.configure(text=u'結束會議')
        self.btnExit['font'] = font36
        self.btnExit['bg'] = _bgcolor
        self.btnExit['fg'] = _fgcolor
        self.btnExit['borderwidth'] = 0

        self.btnWarningOff = Label(top)
        self.btnWarningOff.bind('<Button-1>', lambda e: self.btnWarningOff_Click())
        self.btnWarningOff.place(relx=0.33, rely=0.75, height=130, width=266, y=0)
        self.btnWarningOff.configure(takefocus="")
        self.btnWarningOff.configure(text=u'關閉提醒')
        self.btnWarningOff['font'] = font36
        self.btnWarningOff['bg'] = _bgcolor
        self.btnWarningOff['fg'] = _fgcolor
        self.btnWarningOff['borderwidth'] = 0
        
        self.btnCancel = Label(top)
        self.btnCancel.bind('<Button-1>', lambda e: self.btnCancel_Click())
        self.btnCancel.place(relx=0.66, rely=0.75, height=130, width=267, y=0)
        self.btnCancel.configure(takefocus="")
        self.btnCancel.configure(text=u'取消')
        self.btnCancel['font'] = font36
        self.btnCancel['bg'] = _bgcolor
        self.btnCancel['fg'] = _fgcolor
        self.btnCancel['borderwidth'] = 0

        self.attendees = attendees
        self.job_id = None
        self.m_count = 0

    def countDown(self):
        global g_msg_result

        data = []
        user_mail = u''

        if self.m_count > 0:
            self.m_count -= 1

            v = RDINT.openCard()
            if v['ret'] == 0:
                i = 0
                while i < 3:
                    v = RDINT.readMifareOneBlock(Setting.MifareKeyType, 4 + i, Setting.MifareKey)
                    if v['ret'] != 0:
                        break
                    data.extend(v['data'])
                    i += 1
                i = 0
                for d in data:
                    if d == 0:
                        break
                    i += 1

                user_mail = u''.join(map(chr, data[:i]))
                Log.error('user name: ' + user_mail)
                user_mail += Setting.AccountExtension
                if not (self.attendees is None):
                    successed = False
                    for attendee in self.attendees:
                        if user_mail == attendee[u'email']:
                            RDINT.controlDeviceBlink(RDINT.GREEN_ON | RDINT.SOUND_ON, 0, 2, 0, 2)

                            self.root.after_cancel(self.job_id)
                            g_msg_result = True
                            self.root.destroy()
                            self.root.quit()
                            return
                else:
                    RDINT.controlDeviceBlink(RDINT.RED_ON | RDINT.SOUND_ON, 5, 0, 0, 5)
        else:
            self.root.after_cancel(self.job_id)
            self.root.destroy()
            self.root.quit()
            return

        self.job_id = self.root.after(ms=1000, func=self.countDown)

    def btnExit_Click(self):
        global g_msg_result

        self.lblRoomName[u'text'] = u'請靠卡'
        self.m_count = 5
        self.job_id = self.root.after(ms=1000, func=self.countDown)
        return

    def btnWarningOff_Click(self):
        global g_msg_result
        g_msg_result = False
        self.root.destroy()
        self.root.quit()
        return

    def btnCancel_Click(self):
        global g_msg_result
        g_msg_result = None
        self.root.destroy()
        self.root.quit()
        return