# encoding: utf-8
from Tkinter import *
import ttk
import Model_GoogleApi as Google
import ViewBooking
import ViewInCommit
import Model_Setting as Setting
import Model_RDINT as RDINT
import Model_Gpio as Gpio
import Model_Log as Log


import sys



def main():
    '''
    dir = GoogleDirectory.GoogleDirectory()
    users = dir.getUsers()
    if not users:
        print('No users in the domain.')
    else:
        print('Users:')
    for user in users:
        u = user[u'name'][u'fullName']
        m = user[u'primaryEmail']
        try:
            print(m + '(' + u + ')')
        except:
            print('')
    '''

    '''
    a = [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0]
    f = [a[:i] for i in [1,3,5]]
    print(f)
    print([a[:i] for i in [2,4,6,8]])
    '''

    '''Starting point when module is the main routine.'''

    Log.basic_log()

    if RDINT.openReader(int(Setting.Comport)) != 0x00:
        if RDINT.openReader(int(Setting.Comport)) != 0x00:
            if RDINT.openReader(int(Setting.Comport)) != 0x00:
                Log.critical('Reader can not open.')
                return

    if Gpio.setDirect(Setting.Gpio_LongTermDevices, True) != 0:
        Log.critical('ColdAir pin can not open.')
        return

    '''
    if Gpio.setDirect(Setting.Gpio_Alarm, True) != 0:
        print('err: Alarm pin can not open.')
        return
    '''

    Gpio.initMusic()

    global val, w, root
    root = Tk()
    root.attributes("-fullscreen", Setting.FullScreen)
    #root.option_add('*Dialog.msg.font', 'Helvetica 36')
    top = ViewBooking.ViewBooking (root)
    #top = ViewInCommit.ViewInCommit(root)
    root.mainloop()

    #root = tk.Tk()
    #application = Mainapplication(master=root)
    #root.mainloop()
    RDINT.closeReader()

if __name__ == '__main__':
    main()
