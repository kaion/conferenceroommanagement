import os
import sys
from ctypes import *
import math

g_comport = 0
g_rdint = None

g_uid = (c_uint8 * 11)()
g_atqa = (c_uint8 * 2)()
g_sak = (c_uint8 * 1)()

g_out_blk = (c_uint8 * 16)()
g_out_sector = (c_uint8 * 16 * 3)()

LED_OFF = 0x00
LED_ON = 0x01
LED_FLASH = 0x02
LED_ON_OFF = 0x03 

RED_OFF = (LED_OFF)
RED_ON = (LED_ON)
RED_FLASH = (LED_FLASH)

GREEN_OFF = (LED_OFF * (2 ** 2))
GREEN_ON = (LED_ON * (2 ** 2))
GREEN_FLASH = (LED_FLASH * (2 ** 2))

YELLOW_OFF = (LED_OFF * (2 ** 4))
YELLOW_ON = (LED_ON * (2 ** 4))
YELLOW_FLASH = (LED_FLASH * (2 ** 4))

SOUND_OFF = (LED_OFF * (2 ** 6))
SOUND_ON = (LED_ON * (2 ** 6))
SOUND_FLASH = (LED_FLASH * (2 ** 6))

def openReader(comport):
    global g_comport, g_rdint


    rdint_name = 'libRDINT.so'
    if sys.platform == "win32":
        rdint_name = 'RDINT.dll'

    filepath = os.path.dirname(os.path.abspath(__file__))
    g_rdint = cdll.LoadLibrary(os.path.join(filepath, rdint_name))
    g_comport = comport
    return g_rdint.RDINT_NFC_OpenReader(c_uint8(g_comport), c_uint32(115200), c_char_p(""), c_uint8(1), c_uint32(0), POINTER(c_uint)(), 0x01)

def closeReader():
    global g_comport, g_rdint

    return g_rdint.RDINTsys_CloseReader(c_uint8(g_comport))

def openCard(auto_find = 1):
    global g_comport, g_rdint
    global g_uid, g_atqa, g_sak

    ret = g_rdint.RDINT_OpenCard(c_uint8(g_comport), c_uint8(auto_find), g_uid, g_atqa, g_sak)

    if ret != 0:
        return {'ret': ret, 'uid': None, 'atqa': None, 'sak': 0}

    ret_uid = [i for i in g_uid]
    u = ret_uid[:]
    u.reverse()
    uu = 0
    for d in u:
        if d == 0x4d:
            break;
        uu += 1

    ret_uid = ret_uid[0:uu - 2]
    
    ret_atqa = [i for i in g_atqa]
    ret_sak = [i for i in g_sak]
    return {'ret': ret, 'uid': ret_uid, 'atqa': ret_atqa, 'sak': ret_sak[0]}

def readMifareOneBlock(key_type, blk, key):
    global g_comport, g_rdint
    global g_out_blk


    in_key = (c_uint8 * len(key))(*key)

    ret = g_rdint.RDINT_ReadMifareOneBlock(c_uint8(g_comport), c_uint8(key_type), c_uint8(0), c_uint8(0), c_uint8(blk), in_key, g_out_blk)
    if ret != 0:
        return {'ret': ret, 'data': None}

    data = [d for d in g_out_blk]
    return {'ret': ret, 'data': data}

def readMifareOneSector(key_type, sec, key):
    global g_comport, g_rdint
    global g_out_sector

    in_key = (c_uint8 * len(key))(*key)

    ret = g_rdint.RDINT_ReadMifareOneSector(c_uint8(g_comport), c_uint8(key_type), c_uint8(0), c_uint8(0), c_uint8(sec), in_key, g_out_sector)
    if ret != 0:
        return {'ret': ret, 'data': None}

    data = [d for d in g_out_sector]
    return {'ret': ret, 'data': data}

def writeMifareOneBlock(key_type, blk, key, data):
    global g_comport, g_rdint

    in_key = (c_uint8 * len(key))(*key)
    in_data = (c_uint8 * 16)(*data)

    ret = g_rdint.RDINT_WriteMifareOneBlock(c_uint8(g_comport), c_uint8(key_type), c_uint8(0), c_uint8(0), c_uint8(blk), in_key, in_data)
    return {'ret': ret}

def controlDeviceBlink(dev_type, red_ms100, green_ms100, yellow_ms100, sound_ms100):
    global g_comport, g_rdint


    ret = g_rdint.RDINT_DeviceBlinkControl(c_uint8(g_comport), c_uint8(dev_type), c_uint8(red_ms100), c_uint8(green_ms100), c_uint8(yellow_ms100), c_uint8(sound_ms100))
    return {'ret': ret}