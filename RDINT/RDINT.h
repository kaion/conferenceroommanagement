// 下列 ifdef 區塊是建立巨集以協助從 DLL 匯出的標準方式。
// 這個 DLL 中的所有檔案都是使用命令列中所定義 RDINT_EXPORTS 符號編譯的。
// 在命令列定義的符號。任何專案都不應定義這個符號
// 這樣一來，原始程式檔中包含這檔案的任何其他專案
// 會將 RDINT_API 函式視為從 DLL 匯入的，而這個 DLL 則會將這些符號視為
// 匯出的。

#if defined(_WINDOWS) || defined(WIN32)
    #define RDINT_API __declspec(dllexport)
#else // #if defined(_WINDOWS) || defined(WIN32)
    #define RDINT_API
#endif // #if defined(_WINDOWS) || defined(WIN32)

#include <stdint.h>


#ifdef __cplusplus
extern "C"
{
#endif


    RDINT_API int32_t RDINTsys_OpenReader(uint8_t u8COMPort, uint32_t u32Baudrate, char *strAccessCode, uint8_t u8SecurityMode, uint32_t u32OpenDelayMs, uint32_t *pu32Baudrate);
    RDINT_API int32_t RDINTsys_CloseReader(uint8_t u8COMPort);
    RDINT_API int32_t RDINT_NFC_OpenReader(uint8_t u8COMPort, uint32_t u32Baudrate, char *strAccessCode, uint8_t u8SecurityMode, uint32_t u32OpenDelayMs, uint32_t *pu32Baudrate, uint8_t u8Identity);
    RDINT_API int32_t RDINT_OpenCard(uint8_t u8COMPort, uint8_t u1AutoFind, uint8_t *pu8Uid, uint8_t *pu8Atqa, uint8_t *pu8Sak);
    RDINT_API int32_t RDINT_GetUID(uint8_t u8COMPort, uint8_t *pu8Uid);
    RDINT_API int32_t RDINT_ReadMifareOneBlock(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Block, uint8_t *pu8Key, uint8_t *pu8Data);
    RDINT_API int32_t RDINT_ReadMifareOneSector(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Sector, uint8_t *pu8Key, uint8_t *pu8Data);
    RDINT_API int32_t RDINT_WriteMifareOneBlock(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Block, uint8_t *pu8Key, uint8_t *pu8Data);
    RDINT_API int32_t RDINT_DeviceBlinkControl(uint8_t u8COMPort, uint8_t u8Type, uint8_t u8RedMs100, uint8_t u8GreenMs100, uint8_t u8YellowMs100, uint8_t u8SoundMs100);

#ifdef __cplusplus
}
#endif
