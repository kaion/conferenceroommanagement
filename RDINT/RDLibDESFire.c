/**********************************************************************************************
撰寫人: 陳綿烽

日期: 2010/08/12
 ***********************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "RDINT_DEF.h"
#include "RDLib.h"
#include "RDLibSec.h"
#include "DES.h"
#include <time.h>

#ifdef __cplusplus
  extern "C"
  {
  #endif 

  void GenerateRndA(unsigned char pu8RndA[8])
  {
    int i = 0;

    srand((int)time(NULL));

    for (i = 0; i < 8; i++)
      pu8RndA[i] = (rand() % 0xFF);
  }

  //******************************************************************************
  // FUNCTION: CRC_16()
  //       IN: USHORT iLastCRC		// CRC16數值
  //           UCHAR cValue			// 待計算數
  //      OUT: void
  //   RETURN: void
  //  COMMENT:
  //******************************************************************************
  unsigned short CRC_16(unsigned short iLastCRC, unsigned char cValue)
  {
    unsigned int iResult;
    unsigned char cCnt;

    iResult = iLastCRC ^ (unsigned short)cValue;
    for (cCnt = 0; cCnt < 8; cCnt++)
    {
      if (iResult &0x01)
      {
        iResult >>= 1;
        iResult ^= 0x8408;
      }
      else
      {
        iResult >>= 1;
      }
    }
    return iResult;
  }

  //******************************************************************************
  // FUNCTION: CalculateCRC()
  //       IN: UCHAR *cdata			// CRC計算數據源
  //           UCHAR cLength			// 數據長度
  //   IN OUT: USHORT *sCRC16			// CRC16初始值輸入，和CRC16結果輸出
  //   RETURN: void
  //  COMMENT: 依照ISO14443A 方式計算CRC16，對運算長度有限制
  //******************************************************************************
  unsigned short CalculateCRC(unsigned char *cdata, unsigned char cLength, unsigned short *sCRC16)
  {
    unsigned char cCnt;

    if (cLength > 254)
    {
      return 1;
    }

    for (cCnt = 0; cCnt < cLength; cCnt++)
    {
      *sCRC16 = CRC_16(*sCRC16, cdata[cCnt]);
    }
    return 0;
  }

  unsigned short desfire_Authenticate(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char pu8Key[16], unsigned char u8CryptType)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;
    unsigned char pu8IV[8] = 
    {
      '\0'
    };
    unsigned char pu8Out[8] = 
    {
      '\0'
    };
    unsigned char pu8RndA[8] = 
    {
      '\0'
    };
    unsigned char pu8RndA_[8] = 
    {
      '\0'
    };
    unsigned char pu8RndB[8] = 
    {
      '\0'
    };
    unsigned char pu8RndB_[8] = 
    {
      '\0'
    };

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->u8CryptType = u8CryptType; // 記錄使用的加解密型態
    pVar->u8AuthKeyNo = NOT_YET_AUTHENTICATED; // 未認證狀態

    pVar->pu8DESFireSend[i++] = 0x0A;
    pVar->pu8DESFireSend[i++] = u8KeyNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;
    if (RespLen < 1)
      return LRUNKNOWN;
    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    if (pVar->pu8DESFireResp[0] != 0xAF)
      return pVar->pu8DESFireResp[0];

    memcpy(pu8RndB, pVar->pu8DESFireResp + 1, 8); // End_RndB
    mifare_cbc_decrypt(pu8Key, pu8RndB, pu8IV, MD_RECEIVE, pVar->u8CryptType);

    for (i = 0; i < 7; i++)
      pu8RndB_[i] = pu8RndB[i + 1];
    pu8RndB_[7] = pu8RndB[0];

    GenerateRndA(pu8RndA); // RndA

    memset(pu8IV, 0, 8);
    memcpy(pu8Out, pu8RndA, 8);
    mifare_cbc_decrypt(pu8Key, pu8Out, pu8IV, MD_SEND, pVar->u8CryptType);

    memcpy(pu8IV, pu8Out, 8);
    mifare_cbc_decrypt(pu8Key, pu8RndB_, pu8IV, MD_SEND, pVar->u8CryptType);

    i = 0;
    pVar->pu8DESFireSend[i++] = 0xAF;
    memcpy(pVar->pu8DESFireSend + 1, pu8Out, 8);
    memcpy(pVar->pu8DESFireSend + 9, pu8RndB_, 8);
    i = 17;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;
    if (RespLen < 1)
      return LRUNKNOWN;
    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    if (pVar->pu8DESFireResp[0] != 0x00)
      return pVar->pu8DESFireResp[0];

    memcpy(pu8RndA_, pVar->pu8DESFireResp + 1, 8);
    memset(pu8IV, 0, 8);
    mifare_cbc_decrypt(pu8Key, pu8RndA_, pu8IV, MD_RECEIVE, pVar->u8CryptType);
    memcpy(pu8Out, pu8RndA_, 8);

    pu8RndA_[0] = pu8Out[7];
    for (i = 0; i < 7; i++)
      pu8RndA_[i + 1] = pu8Out[i];

    if (memcmp(pu8RndA, pu8RndA_, 8) != 0)
      return LRUNKNOWN;

    pVar->u8AuthKeyNo = u8KeyNo; // 記錄認證的 KeyNo
    memcpy(pVar->pu8SessionKey, pu8RndA, 4);
    memcpy(pVar->pu8SessionKey + 4, pu8RndB, 4);
    memcpy(pVar->pu8SessionKey + 8, pu8RndA + 4, 4);
    memcpy(pVar->pu8SessionKey + 12, pu8RndB + 4, 4);

    return (unsigned short)pVar->pu8ApduResp[0];
  }

  unsigned short desfire_ChangeKeySettings(PLIBVAR pVar, unsigned char u8KeySet)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;
    unsigned char cksData[8] = 
    {
      0
    };
    unsigned char KeyIV[8] = 
    {
      0
    };
    unsigned char Key[16] = 
    {
      0
    };
    unsigned short cksCRC16 = 0x6363;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    cksData[0] = u8KeySet;
    CalculateCRC(cksData, 1, &cksCRC16);
    cksData[1] = cksCRC16 &0xFF;
    cksData[2] = (cksCRC16 >> 8) &0xFF;

    memset(KeyIV, 0x00, 8);
    memcpy(Key, pVar->pu8SessionKey, 16);
    mifare_cbc_decrypt(Key, cksData, KeyIV, 1, pVar->u8CryptType);

    pVar->pu8DESFireSend[i++] = 0x54;
    memcpy(pVar->pu8DESFireSend + 1, cksData, 8);
    i += 8;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetKeySettings(PLIBVAR pVar, unsigned char *pu8KeySet, unsigned char *pu8KeyNum)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x45;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    if (pVar->pu8DESFireResp[0] != 0x00)
      return pVar->pu8DESFireResp[0];

    *pu8KeySet = pVar->pu8DESFireResp[1];
    *pu8KeyNum = pVar->pu8DESFireResp[2];

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_ChangeKey(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char pu8OldKey[16], unsigned char pu8NewKey[16])
  {
    unsigned short i;
    unsigned char RespLen = 0;
    unsigned char KeyIV[16] = 
    {
      0
    };
    unsigned char SessionKey[16] = 
    {
      0
    };
    unsigned char ChangeKeyData[24] = 
    {
      0
    };
    unsigned short cksCRC16 = 0x6363;
    unsigned short newCRC16 = 0x6363;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    if (pVar->u8AuthKeyNo == u8KeyNo)
    {
      CalculateCRC(pu8NewKey, 16, &cksCRC16);

      memcpy(ChangeKeyData, pu8NewKey, 16);
      ChangeKeyData[16] = cksCRC16 &0xFF;
      ChangeKeyData[17] = (cksCRC16 >> 8) &0xFF;
    }
    else
    {
      for (i = 0; i < 16; i++)
        ChangeKeyData[i] = pu8NewKey[i] ^ pu8OldKey[i];
      CalculateCRC(ChangeKeyData, 16, &cksCRC16);
      CalculateCRC(pu8NewKey, 16, &newCRC16);

      ChangeKeyData[16] = cksCRC16 &0xFF;
      ChangeKeyData[17] = (cksCRC16 >> 8) &0xFF;
      ChangeKeyData[18] = newCRC16 &0xFF;
      ChangeKeyData[19] = (newCRC16 >> 8) &0xFF;
    }

    memcpy(SessionKey, pVar->pu8SessionKey, 16);

    i = 0;
    pVar->pu8DESFireSend[i++] = 0xC4;
    pVar->pu8DESFireSend[i++] = u8KeyNo;
    mifare_cbc_decrypt(SessionKey, ChangeKeyData, KeyIV, MD_SEND, pVar->u8CryptType);
    mifare_cbc_decrypt(SessionKey, ChangeKeyData + 8, KeyIV, MD_SEND, pVar->u8CryptType);
    mifare_cbc_decrypt(SessionKey, ChangeKeyData + 16, KeyIV, MD_SEND, pVar->u8CryptType);
    memcpy(pVar->pu8DESFireSend + 2, ChangeKeyData, 24);
    i += 24;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetKeyVersion(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char *pu8KeyVer)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x64;
    pVar->pu8DESFireSend[i++] = u8KeyNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    if (pVar->pu8DESFireResp[0] != 0x00)
      return pVar->pu8DESFireResp[0];

    *pu8KeyVer = pVar->pu8DESFireResp[1];

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateApplication(PLIBVAR pVar, unsigned char pu8AID[3], unsigned char u8KeySet, unsigned char u8KeyNum)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xCA;
    memcpy(pVar->pu8DESFireSend + i, pu8AID, 3);
    i += 3;
    pVar->pu8DESFireSend[i++] = u8KeySet;
    pVar->pu8DESFireSend[i++] = u8KeyNum;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_DeleteApplication(PLIBVAR pVar, unsigned char pu8AID[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xDA;
    memcpy(pVar->pu8DESFireSend + i, pu8AID, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetApplicationIDs(PLIBVAR pVar, unsigned char *pu8AID, unsigned char *pu8AIDTotal)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x6A;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    *pu8AIDTotal = (RespLen - 1) / 3;
    memcpy(pu8AID, pVar->pu8DESFireResp + 1, RespLen);

    if (pVar->pu8DESFireResp[0] == 0xAF)
    {
      unsigned char offset = RespLen - 1;

      i = 0;
      RespLen = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;

      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

      *pu8AIDTotal += (RespLen - 1) / 3;
      memcpy(pu8AID + offset, pVar->pu8DESFireResp + 1, RespLen);
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_SelectApplication(PLIBVAR pVar, unsigned char pu8AID[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x5A;
    memcpy(pVar->pu8DESFireSend + i, pu8AID, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_FormatPICC(PLIBVAR pVar)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xFC;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetVersion(PLIBVAR pVar, Pdesfire_Version pDESfireVersion)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x60;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    if (pVar->pu8DESFireResp[0] == 0xAF)
    {
      memcpy(pDESfireVersion->union_frame1.uf1, pVar->pu8DESFireResp + 1, 7);

      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;

      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    }

    if (pVar->pu8DESFireResp[0] == 0xAF)
    {
      memcpy(pDESfireVersion->union_frame2.uf2, pVar->pu8DESFireResp + 1, 7);

      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;

      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    }

    if (pVar->pu8DESFireResp[0] == 0x00)
    {
      memcpy(pDESfireVersion->union_frame3.uf3, pVar->pu8DESFireResp + 1, 14);
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetFileIDs(PLIBVAR pVar, unsigned char *pu8FID, unsigned char *pu8FIDTotal)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x6F;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    *pu8FIDTotal = RespLen - 1;
    memcpy(pu8FID, pVar->pu8DESFireResp + 1, RespLen);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetFileSettings(PLIBVAR pVar, unsigned char u8FileNo, Pdesfire_FileSetting pFileSet)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xF5;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    memcpy(pFileSet->union_FileSetting.FS, pVar->pu8DESFireResp + 1, RespLen - 1);

    switch (RespLen - 1)
    {
      case 7:
        pFileSet->FileSettingType = 0;
        break;
      case 17:
        pFileSet->FileSettingType = 1;
        break;
      case 13:
        pFileSet->FileSettingType = 2;
        break;
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_ChangeFileSettings(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;
    unsigned char cksData[8] = 
    {
      0
    };
    unsigned char KeyIV[8] = 
    {
      0
    };
    unsigned char Key[16] = 
    {
      0
    };
    unsigned short cksCRC16 = 0x6363;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    cksData[0] = u8ComSet;
    cksData[1] = pu8AccessRight[0];
    cksData[2] = pu8AccessRight[1];
    CalculateCRC(cksData, 3, &cksCRC16);
    cksData[3] = cksCRC16 &0xFF;
    cksData[4] = (cksCRC16 >> 8) &0xFF;

    memset(KeyIV, 0x00, 8);
    memcpy(Key, pVar->pu8SessionKey, 16);
    mifare_cbc_decrypt(Key, cksData, KeyIV, 1, pVar->u8CryptType);

    pVar->pu8DESFireSend[i++] = 0x5F;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    memcpy(pVar->pu8DESFireSend + 2, cksData, 8);
    i += 8;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateStdDataFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8FileSize[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xCD;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    pVar->pu8DESFireSend[i++] = u8ComSet;
    memcpy(pVar->pu8DESFireSend + i, pu8AccessRight, 2);
    i += 2;
    memcpy(pVar->pu8DESFireSend + i, pu8FileSize, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateBackupDataFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8FileSize[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xCB;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    pVar->pu8DESFireSend[i++] = u8ComSet;
    memcpy(pVar->pu8DESFireSend + i, pu8AccessRight, 2);
    i += 2;
    memcpy(pVar->pu8DESFireSend + i, pu8FileSize, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateValueFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8LowerLimit[4], unsigned char pu8UpperLimit[4], unsigned char pu8Value[4], unsigned char u8LmtCreditEnab)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xCC;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    pVar->pu8DESFireSend[i++] = u8ComSet;
    memcpy(pVar->pu8DESFireSend + i, pu8AccessRight, 2);
    i += 2;
    memcpy(pVar->pu8DESFireSend + i, pu8LowerLimit, 4);
    i += 4;
    memcpy(pVar->pu8DESFireSend + i, pu8UpperLimit, 4);
    i += 4;
    memcpy(pVar->pu8DESFireSend + i, pu8Value, 4);
    i += 4;
    pVar->pu8DESFireSend[i++] = u8LmtCreditEnab;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateLinearRecordFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8RecordSize[3], unsigned char pu8MaxNumOfRecords[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xC1;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    pVar->pu8DESFireSend[i++] = u8ComSet;
    memcpy(pVar->pu8DESFireSend + i, pu8AccessRight, 2);
    i += 2;
    memcpy(pVar->pu8DESFireSend + i, pu8RecordSize, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, pu8MaxNumOfRecords, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CreateCyclicRecordFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8RecordSize[3], unsigned char pu8MaxNumOfRecords[3])
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xC0;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    pVar->pu8DESFireSend[i++] = u8ComSet;
    memcpy(pVar->pu8DESFireSend + i, pu8AccessRight, 2);
    i += 2;
    memcpy(pVar->pu8DESFireSend + i, pu8RecordSize, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, pu8MaxNumOfRecords, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_DeleteFile(PLIBVAR pVar, unsigned char u8FileNo)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xDF;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_ReadFile(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data)
  {
    unsigned short i = 0, j = 0;
    unsigned char Offset[3], Length[3];
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    for (i = 0; i < 3; i++)
    {
      Offset[i] = ((i32Offset >> (i *8)) &0xFF);
      Length[i] = ((i32Length >> (i *8)) &0xFF);
    }

    i = 0;
    pVar->pu8DESFireSend[i++] = 0xBD;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    memcpy(pVar->pu8DESFireSend + i, Offset, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, Length, 3);
    i += 3;

    //
    //mifare_cbc_decrypt_all(pVar->pu8SessionKey, pVar->pu8DESFireSend, i);
    //

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    memcpy(pu8Data + j, pVar->pu8DESFireResp + 1, RespLen - 1);
    j += RespLen;

    while (pVar->pu8DESFireResp[0] == 0xAF)
    {
      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;
      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
      memcpy(pu8Data + j, pVar->pu8DESFireResp + 1, RespLen - 1);
      j += RespLen;
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_WriteFile(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data)
  {
    unsigned short i = 0, j = 0;
    unsigned char Offset[3], Length[3];
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    for (i = 0; i < 3; i++)
    {
      Offset[i] = ((i32Offset >> (i *8)) &0xFF);
      Length[i] = ((i32Length >> (i *8)) &0xFF);
    }

    i = 0;
    pVar->pu8DESFireSend[i++] = 0x3D;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    memcpy(pVar->pu8DESFireSend + i, Offset, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, Length, 3);
    i += 3;

    if (i32Length > 52)
    {
      memcpy(pVar->pu8DESFireSend + i, pu8Data, 52);
      i32Length -= 52;
      i += 52;
      j += 52;
    }
    else
    {
      memcpy(pVar->pu8DESFireSend + i, pu8Data, i32Length);
      i += i32Length;
    }

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    while (pVar->pu8DESFireResp[0] == 0xAF)
    {
      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;
      if (i32Length > 52)
      {
        memcpy(pVar->pu8DESFireSend + i, pu8Data + j, 52);
        i32Length -= 52;
        i += 52;
        j += 52;
      }
      else
      {
        memcpy(pVar->pu8DESFireSend + i, pu8Data + j, i32Length);
        i += i32Length;
      }

      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_GetValue(PLIBVAR pVar, unsigned char u8FileNo, int *i32Value)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x6C;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    if (RespLen < 6)
    {
      *i32Value = 0;
      for (i = 0; i < 4; i++)
        *i32Value += pVar->pu8DESFireResp[i + 1] << (i *8);
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_Credit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value)
  {
    unsigned short i = 0, j = 0;
    unsigned char RespLen = 0;
    unsigned char Data[8] = 
    {
      0
    };

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x0C;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    for (j = 0; j < 4; j++)
      Data[j] = (i32Value >> (j *8)) &0xFF;
    memcpy(pVar->pu8DESFireSend + i, Data, 4);
    i += 4;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_Dedit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value)
  {
    unsigned short i = 0, j = 0;
    unsigned char RespLen = 0;
    unsigned char Data[8] = 
    {
      0
    };

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xDC;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    for (j = 0; j < 4; j++)
      Data[j] = (i32Value >> (j *8)) &0xFF;
    memcpy(pVar->pu8DESFireSend + i, Data, 4);
    i += 4;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_LitmitedCredit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value)
  {
    unsigned short i = 0, j = 0;
    unsigned char RespLen = 0;
    unsigned char Data[8] = 
    {
      0
    };

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0x1C;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    for (j = 0; j < 4; j++)
      Data[j] = (i32Value >> (j *8)) &0xFF;
    memcpy(pVar->pu8DESFireSend + i, Data, 4);
    i += 4;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_WriteRecord(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data)
  {
    unsigned short i = 0, j = 0;
    unsigned char Offset[3], Length[3];
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    for (i = 0; i < 3; i++)
    {
      Offset[i] = ((i32Offset >> (i *8)) &0xFF);
      Length[i] = ((i32Length >> (i *8)) &0xFF);
    }

    i = 0;
    pVar->pu8DESFireSend[i++] = 0x3B;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    memcpy(pVar->pu8DESFireSend + i, Offset, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, Length, 3);
    i += 3;

    if (i32Length > 52)
    {
      memcpy(pVar->pu8DESFireSend + i, pu8Data, 52);
      i32Length -= 52;
      i += 52;
      j += 52;
    }
    else
    {
      memcpy(pVar->pu8DESFireSend + i, pu8Data, i32Length);
      i += i32Length;
    }

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    while (pVar->pu8DESFireResp[0] == 0xAF)
    {
      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;
      if (i32Length > 52)
      {
        memcpy(pVar->pu8DESFireSend + i, pu8Data + j, 52);
        i32Length -= 52;
        i += 52;
        j += 52;
      }
      else
      {
        memcpy(pVar->pu8DESFireSend + i, pu8Data + j, i32Length);
        i += i32Length;
      }

      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_ReadRecord(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data)
  {
    unsigned short i = 0, j = 0;
    unsigned char Offset[3], Length[3];
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    for (i = 0; i < 3; i++)
    {
      Offset[i] = ((i32Offset >> (i *8)) &0xFF);
      Length[i] = ((i32Length >> (i *8)) &0xFF);
    }

    i = 0;
    pVar->pu8DESFireSend[i++] = 0xBB;
    pVar->pu8DESFireSend[i++] = u8FileNo;
    memcpy(pVar->pu8DESFireSend + i, Offset, 3);
    i += 3;
    memcpy(pVar->pu8DESFireSend + i, Length, 3);
    i += 3;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    memcpy(pu8Data + j, pVar->pu8DESFireResp + 1, RespLen - 1);
    j += RespLen;

    while (pVar->pu8DESFireResp[0] == 0xAF)
    {
      i = 0;
      pVar->pu8DESFireSend[i++] = 0xAF;
      if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
        return i;

      if (RespLen < 1)
        return LRUNKNOWN;

      MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);
      memcpy(pu8Data + j, pVar->pu8DESFireResp + 1, RespLen - 1);
      j += RespLen;
    }

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_ClearRecordFile(PLIBVAR pVar, unsigned char u8FileNo)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xEB;
    pVar->pu8DESFireSend[i++] = u8FileNo;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_CommitTransaction(PLIBVAR pVar)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xC7;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  unsigned short desfire_AbortTransaction(PLIBVAR pVar)
  {
    unsigned short i = 0;
    unsigned char RespLen = 0;

    if (TURN_OFF == pVar->u1IsOpenReader)
      return LRRDNOINIT;

    pVar->pu8DESFireSend[i++] = 0xA7;

    if ((i = MpRd_ISO14443AB4COS(pVar, pVar->pu8DESFireSend, (unsigned char)i, &RespLen)) != LRSUCCESS)
      return i;

    if (RespLen < 1)
      return LRUNKNOWN;

    MpRd_GetReturnDataArray(pVar, 0, RespLen, pVar->pu8DESFireResp);

    return pVar->pu8DESFireResp[0];
  }

  #ifdef __cplusplus
  }
#endif
