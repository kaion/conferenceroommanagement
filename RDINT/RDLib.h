#ifndef __RDLIB__
  #define __RDLIB__

  #include "RDINT_DEF.h"

  /*定義變數長度----Start*/
  #define LIB_BUFFER_MAX	(0x800L)//(0x10000L)
  #define READER_TOTAL (255)
  #define GMAID_MAX_IN_SAM (64)
  #ifndef PASSWD_LEN
    #define PASSWD_LEN	(4)
  #endif 
  /*定義變數長度----End*/

  // Fong
  #define KEY_FILE_NAME	_T("svchost.dat")

  #ifdef __cplusplus
    extern "C"
    {
    #endif 

    //static int g_WorkType = 0x00;	// 記錄 WorkType 的值
    extern unsigned char g_u8Protocol;
    extern int g_WorkType;
    extern int g_DelayTime;

    typedef struct _Roll_Code
    {
      union
      {
        unsigned long u32RollCodeStep; //ulRollStep:滾碼步進值
        unsigned char pu8RollCodeStep[4];
      } RollCodeStep;
      union
      {
        unsigned long u32RollCode; //ulRollCode:控制器滾碼
        unsigned char pu8RollCode[4];
      } RollCode;
      unsigned char pu8Value[2];
    }
    Roll_Code,  *PRoll_Code;

    typedef struct _SAMCARD
    {
      unsigned char pu8KeyEA[16]; // Key(enc,auth)
      unsigned char pu8KeyEM[16]; // Key(mac)
      unsigned char pu8HClg[8]; // Host Challenge
      unsigned char pu8CClg[8]; // Card Challenge
      unsigned char u8SecLv; // Secur Level
      unsigned char pu8MAC[8]; // MAC

      unsigned char u1IsSAM;
      unsigned char pu8PINCode[4]; // PIN Code
      unsigned short u16GMaidTotal;
      unsigned long pu32GMaid[GMAID_MAX_IN_SAM];
    } SAMCARD,  *PSAMCARD;

    typedef struct __LIB_RDVAR
    {
      unsigned char u8SecurityMode;
      unsigned char pu8AccessCode[PASSWD_LEN];
      Roll_Code rCode;

      //2008-01-09 新協定變數
      unsigned char u8RdStt;
      SAMCARD pSam[4];
      unsigned char pu8RdDesKey[8];
      //RDINT v2
      unsigned long u32CardSerial;
      unsigned short u16Atqa;
      unsigned char u1IsOpenReader;
    } LIBRDVAR,  *PLIBRDVAR;

    typedef struct __LIB_VARIABLE
    {
      unsigned short u16DeviceNum;
      unsigned char pu8RecvBuffer[LIB_BUFFER_MAX];
      unsigned char pu8SendBuffer[LIB_BUFFER_MAX];
      unsigned char pu8DataBuf[LIB_BUFFER_MAX];
      unsigned int u16CmdCur;

	  unsigned char pu8EventBuffer[LIB_BUFFER_MAX];//20140710
	  unsigned int  u16EventBufferLen;

      unsigned char u8SecurityMode;
      unsigned char pu8AccessCode[PASSWD_LEN];
      Roll_Code rCode;

      //2008-01-09 新協定變數
      unsigned char u8RdID;

      //SAM專用
      unsigned char u8RdStt;
      SAMCARD pSam[4];
      unsigned char pu8RdDesKey[8];

      unsigned char pu8Apdu[256];
      unsigned char pu8ApduResp[256];

      // DESFire 專用
      unsigned char u8CryptType; // 記錄認證時使用的加解密型態
      unsigned char u8AuthKeyNo; // 認證時使用的 KeyNo
      unsigned char pu8DESFireSend[60];
      unsigned char pu8DESFireResp[60];
      unsigned char pu8SessionKey[16];

      //RDINT v2
      unsigned long u32CardSerial;
      unsigned short u16Atqa;
      unsigned char u1IsOpenReader;

      //RDINT NFC
      unsigned char pu8TID[2];

      //PLIBRDVAR pRdVar;
      //LIBRDVAR pRdVar[READER_TOTAL];
    } LIBVAR,  *PLIBVAR;

    /************************以下為RDINT V2新增讀卡機連線傳輸函式********************************************/
    //unsigned short Sys_OpenReaderByID(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode, unsigned char u8ReaderID);
    unsigned short Sys_OpenReaderBasic(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode);
    unsigned short Sys_OpenReader(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode);
    unsigned short Sys_CloseReader(PLIBVAR pVar);
    unsigned short Sys_CloseReaderBasic(PLIBVAR pVar);

    /************************以下為RDINT V2新增讀卡機韌體更新函式********************************************/
    unsigned short Up_GoIntoIspMP501(PLIBVAR pVar);
    unsigned short Up_GoIntoIspMP506(PLIBVAR pVar);
    unsigned short Up_EraseChip(PLIBVAR pVar);
    unsigned short Up_ProgramChip(PLIBVAR pVar, unsigned char *pu8Rom, unsigned short u16Len);
    unsigned short Up_RestartSystem(PLIBVAR pVar);

    /************************以下為RDINT V2 新增 ST 讀卡機韌體更新函式********************************************/
    unsigned short Up_ST_CheckInIsp(PLIBVAR pVar, unsigned char *pu8Mode);
    unsigned short Up_ST_JumpToIsp(PLIBVAR pVar);
    unsigned short Up_ST_JumpToApplication(PLIBVAR pVar);
    unsigned short Up_ST_WriteIsp(PLIBVAR pVar, unsigned short u8Page);
    unsigned short Up_ST_WriteAppliction(PLIBVAR pVar, unsigned short u8Page);
    unsigned short Up_ST_WriteIspOK(PLIBVAR pVar);
    unsigned short Up_ST_WriteApplicationOK(PLIBVAR pVar);
    unsigned short Up_ST_SendData(PLIBVAR pVar, unsigned char u8BlockNo, unsigned char *pu8Data);
    /************************以下為RDINT V2新增讀卡機通訊函式********************************************/
    unsigned short MpRd_AntennaControl(PLIBVAR pVar, unsigned char u8Select);
    unsigned short MpRd_DeviceControl(PLIBVAR pVar, unsigned char u8Type);
    unsigned short MpRd_DeviceBlinkControl(PLIBVAR pVar, unsigned char u8Type, unsigned char u8RedMs100, unsigned char u8GreenMs100, unsigned char u8YellowMs100, unsigned char u8SoundMs100);
    unsigned short MpRd_GetControllerRollCode(PLIBVAR pVar);
    unsigned short MpRd_SetCommucation(PLIBVAR pVar, unsigned char u8Type);

    unsigned short MpRd_ReaderInfo(PLIBVAR pVar, unsigned char *pu8SerialNum, unsigned char *pu8FirmwareVer);
    unsigned short MpRd_SetReaderSerialNum(PLIBVAR pVar, unsigned char *pu8SerialNum);

    unsigned short MpRd_OpenCard(PLIBVAR pVar, unsigned char u1AutoFind, unsigned char *pu8Uid, unsigned char *pu8Atqa, unsigned char *pu8Sak);
	unsigned short MpRd_UIDReverse (unsigned char *pu8Uid, char u8UidLen);//20140821
    unsigned short MpRd_CloseCard(PLIBVAR pVar);
    unsigned short MpRd_ReadMifareOneBlock(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned char *pu8Data);
    unsigned short MpRd_ReadMifareOneSector(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Sector, unsigned char *pu8Key, unsigned char *pu8Data);
    unsigned short MpRd_WriteMifareOneBlock(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned char *pu8Data);

    unsigned short MpRd_PurseInit(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32WriteValue);
    unsigned short MpRd_PurseRead(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long *pu32ReadValue);
    unsigned short MpRd_PurseInc(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32IncValue);
    unsigned short MpRd_PurseDec(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32DecValue);
    unsigned short MpRd_PurseBackup(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8SrcBlock, unsigned char u8DestBlock, unsigned char *pu8Key);

    unsigned short MpRd_KeyChange(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Sector, unsigned char *pu8Key, unsigned char *pu8Data);
    unsigned short MpRd_WriteDefaultKey(PLIBVAR pVar, unsigned char u8DefaultKeyIndx, unsigned char *pu8DefaultKey);

    unsigned short MpRd_ReadUltraLight(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data);
    unsigned short MpRd_WriteUltraLight(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8CmdData);

    unsigned short MpRd_KbStart(PLIBVAR pVar);
    unsigned short MpRd_KbStop(PLIBVAR pVar);
    unsigned short MpRd_KbRead(PLIBVAR pVar, unsigned char *pu8Kb);
    unsigned short MpRd_ButtonCheck(PLIBVAR pVar, unsigned char *pu1Status);

    unsigned short MpRd_PasswordSet(PLIBVAR pVar, unsigned char *pu8OldPasswd, unsigned char *pu8NewPasswd);
    unsigned short MpRd_PasswordInit(PLIBVAR pVar);

    unsigned short MpRd_WorkingType(PLIBVAR pVar, unsigned char u8Type);

    unsigned short MpRd_ISO15693Inventory(PLIBVAR pVar, unsigned char u8Flag, unsigned char u8Afi, unsigned char u8MaskLen, unsigned char *pu8Mask, unsigned char *pu8Dsfid, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693StayQuiet(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693Select(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693Reset2Ready(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693Read(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8BlockStart, unsigned char u8BlockCount, unsigned char *pu8Data);
    unsigned short MpRd_ISO15693Write(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Block, unsigned char *pu8Data);

    unsigned short MpRd_ICODE1Request(PLIBVAR pVar, unsigned char *pu8Uid);
    unsigned short MpRd_ICODE1Read(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data);
    unsigned short MpRd_ICODE1Write(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data);
    unsigned short MpRd_ICODE1Halt(PLIBVAR pVar);
    unsigned short MpRd_ISO15693UserCommand(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Manufacturer, unsigned char u8WaitTime, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8Recv, unsigned char *pu8RecvLen);

    unsigned short MpRd_ISO15693LockBlock(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Block);
    unsigned short MpRd_ISO15693WriteAfi(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8AfiValue);
    unsigned short MpRd_ISO15693LockAfi(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693WriteDsfid(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8DsfidValue);
    unsigned short MpRd_ISO15693LockDsfid(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid);
    unsigned short MpRd_ISO15693GetSysInf(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char *pu8SysInf);
    unsigned short MpRd_ISO15693GetBlockSecurity(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8BlockStart, unsigned char u8BlockCount, unsigned char *pu8SecurityData);

    unsigned short MpRd_ISO14443A4Reset(PLIBVAR pVar, unsigned char *pu8GRLCardInf);
    unsigned short MpRd_ISO14443B4Reset(PLIBVAR pVar, unsigned char u1FindType, unsigned char u8SlotNum, unsigned char u8Afi, unsigned char *pu8GRLCardInf);
    unsigned short MpRd_ISO14443AB4COS(PLIBVAR pVar, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8GRLCos);

    unsigned short MpRd_STCardSelect(PLIBVAR pVar, unsigned char *pu8IDNum);
    unsigned short MpRd_STCardIntoDeactive(PLIBVAR pVar);

    unsigned short MpRd_SamReset(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    unsigned short MpRd_SamApdu(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    // Fong 20090629 Start
    unsigned short MpRd_SamBaudrate_1(PLIBVAR pVar, unsigned char u8Baud);
    unsigned short MpRd_SamReset_1(PLIBVAR pVar, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    unsigned short MpRd_SamApdu_1(PLIBVAR pVar, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    // Fong 20090629 End
    // Fong 20110310 Start
    unsigned short MpRd_SamBaudrate_2(PLIBVAR pVar, unsigned char u8Baud);
    unsigned short MpRd_SamReset_2(PLIBVAR pVar, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    unsigned short MpRd_SamApdu_2(PLIBVAR pVar, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen);
    // Fong 20110310 End

    unsigned short MpRd_RC632RegRead(PLIBVAR pVar, unsigned char u8RegSelect, unsigned char *pu8RegData);
    unsigned short MpRd_RC632RegWrite(PLIBVAR pVar, unsigned char u8RegSelect, unsigned char u8RegData);
    unsigned short MpRd_RC632Cmd(PLIBVAR pVar, unsigned char u8WaitTime, unsigned char u8CmdType, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8Recv, unsigned char *pu8RecvLen);
    unsigned short MpRd_ISO15693Cmd(PLIBVAR pVar, unsigned char u8WaitTime, unsigned char u8CmdLen, unsigned char *pu8Cmd);

    unsigned short MpRd_AntennaAmount(PLIBVAR pVar, unsigned char u8AntennaSwitch);
    unsigned short MpRd_ISO15693AutoInventoryParam(PLIBVAR pVar, unsigned char u8Flag, unsigned char u8Afi, unsigned char u8MaskLen, unsigned char *pu8Mask);
    unsigned short MpRd_ISO15693AutoInventory4Antennas(PLIBVAR pVar, unsigned char *pu8GRLUid);
    unsigned short MpRd_MifareAutoRequest4Antennas(PLIBVAR pVar, unsigned char *pu8GRLCS);

    unsigned short MpRd_GetUID(PLIBVAR pVar, unsigned char *pu8Uid);
    unsigned short MpRd_RollCodeEnable(PLIBVAR pVar, unsigned char u1Enable);
    unsigned short MpRd_ReadModuleType(PLIBVAR pVar, unsigned char *pu8ModuleType);

    unsigned short MpRd_SetUnlockDock(PLIBVAR pVar);
    unsigned short MpRd_SetTimeOut(PLIBVAR pVar);
    unsigned short MpRd_SetBuzzerLed(PLIBVAR pVar, unsigned char u8TimeDelay);
    /*unsigned short MpRd_SetReadTagPulse(PLIBVAR pVar);
     */
    unsigned short MpRd_CmdDispatch(PLIBVAR pVar, unsigned char *pu8SendCmd, unsigned char u8SendCmdLen, unsigned char *pu8GRLCmd);
    unsigned short MpRd_ReaderRespTimeOut(PLIBVAR pVar, unsigned char u8TimeOut);

    unsigned short MpRd_EngineerMode(PLIBVAR pVar, unsigned char *pu8CMD, unsigned char *pu8Recv, unsigned long u32DelayMs);

    unsigned char MpRd_GetReturnData(PLIBVAR pVar, unsigned char u8Indx);
    void MpRd_GetReturnDataArray(PLIBVAR pVar, unsigned char u8Indx, unsigned char u8Offset, unsigned char *pu8Data);

    void MpRd_SetLibAccessCode(PLIBVAR pVar, unsigned char *pu8AccessCode);
    /*
    unsigned short MpRd_SetApdu(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned char u8Lc, void *pData, unsigned char *pu8Apdu);
    unsigned short MpRd_SetApduU32(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned long u32Data, unsigned char *pu8Apdu);
    unsigned short MpRd_SetApduU16(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned short u16Data, unsigned char *pu8Apdu);
    unsigned short MpRd_SetApduU8(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned char u8Data, unsigned char *pu8Apdu);
     */
    unsigned short MpRd_SR176ReadBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data);
    unsigned short MpRd_SR176WriteBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data);
    unsigned short MpRd_SR176LockBlock(PLIBVAR pVar, unsigned char u8BlkNo);

    unsigned short MpRd_SRIX4KReadBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data);
    unsigned short MpRd_SRIX4KWriteBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data);
    unsigned short MpRd_SRIX4KAuth(PLIBVAR pVar, unsigned char *pu8Auth);
    unsigned short MpRd_SRIX4KReadUID(PLIBVAR pVar, unsigned char *pu8Uid);

    unsigned short MpRd_LCDClear(PLIBVAR pVar, unsigned char u8RowNo);
    unsigned short MpRd_LCDBacklight(PLIBVAR pVar, unsigned char u8Mode, unsigned char u8Time);
    unsigned short MpRd_LCDDisplayString(PLIBVAR pVar, unsigned char *pu8SendCmd, unsigned char u8SendCmdLen);
    unsigned short MpRd_LCDDisplayPicture(PLIBVAR pVar, unsigned char u8Row, unsigned char u8Column, unsigned char u8Highlight, unsigned char u8High, unsigned char u8Width, unsigned char *pu8Picture, unsigned short u8PictureLen);
    /************************以下為RDINT V2新增演算法函式********************************************/
    #define v2_AntennaControl	MpRd_AntennaControl
    #define v2_EngineerMode		MpRd_CmdDirectSend
    #define v2_DeviceControl	MpRd_DeviceControl
    unsigned short v2_DeviceBlinkControl(PLIBVAR pVar, unsigned char u8DeviceMap, unsigned char u8Duration);

    unsigned short v2_OpenCard(PLIBVAR pVar, unsigned long *pu32CardSerial);
    unsigned short v2_OpenCardReverse(PLIBVAR pVar, unsigned long *pu32CardSerial, unsigned char u1ReverseOn);
    unsigned short v2_OpenCardAndVer(PLIBVAR pVar, unsigned long *pu32CardSerial, unsigned short *pu16Atqa);
    unsigned short v2_OpenCardAndCompare(PLIBVAR pVar);
    #define v2_CloseCard	MpRd_CloseCard

    unsigned short v2_ReadCardDir(PLIBVAR pVar, unsigned short *pu16MadS50, unsigned short *pu16MadS70);
    unsigned short v2_ReadCardFile(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_WriteCardFile(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);
    unsigned short v2_ReadCard(PLIBVAR pVar, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_WriteCard(PLIBVAR pVar, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);

    unsigned short v2_IncrementPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_DecrementPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_ReadPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_BackupPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8SrcBlkNo, unsigned char u8DestBlkNo);
    unsigned short v2_ForceSetPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);

    unsigned short v2_FormatCard(PLIBVAR pVar, unsigned char u8CommonKeyType);
    unsigned short v2_UnformatCard(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned char u1DeleteAllFile);
    unsigned short v2_AddCardFile(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid, unsigned char u8BlkNo, unsigned char *pu8FileContact);
    unsigned short v2_AddCardPurse(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_DeleteCardFile(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid);
    unsigned short v2_DeleteAllFile(PLIBVAR pVar, unsigned char u8CommonKeyType);

    unsigned short v2_ReadCardByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_WriteCardByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);

    unsigned short v2_IncrementPurseByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_DecrementPurseByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_ReadPurseByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_BackupPurseByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8SrcBlkNo, unsigned char u8DestBlkNo);
    unsigned short v2_ForceSetPurseByMAID(PLIBVAR pVar, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);

    unsigned short v2_AddCardFileByMAID(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid, unsigned char u8Sector, unsigned char u8BlkNo, unsigned char *pu8FileContact);
    unsigned short v2_AddCardPurseByMAID(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid, unsigned char u8Sector, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_DeleteCardFileByMAID(PLIBVAR pVar, unsigned char u8CommonKeyType, unsigned short u16Maid, unsigned char u8Sector);

    unsigned short v2_OpenSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char *pu8PINCode);
    unsigned short v2_FindAIDKeyInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char *pu8KeyAccessMode, unsigned char *pu8Key);
    unsigned short v2_FindFormatKeyInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8GMaidType, unsigned char *pu8SecurData);
    unsigned short v2_FindCommonKey(unsigned char u8_CommonKeyType, unsigned char *pu8_KeyA, unsigned char *pu8_AccessCondition, unsigned char *pu8_KeyB);

    unsigned short v2_ReadCardDirInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned short *pu16MadS50, unsigned short *pu16MadS70, unsigned short u16Gid);
    unsigned short v2_ReadCardFileInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_WriteCardFileInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);
    unsigned short v2_ReadCardInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks, unsigned short u16Gid);
    unsigned short v2_WriteCardInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten, unsigned short u16Gid);

    unsigned short v2_IncrementPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_DecrementPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_ReadPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_BackupPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8SrcBlkNo, unsigned char u8DestBlkNo);
    unsigned short v2_ForceSetPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);

    unsigned short v2_FormatCardInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned short u16Gid);
    unsigned short v2_UnformatCardInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned char u1DeleteAllFile, unsigned short u16Gid);
    unsigned short v2_AddCardFileInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned char *pu8FileContact);
    unsigned short v2_AddCardPurseInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_DeleteCardFileInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned long u32GMaid);
    unsigned short v2_DeleteAllFileInSAM(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned short u16Gid);

    unsigned short v2_ReadCardInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_WriteCardInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);

    unsigned short v2_IncrementPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_DecrementPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_ReadPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_BackupPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8SrcBlkNo, unsigned char u8DestBlkNo);
    unsigned short v2_ForceSetPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8BlkNo, unsigned long u32Value);

    unsigned short v2_AddCardFileInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned char *pu8AccessCondition, unsigned long u32GMaid, unsigned char u8Sector, unsigned char u8BlkNo, unsigned char *pu8FileContact);
    unsigned short v2_AddCardPurseInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned char *pu8AccessCondition, unsigned long u32GMaid, unsigned char u8Sector, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_DeleteCardFileInSAMByGMAID(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CommonKeyType, unsigned char u2KeyType, unsigned long u32GMaid, unsigned char u8Sector);

    unsigned short v2_KbdSet(PLIBVAR pVar, unsigned char u8KbdStatus);
    unsigned short v2_KbdGetChar(PLIBVAR pVar, unsigned char *pu8KbdChar);

    unsigned short v2_LoadKeyFile(PLIBVAR pVar, unsigned char *pu8KeyCardPasswd);
    unsigned short v2_DeleteKeyFile(PLIBVAR pVar);


    unsigned short v2_NS_ReadCardDir(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned short *pu16MadS50, unsigned short *pu16MadS70);
    unsigned short v2_NS_ReadCardFile(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_NS_WriteCardFile(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);
    unsigned short v2_NS_ReadCard(PLIBVAR pVar, unsigned char u8KeyType, unsigned char *pu8Key, unsigned char u8StartBlk, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v2_NS_WriteCard(PLIBVAR pVar, unsigned char u8KeyType, unsigned char *pu8Key, unsigned char u8StartBlk, unsigned char *pu8DataBlks, unsigned char u8BlkNo, unsigned char *pu8BlkWritten);
    unsigned short v2_NS_IncrementPurse(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_NS_DecrementPurse(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);
    unsigned short v2_NS_ReadPurse(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long *pu32Value);
    unsigned short v2_NS_BackupPurse(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8SrcBlkNo, unsigned char u8DestBlkNo);
    unsigned short v2_NS_ForceSetPurse(PLIBVAR pVar, unsigned char u8MadKeyType, unsigned char *pu8MadKey, unsigned char u8KeyType, unsigned char *pu8Key, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long u32Value);

    /************************以下為SAM新增讀卡機通訊函式********************************************/
    unsigned short sam_MakeMAC(unsigned char pu8Apdu[], unsigned short u16ApduLen, unsigned char pu8KeyMac[16], unsigned char pu8InMAC[8], unsigned char pu8OutMAC[8]);
    unsigned short sam_MakeMacEnc(unsigned char pu8Data[], unsigned short u16DataLen, unsigned char pu8Key[16], unsigned char pu8KeyMac[16], unsigned char pu8InMAC[8], unsigned char pu8Out[]);
    unsigned short sam_ExternalAuthenticate(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8SecLv);
    unsigned short sam_GetChallenge(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char pu8ChallengeCode[8]);
    unsigned short sam_GetStatus(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8RefCtrl, unsigned char pu8AidInf[], unsigned char *pu8AidCount);
    unsigned short sam_InitializeUpdate(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8KeySetVers, unsigned char u8KeyIndex, unsigned char pu8Challenge[8], unsigned char pu8KeyAuth[16], unsigned char pu8KeyMAC[16]);
    unsigned short sam_ReadBinary(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8OffsetMSB, unsigned char u8OffsetLSB, unsigned char pu8Out[256], unsigned char *pu8OutLen);
    unsigned short sam_Select(PLIBVAR pVar, unsigned char u8SlotNo, unsigned short u16SamFileId, unsigned char pu8Out[23], unsigned char *pu8OutLen);
    unsigned short sam_SelectApplication(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char pu8Aid[10], unsigned char u8AidLen);
    unsigned short sam_VerifyCHV(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8CHV, unsigned char pu8PINCode[8]);

    unsigned short sam_mpVerifyPIN(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char pu8PINCode[4]);
    unsigned short sam_mpReadExistGMaid(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char u8ReadMethod, unsigned short u16Left, unsigned short u16Right, unsigned short *pu16Total, unsigned long pu32GMaid[GMAID_MAX_IN_SAM]);

    /************************ 以下為 DESFire 新增讀卡機通訊函式 ********************************************/
    void GenerateRndA(unsigned char u8RndA[8]);
    unsigned short CRC_16(unsigned short iLastCRC, unsigned char cValue);
    unsigned short CalculateCRC(unsigned char *cdata, unsigned char cLength, unsigned short *sCRC16);
    unsigned short desfire_Authenticate(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char pu8Key[16], unsigned char u8CryptType);
    unsigned short desfire_ChangeKeySettings(PLIBVAR pVar, unsigned char u8KeySet);
    unsigned short desfire_GetKeySettings(PLIBVAR pVar, unsigned char *pu8KeySet, unsigned char *pu8KeyNum);
    unsigned short desfire_ChangeKey(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char pu8OldKey[16], unsigned char pu8NewKey[16]);
    unsigned short desfire_GetKeyVersion(PLIBVAR pVar, unsigned char u8KeyNo, unsigned char *pu8KeyVer);
    unsigned short desfire_CreateApplication(PLIBVAR pVar, unsigned char pu8AID[3], unsigned char u8KeySet, unsigned char u8KeyNum);
    unsigned short desfire_DeleteApplication(PLIBVAR pVar, unsigned char pu8AID[3]);
    unsigned short desfire_GetApplicationIDs(PLIBVAR pVar, unsigned char *pu8AID, unsigned char *pu8AIDTotal);
    unsigned short desfire_SelectApplication(PLIBVAR pVar, unsigned char pu8AID[3]);
    unsigned short desfire_FormatPICC(PLIBVAR pVar);
    unsigned short desfire_GetVersion(PLIBVAR pVar, Pdesfire_Version pDESfireVersion);
    unsigned short desfire_GetFileIDs(PLIBVAR pVar, unsigned char *pu8FID, unsigned char *pu8FIDTotal);
    unsigned short desfire_GetFileSettings(PLIBVAR pVar, unsigned char u8FileNo, Pdesfire_FileSetting pFileSet);
    unsigned short desfire_ChangeFileSettings(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2]);
    unsigned short desfire_CreateStdDataFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8FileSize[3]);
    unsigned short desfire_CreateBackupDataFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8FileSize[3]);
    unsigned short desfire_CreateValueFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8LowerLimit[4], unsigned char pu8UpperLimit[4], unsigned char pu8Value[4], unsigned char u8LmtCreditEnab);
    unsigned short desfire_CreateLinearRecordFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8RecordSize[3], unsigned char pu8MaxNumOfRecords[3]);
    unsigned short desfire_CreateCyclicRecordFile(PLIBVAR pVar, unsigned char u8FileNo, unsigned char u8ComSet, unsigned char pu8AccessRight[2], unsigned char pu8RecordSize[3], unsigned char pu8MaxNumOfRecords[3]);
    unsigned short desfire_DeleteFile(PLIBVAR pVar, unsigned char u8FileNo);
    unsigned short desfire_ReadFile(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data);
    unsigned short desfire_WriteFile(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data);
    unsigned short desfire_GetValue(PLIBVAR pVar, unsigned char u8FileNo, int *i32Value);
    unsigned short desfire_Credit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value);
    unsigned short desfire_Dedit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value);
    unsigned short desfire_LitmitedCredit(PLIBVAR pVar, unsigned char u8FileNo, int i32Value);
    unsigned short desfire_ReadRecord(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data);
    unsigned short desfire_WriteRecord(PLIBVAR pVar, unsigned char u8FileNo, int i32Offset, int i32Length, unsigned char *pu8Data);
    unsigned short desfire_ClearRecordFile(PLIBVAR pVar, unsigned char u8FileNo);
    unsigned short desfire_CommitTransaction(PLIBVAR pVar);
    unsigned short desfire_AbortTransaction(PLIBVAR pVar);
    /************************以下為RDINT V1新增讀卡機通訊函式********************************************/
    #define v1_AntennaControl			MpRd_AntennaControl
    #define v1_ReaderInfo				MpRd_ReaderInfo

    unsigned short v1_OpenReader(PLIBVAR pVar, unsigned char *pu8AccessCode);
    #define v1_CloseReader				Sys_CloseReader
    //unsigned short v1_ReaderInfo(int len, unsigned char *Info);

    #define v1_DeviceControl			MpRd_DeviceControl
    #define v1_DeviceBlinkControl		v2_DeviceBlinkControl

    #define v1_OpenCard					v2_OpenCard
    #define v1_OpenCardReverse			v2_OpenCardReverse
    #define v1_CloseCard				MpRd_CloseCard
    unsigned short v1_OpenRead(PLIBVAR pVar, unsigned char u8StartBlk, unsigned short u16Maid, unsigned char u8BlkNo, unsigned long *pu32CardSerial, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v1_ReadFile(PLIBVAR pVar, unsigned char u8StartBlk, unsigned short u16Maid, unsigned char u8BlkNo, unsigned char *pu8BlkRead, unsigned char *pu8DataBlks);
    unsigned short v1_WriteFile(PLIBVAR pVar, unsigned char u8StartBlk, unsigned short u16Maid, unsigned char u8BlkNo, unsigned char *pu8BlkWritten, unsigned char *pu8DataBlks);

    unsigned short v1_Decrement(PLIBVAR pVar, unsigned short u16Maid, unsigned long u32Value);
    unsigned short v1_Increment(PLIBVAR pVar, unsigned short u16Maid, unsigned long u32Value);
    unsigned short v1_ReadPurse(PLIBVAR pVar, unsigned short u16Maid, unsigned long *pu32Value);

    unsigned short v1_ReadDir(PLIBVAR pVar, unsigned short *pu16DirSize, unsigned char *pu8CardDir);

    unsigned short v1_Issuecard(PLIBVAR pVar, unsigned char u8IssueMode, unsigned char u8CommonKeyType, PISSUESTRC pInitData, unsigned char u8AidTotal, unsigned long *p32CardSerial);
    #define v1_Unformat(pVar, u8CommonKeyType, pu32CardSerial)		v2_UnformatCard(pVar, u8CommonKeyType,TURN_ON);

    unsigned short v1_LoadKeys(PLIBVAR pVar, unsigned long u32CardSerial);
    #define v1_DeleteKeys				v2_DeleteKeyFile
    #define v1_WorkingType				MpRd_WorkingType
    #define v1_ISO15693Inventory		MpRd_ISO15693Inventory

    #define v1_KbdSet					v2_KbdSet
    unsigned short v1_KbdGetChar(PLIBVAR pVar, unsigned char *pu8KbdChar);

    /************************以下為RDINT_NFC 新增函式********************************************/
    unsigned short ReceiveEventRaise(PLIBVAR pVar, unsigned long delay);
    unsigned short NFC_Sys_OpenReader(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode, unsigned char u8Identity);
    unsigned short NFC_StartAutoDetect(PLIBVAR pVar, unsigned char u8WorkMode);
    unsigned short NFC_StopAutoDetect(PLIBVAR pVar);
    unsigned short NFC_ResetAutoDetect(PLIBVAR pVar);
    unsigned short NFC_EventRaise(PLIBVAR pVar, unsigned char *pu8Data, unsigned short *pu16DataLen);
    unsigned short NFC_SendMifareCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendISO14443TypeACommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendISO14443TypeAActivated(PLIBVAR pVar);
    unsigned short NFC_SendISO14443TypeBCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendFelicaCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendISO15693Command(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendNFCIPData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendISO14443TypeACardData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_SendISO14443TypeBCardData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen);
    unsigned short NFC_FelicaPolling(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8PMm, unsigned char *pu8RequestData);
    unsigned short NFC_FelicaRequestService(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8Count, unsigned short *pu16NodeCodeKeyVerList);
    unsigned short NFC_FelicaRequestResponse(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8Mode);
    unsigned short NFC_FelicaEnumSystemCode(PLIBVAR pVar, unsigned char *pu8IDm, unsigned short *pu16Systemcode, unsigned char *pu8Count);
    unsigned short NFC_FelicaEnumService(PLIBVAR pVar, unsigned char *pu8IDm, unsigned short *pu16Systemservice, unsigned char *pu8Count);
    unsigned short NFC_FelicaReadWithoutEncryption(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char u8ServiceCodeListCount, unsigned short *pu16ServiceCodeList, unsigned char u8BlockListCount, unsigned char *pu8BlockList, unsigned char *pu8StatusFlag1, unsigned char *pu8StatusFlag2, unsigned char *pu8BlockCount, unsigned char *pu8BlockData);
    //R20131014.01	Shawn	start
	//unsigned short NFC_FelicaWriteWithoutEncryption(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char u8ServiceCodeListCount, unsigned short *pu16ServiceCodeList, unsigned char u8BlockCount, unsigned char *pu8BlockData, unsigned char *pu8StatusFlag1, unsigned char *pu8StatusFlag2);
	unsigned short NFC_FelicaWriteWithoutEncryption(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char u8ServiceCodeListCount, unsigned short *pu16ServiceCodeList,  unsigned char u8BlockCount, unsigned char *pu8BlockList, unsigned char *pu8BlockData, unsigned char *pu8StatusFlag1, unsigned char *pu8StatusFlag2);
	//R20131014.01	Shawn	end
	unsigned short NFC_ISO14443BPolling(PLIBVAR pVar, unsigned char *pu8Pupi);
    unsigned short NFC_FelicaGetCard(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8PMm);

	//20130926	Shawn	START
	unsigned short NFC_MifareAuthenticationKeyA(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Uid , unsigned char *pu8Key);
	unsigned short NFC_MifareAuthenticationKeyB(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Uid , unsigned char *pu8Key);
	unsigned short NFC_MifareOneReadBlock(PLIBVAR pVar, unsigned char u1KeyType , unsigned char u8BlkNo, unsigned char *pu8BlkData , unsigned char *pu8Uid , unsigned char *pu8Key);
	//20130926	Shawn	END

	unsigned short NFC_CreateNdefRecord( unsigned char *pu8Ndef, unsigned int *pu32NdefLen
									, unsigned char u8Tnf, unsigned char *pu8Type, unsigned char u8TypeLen
									, unsigned char *pu8Id, unsigned char u8IdLen
									, unsigned char *pu8Payload, unsigned int u32PayloadLen, unsigned int u8RecordPos );
	unsigned short NFC_CreateNdefRecordOfMime( unsigned char *pu8Ndef, unsigned int *pu32NdefLen, char *mimeType, unsigned char *p_mime_data, unsigned int mime_data_len );
	unsigned short NFC_CreateNdefRecordOfText(unsigned char *pu8Ndef, unsigned int *pu32NdefLen, unsigned char u8Encoding, unsigned char *pu8LangCode, unsigned char u8LangCodeLen, unsigned char *pu8TextBytes, unsigned int u32TextBytesLen);
	unsigned short NFC_CreateNdefRecordOfUri(unsigned char *pu8Ndef, unsigned int *pu32NdefLen, char *Uri);
    #ifdef __cplusplus
    }
  #endif 

#endif
