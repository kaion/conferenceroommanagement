#include <stdio.h>
#include <string.h>
#include <wchar.h>
//#include "utf8.h"
#include "RDINT_DEF.h"
#include "RDLibSec.h"
#include "RDLib.h"

#include "RDNdefRecord.h"

#ifdef __cplusplus
extern "C"{
#endif

static unsigned char NFC_CheckUriIsIdentifierCode( char *szUri, char *szUriHeader );

unsigned short NFC_CreateNdefRecord( unsigned char *pu8Ndef, unsigned int *pu32NdefLen
									, unsigned char u8Tnf, unsigned char *pu8Type, unsigned char u8TypeLen
									, unsigned char *pu8Id, unsigned char u8IdLen
									, unsigned char *pu8Payload, unsigned int u32PayloadLen, unsigned int u32RecordPos )
{
	unsigned int		header = 0, pos = 0;
	unsigned int		i;


	/*
	*	Header
	*/

	if( (u32RecordPos & RECORD_MID) == RECORD_MID )
	{
		u32RecordPos = RECORD_MID;
		header |= NDEFRECORD_FLAG_CF;
	}
	else
	{
		if( (u32RecordPos & RECORD_BEGIN) == RECORD_BEGIN )
		{
			header |= (NDEFRECORD_FLAG_MB | NDEFRECORD_FLAG_CF);
		}

		if( (u32RecordPos & RECORD_END) == RECORD_END )
		{
			header |= NDEFRECORD_FLAG_ME;
			header &= ~NDEFRECORD_FLAG_CF;
		}
	}

	if( (u32RecordPos & RECORD_MID) == RECORD_MID )
	{
		u8Tnf = TNF_UNCHANGED;
		u8TypeLen = 0;
		u8IdLen = 0;
	}

	if( ( u32RecordPos & ( RECORD_BEGIN | RECORD_END ) ) == ( RECORD_BEGIN | RECORD_END ) )
	{
		if( u32PayloadLen < 256 )
		{
			//header |= NDEFRECORD_FLAG_SR;
		}
	}

	if( (pu8Id != NULL) && (u8IdLen != 0) )
	{
		header |= NDEFRECORD_FLAG_IL;
	}

	header |= NDEFRECORD_FLAG_TNF( u8Tnf );

	pu8Ndef[ pos++ ] = header;

	/*
	*	Len
	*/
	pu8Ndef[ pos++ ] = u8TypeLen;

	if( (header & NDEFRECORD_FLAG_SR) == NDEFRECORD_FLAG_SR )
	{
		pu8Ndef[ pos++ ] = (unsigned char) u32PayloadLen;
	}
	else
	{
		#if 1
		
		pu8Ndef[ pos++ ] = (unsigned char) ( (u32PayloadLen >> 24) & 0xFF );
		pu8Ndef[ pos++ ] = (unsigned char) ( (u32PayloadLen >> 16) & 0xFF );
		pu8Ndef[ pos++ ] = (unsigned char) ( (u32PayloadLen >> 8) & 0xFF );
		pu8Ndef[ pos++ ] = (unsigned char) ( (u32PayloadLen >> 0) & 0xFF );
		#else
		
		memcpy( &pu8Ndef[ pos ], &u32PayloadLen, 4 );
		pos += u32PayloadLen;
		#endif
	}

	if( (pu8Id != NULL) && (u8IdLen != 0) )
	{
		pu8Ndef[ pos++ ] = u8IdLen;
	}

	/*
	*	Type
	*/
	for( i=0; i<u8TypeLen; i++ )
	{
		pu8Ndef[ pos++ ] = pu8Type[ i ];
	}

	/*
	*	ID
	*/
	for( i=0; i<u8IdLen; i++ )
	{
		pu8Ndef[ pos++ ] = pu8Id[ i ];
	}

	/*
	*	Payload
	*/
	memcpy( pu8Ndef + pos, pu8Payload, u32PayloadLen );
	pos += u32PayloadLen;

	*pu32NdefLen = pos;

	return LRSUCCESS;
}

unsigned short NFC_CreateNdefRecordOfApplication( char *strPackageName )
{
	return LRSYSTEM;
}

unsigned short NFC_CreateNdefRecordOfExternal( unsigned char *pu8Ndef, unsigned int *pu32NdefLen
											  , char *domain, char *type, unsigned char *p_data, unsigned char data_len )
{
	return LRSYSTEM;
}

unsigned short NFC_CreateNdefRecordOfMime( unsigned char *pu8Ndef, unsigned int *pu32NdefLen
										   , char *mimeType, unsigned char *p_mime_data, unsigned int mime_data_len )
{
	return NFC_CreateNdefRecord( pu8Ndef, pu32NdefLen, TNF_MIME_MEDIA, mimeType, (unsigned char)strlen( mimeType ), NULL, 0, p_mime_data, mime_data_len, RECORD_BEGIN | RECORD_END );
}

unsigned short NFC_CreateNdefRecordOfText(unsigned char *pu8Ndef, unsigned int *pu32NdefLen
										  , unsigned char u8Encoding, unsigned char *pu8LangCode, unsigned char u8LangCodeLen, unsigned char *pu8TextBytes, unsigned int u32TextBytesLen)
{
	unsigned char Data[9216] = {0};
	unsigned int len = 0;


	if( ( (int)u8LangCodeLen + (int)u32TextBytesLen ) > 9216 )
	{
		return LRSYSTEM;
	}

	if( u8LangCodeLen != 0 )
	{
		Data[len++] = (u8Encoding << 7) | u8LangCodeLen; // Status: 0 - UTF-8(1 - UTF-16), bit6 - 保留, 000010 = 2 文字碼的長度
		memcpy(Data + len, pu8LangCode, u8LangCodeLen); // Language Code
		len += u8LangCodeLen;
	}
	memcpy(Data + len, pu8TextBytes, u32TextBytesLen); // Payload
	len += u32TextBytesLen;

	return NFC_CreateNdefRecord( pu8Ndef, pu32NdefLen, TNF_WELL_KNOWN, "T", 1, NULL, 0, Data, len, RECORD_BEGIN | RECORD_END );
}

static unsigned char NFC_CheckUriIsIdentifierCode( char *szUri, char *szUriHeader )
{

	if( strncmp( szUriHeader, szUri, strlen( szUriHeader ) ) == 0 )
	{
		return (unsigned char)strlen( szUriHeader );
	}

	return 0;
}

unsigned short NFC_CreateNdefRecordOfUri(unsigned char *pu8Ndef, unsigned int *pu32NdefLen
										 , char *Uri)
{
	unsigned int i;
	unsigned char Data[1024] = {0};
	unsigned int len = 0, uri_len = 0;
	unsigned char identifier_code = 0, identifier_code_len = 0;


	/*
	*	檢查 URI Identifier Code
	*/
	if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "http://www." ) ) != 0 )
	{
		identifier_code = 1;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "https://www." ) ) != 0 )
	{
		identifier_code = 2;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "http://" ) ) != 0 )
	{
		identifier_code = 3;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "https://" ) ) != 0 )
	{
		identifier_code = 4;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "tel:" ) ) != 0 )
	{
		identifier_code = 5;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "mailto:" ) ) != 0 )
	{
		identifier_code = 6;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "ftp://anonymous:anonymous@" ) ) != 0 )
	{
		identifier_code = 7;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "ftp://ftp." ) ) != 0 )
	{
		identifier_code = 8;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "ftps://" ) ) != 0 )
	{
		identifier_code = 9;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "sftp://" ) ) != 0 )
	{
		identifier_code = 10;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "smb://" ) ) != 0 )
	{
		identifier_code = 11;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "nfs://" ) ) != 0 )
	{
		identifier_code = 12;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "ftp://" ) ) != 0 )
	{
		identifier_code = 13;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "dav://" ) ) != 0 )
	{
		identifier_code = 14;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "news:" ) ) != 0 )
	{
		identifier_code = 15;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "telnet://" ) ) != 0 )
	{
		identifier_code = 16;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "imap:" ) ) != 0 )
	{
		identifier_code = 17;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "rtsp://" ) ) != 0 )
	{
		identifier_code = 18;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:" ) ) != 0 )
	{
		identifier_code = 19;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "pop:" ) ) != 0 )
	{
		identifier_code = 20;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "sip:" ) ) != 0 )
	{
		identifier_code = 21;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "sips:" ) ) != 0 )
	{
		identifier_code = 22;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "tftp:" ) ) != 0 )
	{
		identifier_code = 23;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "btspp://" ) ) != 0 )
	{
		identifier_code = 24;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "btl2cap://" ) ) != 0 )
	{
		identifier_code = 25;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "btgoep://" ) ) != 0 )
	{
		identifier_code = 26;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "tcpobex://" ) ) != 0 )
	{
		identifier_code = 27;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "irdaobex://" ) ) != 0 )
	{
		identifier_code = 28;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "file://" ) ) != 0 )
	{
		identifier_code = 29;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:epc:id:" ) ) != 0 )
	{
		identifier_code = 30;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:epc:tag:" ) ) != 0 )
	{
		identifier_code = 31;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:epc:pat:" ) ) != 0 )
	{
		identifier_code = 32;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:epc:raw:" ) ) != 0 )
	{
		identifier_code = 33;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:epc:" ) ) != 0 )
	{
		identifier_code = 34;
	}
	else if( ( identifier_code_len = NFC_CheckUriIsIdentifierCode( Uri, "urn:nfc:" ) ) != 0 )
	{
		identifier_code = 35;
	}
	else
	{
		identifier_code_len = 0;
	}

	Data[len++] = identifier_code;					// URI 識別碼
	uri_len = (unsigned int)strlen( Uri ) - identifier_code_len;
	/*
	mbstowcs( wChar, Uri + identifier_code_len, uri_len );
	wchar_to_utf8( wChar, uri_len, Data + len, 254, 0 );
	len += uri_len;
	*/
	for( i=0; i<uri_len; i++ )
	{
		Data[len++] = Uri[identifier_code_len + i];
	}

	return NFC_CreateNdefRecord( pu8Ndef, pu32NdefLen, TNF_WELL_KNOWN, "U", 1, NULL, 0, Data, len, RECORD_BEGIN | RECORD_END );
}



#ifdef __cplusplus
}
#endif
