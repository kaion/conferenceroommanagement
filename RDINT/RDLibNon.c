/**********************************************************************************************
撰寫人:  蔡盛源

日期:2006/10/19
新增:
KBLib_FindAIDKey
KBLib_FindFormatKey
KBLib_MakeKeyCard_KeyA
KBLib_GetKeyFileSize
KBLib_MakeKeyFileHeader
KBLib_AddKey
***********************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "RDINT_DEF.h"
#include "RDLibSec.h"
#include "RDLib.h"

//#define RECV_DELAY		(250L)
//#define RECV_DELAY2		(50L)
//#define RECV_DELAY		(1000L)
#define RECV_DELAY2		(20L)

#define ROLL_CHANGE		(1)
#define ROLL_NO_CHANGE	(0)

#ifdef DO_NO_RESP
#define NoResponseProcess() { \
	if( (SECUR_MODE_NONE != pVar->u8SecurityMode) && (SECUR_MODE_READER_SEL_NONE != pVar->u8SecurityMode) ) MpRd_GetControllerRollCode(pVar); \
	return LRRDNORESP; \
  }\
  if( LRSUCCESS != pVar->u8RdStt ) return LRRDUNKNOWN;
#else 
#define NoResponseProcess() return LRRDNORESP;\
	if( LRSUCCESS != pVar->u8RdStt ) return LRRDUNKNOWN;
#endif 

#define AddSendBuf(u8Data) \
{ \
	if( g_u8Protocol == 0) \
	{ \
	if( (STX == (u8Data)) || (DLE == (u8Data)) || (ETX == (u8Data)) || (C_STX == (u8Data)) )\
	pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE; \
	pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Data; \
	} \
	else \
	{ \
	if( (STX_NFC == (u8Data)) || (DLE == (u8Data)) || (ETX == (u8Data)) || (C_STX == (u8Data)) )\
	pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE; \
	pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Data; \
	} \
}

#ifdef __cplusplus
extern "C"
{
#endif 

	extern unsigned int RecvFunc(unsigned short u16DeviceNum, unsigned char *RecvData, unsigned int len);
	extern unsigned int SendFunc(unsigned short u16DeviceNum, unsigned char *SendData, unsigned int len);
	extern unsigned long GetTickFunc(void);

	unsigned short DoCommand(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay);
	unsigned short DoCommand_v1(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay);
	unsigned short DoCommand_NFC(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay);
	unsigned short DoCommandReaderSelect(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay);
	void CalculateCode(PRoll_Code prCode);
	void CalculateTID(PLIBVAR pVar);

	void CalculateCode(PRoll_Code prCode)
	{

		prCode->RollCode.u32RollCode += prCode->RollCodeStep.u32RollCodeStep;

		if (prCode->RollCode.pu8RollCode[0] &0x01)
		{
			prCode->pu8Value[1] = prCode->RollCode.pu8RollCode[0] ^ prCode->RollCode.pu8RollCode[2];
			prCode->pu8Value[0] = prCode->RollCode.pu8RollCode[1] ^ prCode->RollCode.pu8RollCode[3];
		}
		else
		{
			prCode->pu8Value[0] = prCode->RollCode.pu8RollCode[0] ^ prCode->RollCode.pu8RollCode[3];
			prCode->pu8Value[1] = prCode->RollCode.pu8RollCode[1] ^ prCode->RollCode.pu8RollCode[2];
		}
	}

	void CalculateTID(PLIBVAR pVar)
	{
		unsigned int i32randowm;

		//srand((unsigned int)time(NULL));
		srand(GetTickFunc());
		i32randowm = (rand() % 0xFFFF) + 1;

		pVar->pu8TID[0] = i32randowm &0xFF;
		pVar->pu8TID[1] = (i32randowm >> 8) &0xFF;

#ifdef _DEBUG
		pVar->pu8TID[0] = 0x00;
		pVar->pu8TID[1] = 0x00;
#endif // _DEBUG

		return ;
	}

	unsigned short DoCommand(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay)
	{
		if (g_u8Protocol == 0)
			return DoCommand_v1(pVar, u16DataLen, delay);
		else
			return DoCommand_NFC(pVar, u16DataLen, delay);
	}

	unsigned short DoCommand_v1(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay)
	{
		unsigned int i;
		unsigned long t;
		unsigned int recv_len = 0, tmp_len;
		unsigned char StartPos;
		unsigned char chk = 0;
		unsigned char recv_stt = 0, GetDLE;


		pVar->u8RdStt = LRSUCCESS;
		switch (pVar->u8SecurityMode)
		{
		case SECUR_MODE_NONE:
		case SECUR_MODE_DEFAULT:
			break;
		case SECUR_MODE_READER_SEL_NONE:
		case SECUR_MODE_READER_SEL_ROLLCODE:
		case SECUR_MODE_READER_SEL_DES:
			return DoCommandReaderSelect(pVar, u16DataLen, delay);
		default:
			return 0;
		}

		pVar->u16CmdCur = 0;

		if (!pVar->pu8DataBuf || !pVar->pu8SendBuffer || !pVar->pu8RecvBuffer)
			return 0;

		for (i = u16DataLen; i > 0; i--)
			pVar->pu8DataBuf[i] = pVar->pu8DataBuf[i - 1];
		pVar->pu8DataBuf[0] = (unsigned char)u16DataLen + 1;

		StartPos = pVar->pu8DataBuf[0]; //第一次移位編碼前後部份的分界
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = STX; //加入起始字元

		/*加入移位編碼後部份*/
		if ((pVar->pu8DataBuf[1] != JIN_GET_CONTROLLER_ROLL_CODE) && (pVar->pu8DataBuf[1] != JIN_PASSWDINIT) && (SECUR_MODE_DEFAULT == pVar->u8SecurityMode))
		{
			CalculateCode(&pVar->rCode);
			i = StartPos = pVar->rCode.pu8Value[1] % pVar->pu8DataBuf[0]; //第二次移位編碼前後部份的分界

			/*由此加入移位編碼後部份*/
			while (i > 0 && i < pVar->pu8DataBuf[0])
			{
				if (pVar->pu8DataBuf[i] == STX || pVar->pu8DataBuf[i] == ETX || pVar->pu8DataBuf[i] == DLE)
					pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8DataBuf[i];
				chk += pVar->pu8DataBuf[i];
				i++;
			}

			if (StartPos == 0)
				StartPos = pVar->pu8DataBuf[0];
			//第三次移位編碼前後部份的分界
		}

		/*加入移位編碼前部份*/
		for (i = 0; i < StartPos; i++)
		{
			if (pVar->pu8DataBuf[i] == STX || pVar->pu8DataBuf[i] == ETX || pVar->pu8DataBuf[i] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8DataBuf[i];
			chk += pVar->pu8DataBuf[i];
		}

		/*加入滾碼*/
		if ((pVar->pu8DataBuf[1] != JIN_GET_CONTROLLER_ROLL_CODE) && (pVar->pu8DataBuf[1] != JIN_PASSWDINIT) && (SECUR_MODE_DEFAULT == pVar->u8SecurityMode))
		{
			if (pVar->rCode.pu8Value[1] == STX || pVar->rCode.pu8Value[1] == ETX || pVar->rCode.pu8Value[1] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->rCode.pu8Value[1];
			chk += pVar->rCode.pu8Value[1];

			if (pVar->rCode.pu8Value[0] == STX || pVar->rCode.pu8Value[0] == ETX || pVar->rCode.pu8Value[0] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->rCode.pu8Value[0];
			chk += pVar->rCode.pu8Value[0];
		}

		/*加入檢查碼*/
		if (chk == STX || chk == ETX || chk == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = chk;

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = ETX; //加入結束字元

		if (SendFunc(pVar->u16DeviceNum, pVar->pu8SendBuffer, (unsigned short)pVar->u16CmdCur) != pVar->u16CmdCur)
			return 0;

		t = GetTickFunc();
		while ((GetTickFunc() - t) < delay)
		{
			tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, (unsigned short)(LIB_BUFFER_MAX - recv_len));
			for (i = recv_len; i < (recv_len + tmp_len); i++)
			{
				switch (recv_stt)
				{
				case 0:
					if (pVar->pu8RecvBuffer[i] == STX)
					{
						recv_stt++;
						GetDLE = 0;
					}
					break;
				case 1:
					if (pVar->pu8RecvBuffer[i] == ETX && !GetDLE)
						recv_stt++;

					if (pVar->pu8RecvBuffer[i] == DLE && !GetDLE)
						GetDLE = 1;
					else
						GetDLE = 0;
					break;
				}
			}

			recv_len += (i - recv_len);
			if (recv_stt == 2)
				break;
			if (GetTickFunc() < t)
				t = GetTickFunc();

			if (tmp_len != 0)
			{
				t = GetTickFunc();
				delay = RECV_DELAY2;
			}

			if (recv_len > LIB_BUFFER_MAX)
				return 0;
		}


		StartPos = 0;
		while (pVar->pu8RecvBuffer[StartPos] != STX && StartPos < recv_len)
			StartPos++;
		//找出起始字元
		if (StartPos == recv_len)
			return 0;
		//若找不到起始字元則返回
		if (StartPos > 0 && pVar->pu8RecvBuffer[StartPos - 1] == DLE)
			return 0;
		//若找到起始字元，但起始字元前面是DLE返回
		if (pVar->pu8RecvBuffer[StartPos] != STX || pVar->pu8RecvBuffer[recv_len - 1] != ETX)
			return 0;
		//若開頭不是起始字元，結尾不是結束字元也返回

		/*去掉DLE*/
		pVar->u16CmdCur = 0;
		for (i = (StartPos + 1); i < (recv_len - 1); i++)
		{
			if (pVar->pu8RecvBuffer[i] == DLE)
				i++;
			pVar->pu8RecvBuffer[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[i];
		}

		/*驗證檢查碼是否正確*/
		chk = 0;
		recv_len = pVar->u16CmdCur;
		for (i = 0; i < (recv_len - 1); i++)
			chk += pVar->pu8RecvBuffer[i];
		if (pVar->pu8RecvBuffer[i] != chk)
			return 0;

		if (SECUR_MODE_DEFAULT == pVar->u8SecurityMode)
		{
			recv_len -= 3;

			if (0 == recv_len)
				return 0;

			/*移位編碼前後部份的分界*/
			StartPos = pVar->pu8DataBuf[0];
			if (pVar->pu8DataBuf[1] != JIN_GET_CONTROLLER_ROLL_CODE && pVar->pu8DataBuf[1] != JIN_PASSWDINIT)
			{
				if (pVar->rCode.pu8Value[1] != pVar->pu8RecvBuffer[i - 2] || pVar->rCode.pu8Value[0] != pVar->pu8RecvBuffer[i - 1])
					return 0;
				i = StartPos = pVar->rCode.pu8Value[1] % recv_len;
			}
			else
			{
				i = StartPos = pVar->pu8RecvBuffer[i - 2] % recv_len;
			}

			//將移位編碼後部份加到DataBuf前面
			pVar->u16CmdCur = 0;
			while (i > 0 && i < recv_len)
			{
				pVar->pu8DataBuf[i] = pVar->pu8RecvBuffer[pVar->u16CmdCur++];
				i++;
			}
		}
		else
		{
			recv_len--;
			StartPos = 0;
		}

		//將移位編碼前部份加到DataBuf後面
		if (StartPos == 0)
			StartPos = pVar->pu8RecvBuffer[0];
		for (i = 0; i < StartPos; i++)
			pVar->pu8DataBuf[i] = pVar->pu8RecvBuffer[pVar->u16CmdCur++];

		pVar->pu8SendBuffer[0] = pVar->pu8DataBuf[0]; //暫時用SendBuf作迴圈暫存
		for (i = 0; i < pVar->pu8SendBuffer[0]; i++)
			pVar->pu8DataBuf[i] = pVar->pu8DataBuf[i + 1];

		return (unsigned short)(pVar->pu8SendBuffer[0] - 1);
	}

	unsigned short DoCommand_NFC(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay)
	{
		unsigned int i;
		unsigned long t;
		unsigned int recv_len = 0, tmp_len;
		unsigned int StartPos;
		unsigned int EndPos;
		unsigned char chk = 0;
		unsigned char recv_stt = 0, GetDLE;
		unsigned char u8Temp;
		//unsigned char pu8SendBuffer[256] = {0};
		//unsigned char pu8RecvBuffer[256] = {0};

		unsigned char check_fc = 0; //20140710
		unsigned int data_cnt = 0; //20140710

		unsigned char pu8DataBuf[256] = 
		{
			0
		};

		pVar->u8RdStt = LRSUCCESS;
		switch (pVar->u8SecurityMode)
		{
		case SECUR_MODE_NONE:
		case SECUR_MODE_DEFAULT:
			break;
		case SECUR_MODE_READER_SEL_NONE:
		case SECUR_MODE_READER_SEL_ROLLCODE:
		case SECUR_MODE_READER_SEL_DES:
			return DoCommandReaderSelect(pVar, u16DataLen, delay);
		default:
			return 0;
		}

		pVar->u16CmdCur = 0;

		if (!pVar->pu8DataBuf || !pVar->pu8SendBuffer || !pVar->pu8RecvBuffer)
			return 0;

		//memcpy(pu8DataBuf, pVar->pu8DataBuf, u16DataLen);

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = STX_NFC; // 加入起始字元

		u8Temp = (unsigned char)((u16DataLen + 2+7) &0xFF); // Len1 Byte1
		if (u8Temp == STX_NFC || u8Temp == ETX || u8Temp == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Temp;
		chk ^= u8Temp;

		u8Temp = (unsigned char)(((u16DataLen + 2+7) << 8) &0xFF); // Len1 Byte2
		if (u8Temp == STX_NFC || u8Temp == ETX || u8Temp == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Temp;
		chk ^= u8Temp;

		if (pVar->u8RdID == STX_NFC || pVar->u8RdID == ETX || pVar->u8RdID == DLE)
			// ID
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->u8RdID;
		chk ^= pVar->u8RdID;

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = 0x00; // Security
		//if(pVar->u8SecurityMode == STX || pVar->u8SecurityMode == ETX || pVar->u8SecurityMode == DLE)// Security
		//	pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		//pu8SendBuffer[pVar->u16CmdCur++] = pVar->u8SecurityMode;

		u8Temp = (unsigned char)((u16DataLen + 2) &0xFF); // Len2 Byte1
		if (u8Temp == STX_NFC || u8Temp == ETX || u8Temp == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Temp;
		chk ^= u8Temp;

		u8Temp = (unsigned char)(((u16DataLen + 2) << 8) &0xFF); // Len2 Byte2
		if (u8Temp == STX_NFC || u8Temp == ETX || u8Temp == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = u8Temp;
		chk ^= u8Temp;

		memset(&pVar->pu8TID[0], 0x00, 2);
		StartPos = u16DataLen; // FC + Data 的長度

		check_fc = pVar->pu8DataBuf[0]; //20140710

		// 移位編碼第一部份
		if ((pVar->pu8DataBuf[0] != JIN_GET_CONTROLLER_ROLL_CODE) && (pVar->pu8DataBuf[0] != JIN_PASSWDINIT) && (SECUR_MODE_DEFAULT == pVar->u8SecurityMode))
		{
			CalculateTID(pVar);
			i = StartPos = pVar->pu8TID[1] % u16DataLen;

			while ((i > 0) && (i < u16DataLen))
			{
				if (pVar->pu8DataBuf[i] == STX_NFC || pVar->pu8DataBuf[i] == ETX || pVar->pu8DataBuf[i] == DLE)
					pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8DataBuf[i];
				chk ^= pVar->pu8DataBuf[i];
				i++;
			}

			if (StartPos == 0)
				StartPos = u16DataLen;
		}

		// 移位編碼第二部份
		for (i = 0; i < StartPos; i++)
		{
			if (pVar->pu8DataBuf[i] == STX_NFC || pVar->pu8DataBuf[i] == ETX || pVar->pu8DataBuf[i] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8DataBuf[i];
			chk ^= pVar->pu8DataBuf[i];
		}

		// 加入滾碼
		if ((pVar->pu8DataBuf[0] != JIN_PASSWDINIT) && (SECUR_MODE_DEFAULT == pVar->u8SecurityMode))
		{
			if (pVar->pu8TID[1] == STX_NFC || pVar->pu8TID[1] == ETX || pVar->pu8TID[1] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8TID[1];
			chk ^= pVar->pu8TID[1];

			if (pVar->pu8TID[0] == STX_NFC || pVar->pu8TID[0] == ETX || pVar->pu8TID[0] == DLE)
				pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8TID[0];
			chk ^= pVar->pu8TID[0];
		}

		//20150210 0xF6無滾碼，修正長度錯誤和check sum 錯誤問題
		if(pVar->pu8DataBuf[0] == JIN_PASSWDINIT)
		{
			pVar->pu8SendBuffer[1] -= 2;
			chk -= 2;
		}

		// 加入檢查碼
		if (chk == STX_NFC || chk == ETX || chk == DLE)
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = DLE;
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = chk;

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = ETX; // 加入結束字元

		//memcpy(pVar->pu8SendBuffer, pu8SendBuffer, pVar->u16CmdCur);

		if (SendFunc(pVar->u16DeviceNum, pVar->pu8SendBuffer, (unsigned short)pVar->u16CmdCur) != pVar->u16CmdCur)
			return 0;

RE_RECEIVE:

		recv_len = 0;
		t = GetTickFunc();
		while ((GetTickFunc() - t) < delay)
		{
			tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, (unsigned short)(LIB_BUFFER_MAX - recv_len));

			if( tmp_len > LIB_BUFFER_MAX )
				break;

			for (i = recv_len; i < (recv_len + tmp_len); i++)
			{
				switch (recv_stt)
				{
				case 0:
					if (pVar->pu8RecvBuffer[i] == STX_NFC)
					{
						recv_stt++;
						GetDLE = 0;
					}
					break;
				case 1:
					if (pVar->pu8RecvBuffer[i] == ETX && !GetDLE)
						recv_stt++;

					if (pVar->pu8RecvBuffer[i] == DLE && !GetDLE)
						GetDLE = 1;
					else
						GetDLE = 0;
					break;
				}
			}

			recv_len += (i - recv_len);
			if (recv_stt == 2)
				break;
			if (GetTickFunc() < t)
				t = GetTickFunc();

			if (tmp_len != 0)
			{
				t = GetTickFunc();
				delay = RECV_DELAY2;
			}

			if (recv_len > LIB_BUFFER_MAX)
				return 0;
		}

		StartPos = 0;

RECHECK_CMD:

		while (pVar->pu8RecvBuffer[StartPos] != STX_NFC && StartPos < recv_len)
			//找出起始字元
			StartPos++;


		if (StartPos == recv_len)
			//若找不到起始字元則返回
			return 0;

		if (StartPos > 0 && pVar->pu8RecvBuffer[StartPos - 1] == DLE)
			//若找到起始字元，但起始字元前面是DLE返回
			return 0;

		if (pVar->pu8RecvBuffer[StartPos] != STX_NFC /*|| pVar->pu8RecvBuffer[recv_len - 1] != ETX*/)
			//若開頭不是起始字元，結尾不是結束字元也返回
			return 0;

		//20140710 start
		//當找出起始字元開始找結束字元
		EndPos = StartPos ;
		while ( EndPos < recv_len)
		{			
			EndPos++;
			if( pVar->pu8RecvBuffer[EndPos] == ETX )
			{
				if(pVar->pu8RecvBuffer[EndPos - 1] != DLE || (pVar->pu8RecvBuffer[EndPos - 1] == DLE && pVar->pu8RecvBuffer[EndPos - 2] == DLE))
					break;
			}
		}

		if(((EndPos + 1) == recv_len ) && ( pVar->pu8RecvBuffer[EndPos] != ETX))
		{
			return 0;
		}
		//20140710 end

		//memcpy(pu8RecvBuffer, pVar->pu8RecvBuffer, recv_len);

		// 去掉DLE
		pVar->u16CmdCur = 0;
		//for (i = (StartPos + 1); i < (recv_len - 1); i++) 
		for (i = (StartPos); i < EndPos; i++)
		{
			if (pVar->pu8RecvBuffer[i] == DLE)
				i++;
			pVar->pu8RecvBuffer[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[i];
		}

		// 驗證檢查碼是否正確
		chk = 0;

// 		recv_len = pVar->u16CmdCur;
// 		tmp_len = recv_len;
// 		//for (i = 0; i < (recv_len - 1); i++)
// 		for (i = StartPos; i < (recv_len - 1); i++)		

		data_cnt = pVar->u16CmdCur ;
		tmp_len = data_cnt;
		for (i = StartPos + 1; i < (data_cnt - 1 ); i++) //StartPos + 1 , crc跳過STX
			chk ^= pVar->pu8RecvBuffer[i];

		if (pVar->pu8RecvBuffer[i] != chk)
			return 0;

		//StartPos = 0;
		if (SECUR_MODE_DEFAULT == pVar->u8SecurityMode)
		{
// 			recv_len -= 3; // 去掉 Len1(2) + ID(1) + Security(1)
// 
// 			if (0 == recv_len)
// 				return 0;
//			tmp_len = (pVar->pu8RecvBuffer[5] << 8) + pVar->pu8RecvBuffer[4] - 2;
//			i = StartPos = tmp_len - (pVar->pu8RecvBuffer[recv_len] % tmp_len);

			data_cnt -= 3; // 去掉 Len1(2) + ID(1) + Security(1)
			if (0 == data_cnt)
 				return 0;

			tmp_len = (pVar->pu8RecvBuffer[StartPos+5+1] << 8) + pVar->pu8RecvBuffer[StartPos+4+1] - 2;

			i = StartPos = tmp_len - (pVar->pu8RecvBuffer[data_cnt] % tmp_len);

			pVar->u16CmdCur = 0;
			while (i > 0 && i < tmp_len)
			{
				pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+1+i];
				i++;
			}
		}
		//else
		//{
		//	recv_len -= 7;	// 去掉 Len1(2) + ID(1) + Security(1) + TID(2) + BCC(1)
		//}

		// 前面移位放到 buffer
		if (StartPos == 0)
			StartPos = ((pVar->pu8RecvBuffer[5+1] << 8) + pVar->pu8RecvBuffer[4+1]) - 2;

		for (i = 0; i < StartPos; i++)
			pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+1+i];

		//20140710 檢查fc
		if(check_fc != pVar->pu8DataBuf[0])
		{
			//若無資料重收一次
			if((EndPos + 1) == recv_len)
			{
				//Sleep(500);
				//delay = 500;
				goto RE_RECEIVE;
			}

			//若資料段連接在event 後則再重新解析一次
			StartPos = EndPos;
			goto RECHECK_CMD;
		}

		memcpy(pu8DataBuf, pVar->pu8DataBuf, tmp_len);
		if (pVar->pu8DataBuf[1] != 0)
			pu8DataBuf[0] = ~pVar->pu8DataBuf[0];

		for (i = 1; i < tmp_len - 1; i++)
			pu8DataBuf[i] = pVar->pu8DataBuf[i + 1];

		// 調整成符合第一代回應的格式
		if (pVar->pu8DataBuf[1] != 0)
			pVar->pu8DataBuf[0] = ~pVar->pu8DataBuf[0];

		for (i = 1; i < tmp_len - 1; i++)
			pVar->pu8DataBuf[i] = pVar->pu8DataBuf[i + 1];

		//若所收到的資料還沒解完就送到event buffer 去等待解析
		if((EndPos + 1) != recv_len)
		{
			memcpy ( pVar->pu8EventBuffer, pVar->pu8RecvBuffer + (EndPos+ 1), ((recv_len - (EndPos+ 1) ) )) ;
			pVar->u16EventBufferLen = recv_len - (EndPos+ 1);
		}

		//memcpy(pu8DataBuf, pVar->pu8DataBuf, i);

		return (unsigned short)(tmp_len - 1);
	}

	unsigned short DoCommand_NFC_P2P(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay)
	{
		unsigned int i;
		unsigned long t;
		unsigned int recv_len = 0, tmp_len;
		unsigned int StartPos;
		unsigned char chk = 0;
		unsigned char recv_stt = 0, GetDLE;
		//unsigned char pu8SendBuffer[256] = {0};
		//unsigned char pu8RecvBuffer[256] = {0};
		
		unsigned char pu8DataBuf[2048] = 
		{
			0
		};
		

		unsigned char recv_stt_end = 0xFF;
		unsigned int p2p_len_recv = 0;
		unsigned int p2p_count_recv = 0;

		pVar->u8RdStt = LRSUCCESS;
		switch (pVar->u8SecurityMode)
		{
		case SECUR_MODE_NONE:
		case SECUR_MODE_DEFAULT:
			break;
		case SECUR_MODE_READER_SEL_NONE:
		case SECUR_MODE_READER_SEL_ROLLCODE:
		case SECUR_MODE_READER_SEL_DES:
			return DoCommandReaderSelect(pVar, u16DataLen, delay);
		default:
			return 0;
		}

		pVar->u16CmdCur = 0;

		if (!pVar->pu8DataBuf || !pVar->pu8SendBuffer || !pVar->pu8RecvBuffer)
			return 0;

		//memcpy(pu8DataBuf, pVar->pu8DataBuf, u16DataLen);

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = STX_NFC_P2P; // 加入起始字元

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = (u16DataLen & 0xFF);
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = ( (u16DataLen >> 8) & 0xFF);
		
		for( i=0; i<u16DataLen; i++ )
		{
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = pVar->pu8DataBuf[ i ];
			chk ^= pVar->pu8DataBuf[ i ];
		}

		pVar->pu8SendBuffer[pVar->u16CmdCur++] = chk;

		if (SendFunc(pVar->u16DeviceNum, pVar->pu8SendBuffer, (unsigned short)pVar->u16CmdCur) != pVar->u16CmdCur)
			return 0;

		chk = 0;

		t = GetTickFunc();
		while ((GetTickFunc() - t) < delay)
		{
			tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, (unsigned short)(LIB_BUFFER_MAX - recv_len));

			if( tmp_len > LIB_BUFFER_MAX )
				break;

			for (i = recv_len; i < (recv_len + tmp_len); i++)
			{
				switch (recv_stt)
				{
				case 0:
					if(pVar->pu8RecvBuffer[i] == STX_NFC)
					{
						recv_stt_end = 2;
						recv_stt = 1;
						GetDLE = 0;
					}
					else if( pVar->pu8RecvBuffer[i] == STX_NFC_P2P )
					{
						recv_stt_end = 7;
						recv_stt = 3;
					}
					break;
				case 1:
					if (pVar->pu8RecvBuffer[i] == ETX && !GetDLE)
						recv_stt++;

					if (pVar->pu8RecvBuffer[i] == DLE && !GetDLE)
						GetDLE = 1;
					else
						GetDLE = 0;
					break;

				case 3:
					p2p_len_recv += pVar->pu8RecvBuffer[i];
					recv_stt++;
					
					pVar->pu8DataBuf[ p2p_count_recv++ ] = MPRD_RCDETECT;
					pVar->pu8DataBuf[ p2p_count_recv++ ] = 0;
					pVar->pu8DataBuf[ p2p_count_recv++ ] = 0x10;

					p2p_len_recv += 3;
					break;

				case 4:
					p2p_len_recv += ((unsigned int)pVar->pu8RecvBuffer[i] << 8);
					recv_stt++;
					break;

				case 5:
					chk ^= pVar->pu8RecvBuffer[i];
					pVar->pu8DataBuf[ p2p_count_recv++ ] = pVar->pu8RecvBuffer[i];
					if( p2p_count_recv >= p2p_len_recv )
					{
						recv_stt++;	
					}
					break;

				case 6:
					if( 0xFF == recv_stt_end )
					{
						break;
					}

					if( chk != pVar->pu8RecvBuffer[i] )
					{
						recv_stt_end = 0xFF;
						break;
					}
					recv_stt++;	
					break;
				}
			}

			recv_len += (i - recv_len);
			if (recv_stt == recv_stt_end)
				break;
			if (GetTickFunc() < t)
				t = GetTickFunc();

			if (tmp_len != 0)
			{
				t = GetTickFunc();
				delay = RECV_DELAY2;
			}

			if (recv_len > LIB_BUFFER_MAX)
				return 0;
		}

		if( recv_stt_end == 7 )
		{
			return p2p_count_recv;
		}

		StartPos = 0;
		while (pVar->pu8RecvBuffer[StartPos] != STX_NFC && StartPos < recv_len)
			//找出起始字元
			StartPos++;

		if (StartPos == recv_len)
			//若找不到起始字元則返回
			return 0;

		if (StartPos > 0 && pVar->pu8RecvBuffer[StartPos - 1] == DLE)
			//若找到起始字元，但起始字元前面是DLE返回
			return 0;

		if (pVar->pu8RecvBuffer[StartPos] != STX_NFC || pVar->pu8RecvBuffer[recv_len - 1] != ETX)
			//若開頭不是起始字元，結尾不是結束字元也返回
			return 0;

		//memcpy(pu8RecvBuffer, pVar->pu8RecvBuffer, recv_len);

		// 去掉DLE
		pVar->u16CmdCur = 0;
		for (i = (StartPos + 1); i < (recv_len - 1); i++)
		{
			if (pVar->pu8RecvBuffer[i] == DLE)
				i++;
			pVar->pu8RecvBuffer[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[i];
		}

		// 驗證檢查碼是否正確
		chk = 0;
		recv_len = pVar->u16CmdCur;
		for (i = 0; i < (recv_len - 1); i++)
			chk ^= pVar->pu8RecvBuffer[i];

		if (pVar->pu8RecvBuffer[i] != chk)
			return 0;

		//memcpy(pu8RecvBuffer, pVar->pu8RecvBuffer, recv_len);
		StartPos = 0;
		if (SECUR_MODE_DEFAULT == pVar->u8SecurityMode)
		{
			recv_len -= 3; // 去掉 Len1(2) + ID(1) + Security(1)

			if (0 == recv_len)
				return 0;

			tmp_len = (pVar->pu8RecvBuffer[5] << 8) + pVar->pu8RecvBuffer[4] - 2;
			i = StartPos = tmp_len - (pVar->pu8RecvBuffer[recv_len] % tmp_len);

			pVar->u16CmdCur = 0;
			while (i > 0 && i < tmp_len)
			{
				pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+i];
				i++;
			}
		}
		//else
		//{
		//	recv_len -= 7;	// 去掉 Len1(2) + ID(1) + Security(1) + TID(2) + BCC(1)
		//}

		// 前面移位放到 buffer
		if (StartPos == 0)
			StartPos = ((pVar->pu8RecvBuffer[5] << 8) + pVar->pu8RecvBuffer[4]) - 2;

		for (i = 0; i < StartPos; i++)
			pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+i];

		memcpy(pu8DataBuf, pVar->pu8DataBuf, tmp_len);
		if (pVar->pu8DataBuf[1] != 0)
			pu8DataBuf[0] = ~pVar->pu8DataBuf[0];

		for (i = 1; i < tmp_len - 1; i++)
			pu8DataBuf[i] = pVar->pu8DataBuf[i + 1];

		// 調整成符合第一代回應的格式
		if (pVar->pu8DataBuf[1] != 0)
			pVar->pu8DataBuf[0] = ~pVar->pu8DataBuf[0];

		for (i = 1; i < tmp_len - 1; i++)
			pVar->pu8DataBuf[i] = pVar->pu8DataBuf[i + 1];

		//memcpy(pu8DataBuf, pVar->pu8DataBuf, i);

		return (unsigned short)(tmp_len - 1);
	}

	unsigned short ReceiveEventRaise(PLIBVAR pVar, unsigned long delay)
	{
		unsigned int i;
		unsigned long t;
		unsigned int recv_len = 0, tmp_len;
		unsigned int StartPos;
		unsigned char chk = 0;
		unsigned char recv_stt = 0, GetDLE;
		//unsigned char pu8RecvBuffer[256] = {0};
		//unsigned char pu8DataBuf[256] = {0};

		unsigned char recv_stt_end = 0xFF;
		unsigned int p2p_len_recv = 0;
		unsigned int p2p_count_recv = 0;


		pVar->u8RdStt = LRSUCCESS;

		pVar->u16CmdCur = 0;

		//if (!pVar->pu8DataBuf || !pVar->pu8SendBuffer || !pVar->pu8RecvBuffer)
		if (!pVar->pu8EventBuffer || !pVar->pu8DataBuf || !pVar->pu8SendBuffer || !pVar->pu8RecvBuffer)
			return 0;

		t = GetTickFunc();
		while ((GetTickFunc() - t) < delay)
		{
// 			tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, (unsigned short)(LIB_BUFFER_MAX - recv_len));
// 			if (tmp_len == 0)
// 				return 0;
 
			if ( pVar->u16EventBufferLen != 0 ) //20140711
			{
				tmp_len = pVar->u16EventBufferLen;
				memcpy (pVar->pu8RecvBuffer, pVar->pu8EventBuffer, tmp_len);
				pVar->u16EventBufferLen = 0;
			}
			else
			{
				tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, (unsigned short)(LIB_BUFFER_MAX - recv_len));
				if (tmp_len == 0)
					return 0;
			}

			for (i = recv_len; i < (recv_len + tmp_len); i++)
			{
				switch (recv_stt)
				{
				case 0:
					if(pVar->pu8RecvBuffer[i] == STX_NFC)
					{
						recv_stt_end = 2;
						recv_stt = 1;
						GetDLE = 0;
					}
					else if( pVar->pu8RecvBuffer[i] == STX_NFC_P2P )
					{
						recv_stt_end = 7;
						recv_stt = 3;
					}
					break;
				case 1:
					if (pVar->pu8RecvBuffer[i] == ETX && !GetDLE)
						recv_stt++;

					if (pVar->pu8RecvBuffer[i] == DLE && !GetDLE)
						GetDLE = 1;
					else
						GetDLE = 0;
					break;

				case 3:
					p2p_len_recv += pVar->pu8RecvBuffer[i];
					recv_stt++;
					break;

				case 4:
					p2p_len_recv += pVar->pu8RecvBuffer[i];
					recv_stt++;
					
					pVar->pu8DataBuf[ p2p_count_recv++ ] = MPRD_RCDETECT;
					pVar->pu8DataBuf[ p2p_count_recv++ ] = 0;
					pVar->pu8DataBuf[ p2p_count_recv++ ] = 0x10;

					p2p_len_recv += 3;
					break;

				case 5:
					chk ^= pVar->pu8RecvBuffer[i];
					pVar->pu8DataBuf[ p2p_count_recv++ ] = pVar->pu8RecvBuffer[i];
					if( p2p_count_recv >= p2p_len_recv )
					{
						recv_stt++;	
					}
					break;

				case 6:
					if( 0xFF == recv_stt_end )
					{
						break;
					}

					if( chk != pVar->pu8RecvBuffer[i] )
					{
						recv_stt_end = 0xFF;
						break;
					}
					recv_stt++;	
					break;
				}
			}

			recv_len += (i - recv_len);
			if (recv_stt == recv_stt_end)
				break;
			if (GetTickFunc() < t)
				t = GetTickFunc();

			if (tmp_len != 0)
			{
				t = GetTickFunc();
				delay = RECV_DELAY2;
			}

			if (recv_len > LIB_BUFFER_MAX)
				return 0;
		}

		if( recv_stt_end == 7 )
		{
			return p2p_count_recv;
		}

		StartPos = 0;
		while (pVar->pu8RecvBuffer[StartPos] != STX_NFC && StartPos < recv_len)
			//找出起始字元
			StartPos++;

		if (StartPos == recv_len)
			//若找不到起始字元則返回
			return 0;

		if (StartPos > 0 && pVar->pu8RecvBuffer[StartPos - 1] == DLE)
			//若找到起始字元，但起始字元前面是DLE返回
			return 0;

		if (pVar->pu8RecvBuffer[StartPos] != STX_NFC || pVar->pu8RecvBuffer[recv_len - 1] != ETX)
			//若開頭不是起始字元，結尾不是結束字元也返回
			return 0;

		//memcpy(pu8RecvBuffer, pVar->pu8RecvBuffer, recv_len);

		// 去掉DLE
		pVar->u16CmdCur = 0;
		for (i = (StartPos + 1); i < (recv_len - 1); i++)
		{
			if (pVar->pu8RecvBuffer[i] == DLE)
				i++;
			pVar->pu8RecvBuffer[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[i];
		}

		// 驗證檢查碼是否正確
		chk = 0;
		recv_len = pVar->u16CmdCur;
		for (i = 0; i < (recv_len - 1); i++)
			chk ^= pVar->pu8RecvBuffer[i];

		if (pVar->pu8RecvBuffer[i] != chk)
			return 0;

		//memcpy(pu8RecvBuffer, pVar->pu8RecvBuffer, recv_len);
		StartPos = 0;
		if (SECUR_MODE_DEFAULT == pVar->u8SecurityMode)
		{
			recv_len -= 3; // 去掉 Len1(2) + ID(1) + Security(1)

			if (0 == recv_len)
				return 0;

			tmp_len = (pVar->pu8RecvBuffer[5] << 8) + pVar->pu8RecvBuffer[4] - 2;
			i = StartPos = tmp_len - (pVar->pu8RecvBuffer[recv_len] % tmp_len);

			pVar->u16CmdCur = 0;
			while (i > 0 && i < tmp_len)
			{
				pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+i];
				i++;
			}
		}

		if (StartPos == 0)
			StartPos = ((pVar->pu8RecvBuffer[5] << 8) + pVar->pu8RecvBuffer[4]) - 2;
		for (i = 0; i < StartPos; i++)
			pVar->pu8DataBuf[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[6+i];

		//memcpy(pu8DataBuf, pVar->pu8DataBuf, i);
		return (unsigned short)tmp_len;
	}

	unsigned short DoCommandReaderSelect(PLIBVAR pVar, unsigned int u16DataLen, unsigned long delay)
	{
		unsigned int i;
		unsigned int len, recv_len, tmp_len, StartPos;
		unsigned char chk, GetDLE, recv_stt = 0;
		unsigned long t;


		len = 2+1+1+(u16DataLen) + 2; // Len + ID + Security + (FC + Data) + TransactionID
		if (JIN_PASSWDINIT == pVar->pu8DataBuf[0])
			len -= 2;
		// Len + ID + Security + FC + Data

		pVar->u16CmdCur = 0;

		//STX
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = STX;

		//LEN
		AddSendBuf((unsigned char)(len >> 8));
		AddSendBuf((unsigned char)(len &0xFF));

		//ID
		AddSendBuf(pVar->u8RdID);

		//Security
		if ((JIN_PASSWDINIT == pVar->pu8DataBuf[0]) || (JIN_GET_CONTROLLER_ROLL_CODE == pVar->pu8DataBuf[0]))
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = 0;
		else
			AddSendBuf(pVar->u8SecurityMode - SECUR_MODE_READER_SEL_NONE);

		//FC + Data
		for (i = 0; i < u16DataLen; i++)
			AddSendBuf(pVar->pu8DataBuf[i]);

		//TransactionID
		if (JIN_PASSWDINIT == pVar->pu8DataBuf[0])
			;
		else if (JIN_GET_CONTROLLER_ROLL_CODE == pVar->pu8DataBuf[0])
		{
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = 0;
			pVar->pu8SendBuffer[pVar->u16CmdCur++] = 0;
		}
		else
		{
			CalculateCode(&pVar->rCode);
			AddSendBuf(pVar->rCode.pu8Value[1]);
			AddSendBuf(pVar->rCode.pu8Value[0]);
		}

		//CHK
		chk = 0;
		for (i = 1; i < (pVar->u16CmdCur); i++)
		{
			if (DLE == pVar->pu8SendBuffer[i])
				i++;
			chk += pVar->pu8SendBuffer[i];
		}
		AddSendBuf(chk);

		//EXT
		pVar->pu8SendBuffer[pVar->u16CmdCur++] = ETX;

		if (SendFunc(pVar->u16DeviceNum, pVar->pu8SendBuffer, pVar->u16CmdCur) != pVar->u16CmdCur)
			return 0;

		recv_len = 0;
		t = GetTickFunc();
		while ((GetTickFunc() - t) < delay)
		{
			tmp_len = RecvFunc(pVar->u16DeviceNum, pVar->pu8RecvBuffer + recv_len, LIB_BUFFER_MAX - recv_len);
			for (i = recv_len; i < (recv_len + tmp_len); i++)
			{
				switch (recv_stt)
				{
				case 0:
					if (pVar->pu8RecvBuffer[i] == C_STX)
					{
						recv_stt++;
						GetDLE = 0;
					}
					break;
				case 1:
					if (pVar->pu8RecvBuffer[i] == ETX && !GetDLE)
						recv_stt++;

					if (pVar->pu8RecvBuffer[i] == DLE && !GetDLE)
						GetDLE = 1;
					else
						GetDLE = 0;
					break;
				}
			}

			recv_len += (i - recv_len);
			if (recv_stt == 2)
				break;
			if (GetTickFunc() < t)
				t = GetTickFunc();

			if (tmp_len != 0)
			{
				t = GetTickFunc();
				delay = RECV_DELAY2;
			}

			if (recv_len > LIB_BUFFER_MAX)
				return 0;
		}

		/*
		Start	Len	ID	Security	FC	Status	Data	TransactionID	Checksum	End
		C_STX	 2	1	   1    	1	  1		1		     2			   1		ETX   
		*/

		StartPos = 0;
		while ((pVar->pu8RecvBuffer[StartPos] != C_STX) && (StartPos < recv_len))
			StartPos++;
		//找出起始字元
		if (StartPos == recv_len)
			return 0;
		//若找不到起始字元則返回

		if (StartPos > 0 && pVar->pu8RecvBuffer[StartPos - 1] == DLE)
			return 0;
		//若找到起始字元，但起始字元前面是DLE返回
		if (pVar->pu8RecvBuffer[StartPos] != C_STX || pVar->pu8RecvBuffer[recv_len - 1] != ETX)
			return 0;
		//若開頭不是起始字元，結尾不是結束字元也返回

		/* 去掉DLE */
		pVar->u16CmdCur = 0;
		for (i = (StartPos + 1); i < (recv_len - 1); i++)
		{
			if (pVar->pu8RecvBuffer[i] == DLE)
				i++;
			pVar->pu8RecvBuffer[pVar->u16CmdCur++] = pVar->pu8RecvBuffer[i];
		}

		/* 驗證檢查碼是否正確 */
		chk = 0;
		recv_len = pVar->u16CmdCur;
		for (i = 0; i < (recv_len - 1); i++)
			chk += pVar->pu8RecvBuffer[i];
		if (pVar->pu8RecvBuffer[i] != chk)
			return 0;

		/* 獲取長度 */
		len = ((unsigned short)pVar->pu8RecvBuffer[0] << 8) + ((unsigned short)pVar->pu8RecvBuffer[1]);

		// Status
		pVar->u8RdStt = pVar->pu8RecvBuffer[5];

		// 驗證長度
		if ((recv_len - 1) != len)
			return 0;

		// 驗證讀卡機編號
		if (pVar->pu8RecvBuffer[2] != pVar->u8RdID)
			return 0;

		// FC
		if ((JIN_PASSWDINIT != pVar->pu8RecvBuffer[4]))
			len -= 7;
		else
			len -= 5;

		pVar->pu8DataBuf[0] = pVar->pu8RecvBuffer[4];
		for (i = 1; i < len; i++)
			pVar->pu8DataBuf[i] = pVar->pu8RecvBuffer[5+i];

		return len;
	}


	unsigned short MpRd_AntennaControl(PLIBVAR pVar, unsigned char u8Select)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ANTENNACONTROL;
		pVar->pu8DataBuf[i++] = u8Select;

		if (!(i = DoCommand(pVar, i, /*g_DelayTime*/ 1000L)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ANTENNACONTROL)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_DeviceControl(PLIBVAR pVar, unsigned char u8Type)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_DEVICECONTROL;
		pVar->pu8DataBuf[i++] = u8Type;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_DEVICECONTROL)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_DeviceBlinkControl(PLIBVAR pVar, unsigned char u8Type, unsigned char u8RedMs100, unsigned char u8GreenMs100, unsigned char u8YellowMs100, unsigned char u8SoundMs100)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_DEVICEBLINKCONTROL;
		pVar->pu8DataBuf[i++] = u8Type;
		pVar->pu8DataBuf[i++] = u8RedMs100;
		pVar->pu8DataBuf[i++] = u8GreenMs100;
		pVar->pu8DataBuf[i++] = u8YellowMs100;
		pVar->pu8DataBuf[i++] = u8SoundMs100;


		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_DEVICEBLINKCONTROL)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_GetControllerRollCode(PLIBVAR pVar)
	{
		unsigned long t;
		int i = 0;

		t = GetTickFunc();

		while ((GetTickFunc() - t) < 50L)
			if (GetTickFunc() < t)
				t = GetTickFunc();

		pVar->pu8DataBuf[i++] = JIN_GET_CONTROLLER_ROLL_CODE;
		if (pVar->u8SecurityMode > SECUR_MODE_DEFAULT)
			pVar->pu8DataBuf[i++] = pVar->u8SecurityMode - SECUR_MODE_READER_SEL_NONE;

		i = DoCommand(pVar, i, g_DelayTime);

		if (0 == i)
			return LRRDNORESP;

		if (pVar->pu8DataBuf[0] != JIN_GET_CONTROLLER_ROLL_CODE)
			return LRRDUNKNOWN;

		if (g_u8Protocol == 0)
		{
			if (9 == i)
			{
				pVar->rCode.RollCode.pu8RollCode[0] = pVar->pu8DataBuf[1] ^ pVar->pu8AccessCode[0];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[1] = pVar->pu8DataBuf[2] ^ pVar->pu8AccessCode[1];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[0] = pVar->pu8DataBuf[3] ^ pVar->pu8AccessCode[2];
				pVar->rCode.RollCode.pu8RollCode[3] = pVar->pu8DataBuf[4] ^ pVar->pu8AccessCode[3];
				pVar->rCode.RollCode.pu8RollCode[1] = pVar->pu8DataBuf[5] ^ pVar->pu8AccessCode[0];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[3] = pVar->pu8DataBuf[6] ^ pVar->pu8AccessCode[1];
				pVar->rCode.RollCode.pu8RollCode[2] = pVar->pu8DataBuf[7] ^ pVar->pu8AccessCode[2];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[2] = pVar->pu8DataBuf[8] ^ pVar->pu8AccessCode[3];
			}
			else
			{
				if (LRSUCCESS != pVar->u8RdStt)
					return LRRDUNKNOWN;
				pVar->u8SecurityMode = pVar->pu8DataBuf[1] + SECUR_MODE_READER_SEL_NONE;
				if (SECUR_MODE_READER_SEL_NONE == pVar->u8SecurityMode)
					return LRSUCCESS;

				pVar->rCode.RollCode.pu8RollCode[0] = pVar->pu8DataBuf[2] ^ pVar->pu8AccessCode[0];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[1] = pVar->pu8DataBuf[3] ^ pVar->pu8AccessCode[1];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[0] = pVar->pu8DataBuf[4] ^ pVar->pu8AccessCode[2];
				pVar->rCode.RollCode.pu8RollCode[3] = pVar->pu8DataBuf[5] ^ pVar->pu8AccessCode[3];
				pVar->rCode.RollCode.pu8RollCode[1] = pVar->pu8DataBuf[6] ^ pVar->pu8AccessCode[0];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[3] = pVar->pu8DataBuf[7] ^ pVar->pu8AccessCode[1];
				pVar->rCode.RollCode.pu8RollCode[2] = pVar->pu8DataBuf[8] ^ pVar->pu8AccessCode[2];
				pVar->rCode.RollCodeStep.pu8RollCodeStep[2] = pVar->pu8DataBuf[9] ^ pVar->pu8AccessCode[3];
			}

			if (pVar->rCode.RollCode.pu8RollCode[0] &0x01)
			{
				pVar->rCode.pu8Value[1] = pVar->rCode.RollCode.pu8RollCode[0] ^ pVar->rCode.RollCode.pu8RollCode[2];
				pVar->rCode.pu8Value[0] = pVar->rCode.RollCode.pu8RollCode[1] ^ pVar->rCode.RollCode.pu8RollCode[3];
			}
			else
			{

				pVar->rCode.pu8Value[0] = pVar->rCode.RollCode.pu8RollCode[0] ^ pVar->rCode.RollCode.pu8RollCode[3];
				pVar->rCode.pu8Value[1] = pVar->rCode.RollCode.pu8RollCode[1] ^ pVar->rCode.RollCode.pu8RollCode[2];
			}
		}

		return LRSUCCESS;
	}


	unsigned short MpRd_SetCommucation(PLIBVAR pVar, unsigned char u8Type)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SETCOMMUCATION;
		pVar->pu8DataBuf[i++] = u8Type;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SETCOMMUCATION)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ReaderInfo(PLIBVAR pVar, unsigned char *pu8SerialNum, unsigned char *pu8FirmwareVer)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_GETREADERSERIALNUM;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_GETREADERSERIALNUM)
			return LRRDUNKNOWN;

		memcpy(pu8SerialNum, &pVar->pu8DataBuf[1], READER_SERIAL_LEN);
		//20140402.02	Shawn	start
		//memcpy(pu8FirmwareVer, &pVar->pu8DataBuf[1+READER_SERIAL_LEN], FIRMWARE_VER_LEN);
		memcpy(pu8FirmwareVer, &pVar->pu8DataBuf[1+READER_SERIAL_LEN], FIRMWARE_VER_FULL_LEN);
		//20140402.02	Shawn	end

		return LRSUCCESS;
	}


	unsigned short MpRd_SetReaderSerialNum(PLIBVAR pVar, unsigned char *pu8SerialNum)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SETREADERSERIALNUM;
		memcpy(pVar->pu8DataBuf + i, pu8SerialNum, READER_SERIAL_LEN);
		i += READER_SERIAL_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SETREADERSERIALNUM)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_OpenCard(PLIBVAR pVar, unsigned char u1AutoFind, unsigned char *pu8Uid, unsigned char *pu8Atqa, unsigned char *pu8Sak)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_OPENCARD;
		pVar->pu8DataBuf[i++] = u1AutoFind;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_OPENCARD)
			return LRRDUNKNOWN;

		memset(pu8Uid, 0, sizeof(unsigned char) *UID_LEN);
		if (JIN_OPENCARD_V1_RET_LEN == i)
		{
			memcpy(pu8Uid, pVar->pu8DataBuf + 1, 4);
			pu8Uid[4] = (unsigned char)UID_END;

			pu8Atqa[0] = (ATQA_MIFAER_S50 >> 8);
			pu8Atqa[1] = (ATQA_MIFAER_S50 &0xFF);
			*pu8Sak = MIFARE_ULTRA_LIGHT; //SAK_ISO14443_3;
			return LRSUCCESS;
		}

		memcpy(pu8Uid, pVar->pu8DataBuf + 1, i - 2-2);
		pu8Uid[i - 2-2] = (unsigned char)UID_END;
		memcpy(pu8Atqa, pVar->pu8DataBuf + 1+i - 2-2, 2);
		memcpy(pu8Sak, pVar->pu8DataBuf + 1+i - 2, 1);
		return LRSUCCESS;
	}

	unsigned short MpRd_CloseCard(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_CLOSECARD;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_CLOSECARD)
		{
			//20140410 START
			//20140410 Sandy 與 AL 討論 cover PNS544無法 close card 問題，用read module type 當為 544 就回應成功。
			int j = 0;
			pVar->pu8DataBuf[j++] = JIN_READMODULETYPE;

			if (!(j = DoCommand(pVar, j, g_DelayTime)))
				NoResponseProcess();
			if (pVar->pu8DataBuf[0] != JIN_READMODULETYPE)
				return LRRDUNKNOWN;
			if ( 0x80 == pVar->pu8DataBuf[1] )
				//return LRSUCCESS;
				return LRMODULENOSUPPORT;
			else
			//20140410 END
				return LRRDUNKNOWN;
		}

		return LRSUCCESS;
	}


	unsigned short MpRd_ReadMifareOneBlock ( PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned char *pu8Data)
	{
		// v3.0.0.5版本再改版底層由command方式發送
		// 	if (g_u8Protocol == 0)
		// 	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_READMIFAREONEBLOCK;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_READMIFAREONEBLOCK)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, BLOCK_LEN);
		return LRSUCCESS;
		// 	}
		// 	else
		// 	{
		// 		unsigned int i = 0;
		// 		unsigned short ret;
		// 		unsigned char pu8Uid[255] = { 0 };
		// 		unsigned char atqa[ATQA_LEN], sak;
		// 
		// 		if( (ret = MpRd_OpenCard( pVar, 0,  pu8Uid, atqa, & sak)) != 0)
		// 		{
		// 			return ret;
		// 		}
		// 
		// 		if (u1KeyType == 0x00)//(unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_A
		// 		{
		// 			ret = NFC_MifareAuthenticationKeyA ( pVar , u8Block, pu8Uid , pu8Key);
		// 		}
		// 		else if (u1KeyType == 0x01)// (unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_B
		// 		{
		// 			ret = NFC_MifareAuthenticationKeyB ( pVar , u8Block, pu8Uid , pu8Key);
		// 		}
		// 		else
		// 		{
		// 			return LRKEYCARD;
		// 		}
		// 
		// 		if(ret != 0)//RDINT_RESULT.LRSUCCESS
		// 		{
		// 			return ret;
		// 		}
		// 
		// 		pu8Data[i] = 0x30;//16 bytes reading
		// 		++i;
		// 
		// 		pu8Data[i] = u8Block;
		// 		++i;
		// 
		// 		memcpy(pu8Data + i, pu8Uid, 4);
		// 		i+=4;
		// 
		// 		memcpy(pu8Data + i, pu8Key, 6);
		// 		i+=6;
		// 
		// 		//cmd blkNo Uid   Key
		// 		// 1 + 1  +  4  +  6
		// 		//i = 12;
		// 		return NFC_SendMifareCommand(pVar, pu8Data, &i);
		// 	}
	}


	unsigned short MpRd_ReadMifareOneSector(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Sector, unsigned char *pu8Key, unsigned char *pu8Data)
	{
		// v3.0.0.5版本再改版底層由command方式發送
		// 	  if (g_u8Protocol == 0)
		// 	  {
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_READMIFAREONESECTOR;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Sector;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_READMIFAREONESECTOR)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, SECTOR_LEN);

		return LRSUCCESS;
		// 	  }
		// 	  else
		// 	  {
		// 		  unsigned int i = 0;
		// 		  unsigned short ret;
		// 		  unsigned char pu8Uid[255] = { 0 };
		// 		  unsigned char atqa[ATQA_LEN], sak;
		// 
		// 		  if( (ret = MpRd_OpenCard( pVar, 0,  pu8Uid, atqa, & sak)) != 0)
		// 		  {
		// 			  return ret;
		// 		  }
		// 
		// 		  if (u1KeyType == 0x00)//(unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_A
		// 		  {
		// 			  ret = NFC_MifareAuthenticationKeyA ( pVar , u8Sector, pu8Uid , pu8Key);
		// 		  }
		// 		  else if (u1KeyType == 0x01)// (unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_B
		// 		  {
		// 			  ret = NFC_MifareAuthenticationKeyB ( pVar , u8Sector, pu8Uid , pu8Key);
		// 		  }
		// 		  else
		// 		  {
		// 			  return LRKEYCARD;
		// 		  }
		// 
		// 		  if(ret != 0)//RDINT_RESULT.LRSUCCESS
		// 		  {
		// 			  return ret;
		// 		  }
		// 
		// 		  pu8Data[i] = 0x38;//Read sector
		// 		  ++i;
		// 
		// 		  pu8Data[i] = u8Sector;
		// 		  ++i;
		// 
		// 		  memcpy(pu8Data + i, pu8Uid, 4);
		// 		  i+=4;
		// 
		// 		  memcpy(pu8Data + i, pu8Key, 6);
		// 		  i+=6;
		// 
		// 		  //cmd blkNo Uid   Key
		// 		  // 1 + 1  +  4  +  6
		// 		  //i = 12;
		// 		  return NFC_SendMifareCommand(pVar, pu8Data, &i);
		// 	  }
	}


	unsigned short MpRd_WriteMifareOneBlock(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned char *pu8Data)
	{
		// v3.0.0.5版本再改版底層由command方式發送
		// 	  if (g_u8Protocol == 0)
		// 	  {
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_WRITEMIFAREONEBLOCK;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		memcpy(pVar->pu8DataBuf + i, pu8Data, BLOCK_LEN);
		i += BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_WRITEMIFAREONEBLOCK)
			return LRRDUNKNOWN;
		// 	  }
		// 	  else
		// 	  {
		// 		  unsigned int i = 0;
		// 		  unsigned short ret;
		// 		  unsigned char pu8Uid[255] = { 0 };
		// 		  unsigned char atqa[ATQA_LEN], sak;
		// 
		// 		  unsigned char pu8BlkDataTmp[BLOCK_LEN];
		// 		  memcpy( pu8BlkDataTmp, pu8Data, BLOCK_LEN);
		// 
		// 		  if( (ret = MpRd_OpenCard( pVar, 0,  pu8Uid, atqa, & sak)) != 0)
		// 		  {
		// 			  return ret;
		// 		  }
		// 
		// 		  if (u1KeyType == 0x00)//(unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_A
		// 		  {
		// 			  ret = NFC_MifareAuthenticationKeyA ( pVar , u8Block, pu8Uid , pu8Key);
		// 		  }
		// 		  else if (u1KeyType == 0x01)// (unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_B
		// 		  {
		// 			  ret = NFC_MifareAuthenticationKeyB ( pVar , u8Block, pu8Uid , pu8Key);
		// 		  }
		// 		  else
		// 		  {
		// 			  return LRKEYCARD;
		// 		  }
		// 
		// 		  if(ret != 0)//RDINT_RESULT.LRSUCCESS
		// 		  {
		// 			  return ret;
		// 		  }
		// 
		// 		  pu8Data[0] = 0xA0;//16 bytes writing
		// 		  ++i;
		// 
		// 		  pu8Data[1] = u8Block;
		// 		  ++i;
		// 
		// 		  memcpy(pu8Data + 2, pu8BlkDataTmp, BLOCK_LEN);
		// 
		// 		  //cmd blkNo Uid   Key
		// 		  // 1 + 1  +  4  +  6
		// 		  //i = 12;
		// 		  return NFC_SendMifareCommand(pVar, pu8Data, &i);
		// 	  }
		return LRSUCCESS;
	}

	unsigned short MpRd_PurseInit(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32WriteValue)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1

		// Fong 20090212 (Start)
		// 修改：初始值不能大於 2147483647(0x7FFFFFFF)

		if (u32WriteValue > 2147483647)
			return LRPURSE;
		//初始值錯誤

		// Fong 20090212 (End)

		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_MONEYINIT;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		memcpy(pVar->pu8DataBuf + i, &u32WriteValue, 4);
		i += 4;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MONEYINIT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_PurseRead(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long *pu32ReadValue)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_MONEYREAD;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MONEYREAD)
			return LRRDUNKNOWN;

		memcpy(pu32ReadValue, pVar->pu8DataBuf + 1, 4);
		return LRSUCCESS;
	}


	unsigned short MpRd_PurseInc(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32IncValue)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1

		// Fong 20090212 (Start)
		// 修改：(原值 + 增加值)不能大於 2147483647(0x7FFFFFFF)
		unsigned long Value = 0;

		MpRd_PurseRead(pVar, u1KeyType, u1DefaultKey, u8DefaultKeyIndx, u8Block, pu8Key, &Value);

		if ((u32IncValue + Value) > 2147483647)
			return LRINCR;
		//加值錯誤

		// Fong 20090212 (End)

		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_MONEYINC;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		memcpy(pVar->pu8DataBuf + i, &u32IncValue, 4);
		i += 4;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MONEYINC)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_PurseDec(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Block, unsigned char *pu8Key, unsigned long u32DecValue)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1

		// Fong 20090212 (Start)
		// 修改：減值 不能小於電子錢內的值
		unsigned long Value = 0;

		MpRd_PurseRead(pVar, u1KeyType, u1DefaultKey, u8DefaultKeyIndx, u8Block, pu8Key, &Value);

		if (u32DecValue > Value)
			return LRDECR;
		//減值錯誤

		// Fong 20090212 (End)

		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_MONEYDEC;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Block;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		memcpy(pVar->pu8DataBuf + i, &u32DecValue, 4);
		i += 4;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MONEYDEC)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_PurseBackup(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8SrcBlock, unsigned char u8DestBlock, unsigned char *pu8Key)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_MONEYBACKUP;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8SrcBlock;
		pVar->pu8DataBuf[i++] = u8DestBlock;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MONEYBACKUP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_KeyChange(PLIBVAR pVar, unsigned char u1KeyType, unsigned char u1DefaultKey, unsigned char u8DefaultKeyIndx, unsigned char u8Sector, unsigned char *pu8Key, unsigned char *pu8Data)
	{
		int i = 0;
		unsigned char b = u1KeyType; //KeyA:0	KeyB:1


		b |= (u1DefaultKey << 1);
		b |= (u8DefaultKeyIndx << 2);

		pVar->pu8DataBuf[i++] = JIN_KEYCHANGE;
		pVar->pu8DataBuf[i++] = b;
		pVar->pu8DataBuf[i++] = u8Sector;

		if (TURN_ON == u1DefaultKey || NULL == pu8Key)
			memset(pVar->pu8DataBuf + i, 0, CARD_KEY_LEN);
		else
			memcpy(pVar->pu8DataBuf + i, pu8Key, CARD_KEY_LEN);
		i += CARD_KEY_LEN;

		memcpy(pVar->pu8DataBuf + i, pu8Data, BLOCK_LEN);
		i += BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_KEYCHANGE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_WriteDefaultKey(PLIBVAR pVar, unsigned char u8DefaultKeyIndx, unsigned char *pu8DefaultKey)
	{
		int i = 0;

		//20150111
		if(u8DefaultKeyIndx > 31)
		{
			return LRPARAMETEROVERRANGE;
		}

		pVar->pu8DataBuf[i++] = JIN_WRITEDEFAULEKEY;
		pVar->pu8DataBuf[i++] = u8DefaultKeyIndx;
		memcpy(pVar->pu8DataBuf + i, pu8DefaultKey, 6);
		i += 6;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_WRITEDEFAULEKEY)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ReadUltraLight(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_READULTRALIGHT;
		pVar->pu8DataBuf[i++] = u8Block;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_READULTRALIGHT)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, 0x10);

		return LRSUCCESS;
	}


	unsigned short MpRd_WriteUltraLight(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_WRITEULTRALIGHT;
		pVar->pu8DataBuf[i++] = u8Block;

		memcpy(pVar->pu8DataBuf + i, pu8Data, 0x04);
		i += 4;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_WRITEULTRALIGHT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_KbStart(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_KBSTART;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_KBSTART)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_KbStop(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_KBSTOP;

		if (!(i = DoCommand(pVar, i, /*g_DelayTime*/1000L)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_KBSTOP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_KbRead(PLIBVAR pVar, unsigned char *pu8Kb)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_KBREAD;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (i > JIN_KBREAD_RET_LEN_MAX || i < JIN_KBREAD_RET_LEN_MIN)
			return LRRDUNKNOWN;
		if (pVar->pu8DataBuf[0] != JIN_KBREAD)
			return LRRDUNKNOWN;

		memcpy(pu8Kb, pVar->pu8DataBuf + 1, KB_LEN);

		return LRSUCCESS;
	}

	unsigned short MpRd_ButtonCheck(PLIBVAR pVar, unsigned char *pu1Status)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_BUTTONCHECK;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_BUTTONCHECK)
			return LRRDUNKNOWN;

		*pu1Status = pVar->pu8DataBuf[1];

		return LRSUCCESS;
	}


	unsigned short MpRd_PasswordSet(PLIBVAR pVar, unsigned char *pu8OldPasswd, unsigned char *pu8NewPasswd)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_PASSWDSET;
		memcpy(pVar->pu8DataBuf + i, pu8OldPasswd, 4);
		i += 4;
		memcpy(pVar->pu8DataBuf + i, pu8NewPasswd, 4);
		i += 4;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_PASSWDSET)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_PasswordInit(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_PASSWDINIT;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			return LRRDLINKLOST;
		if (pVar->pu8DataBuf[0] != JIN_PASSWDINIT)
			return LRRDUNKNOWN;

		return LRSUCCESS;

	}


	unsigned short MpRd_WorkingType(PLIBVAR pVar, unsigned char u8Type)
	{
		int i = 0;

		g_WorkType = u8Type;
		if (u8Type == WT_SR176_SRIX4K)
			g_WorkType = WT_ISO14443_TYPEB;

		pVar->pu8DataBuf[i++] = JIN_WORKINGTYPE;
		pVar->pu8DataBuf[i++] = u8Type;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_WORKINGTYPE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Inventory(PLIBVAR pVar, unsigned char u8Flag, unsigned char u8Afi, unsigned char u8MaskLen, unsigned char *pu8Mask, unsigned char *pu8Dsfid, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693INVENTORY;
		pVar->pu8DataBuf[i++] = u8Flag;
		pVar->pu8DataBuf[i++] = u8Afi;
		pVar->pu8DataBuf[i++] = u8MaskLen;

		memset(pVar->pu8DataBuf + i, 0, ISO15693_MASK_LEN);
		memcpy(pVar->pu8DataBuf + i, pu8Mask, u8MaskLen);
		i += ISO15693_MASK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693INVENTORY)
			return LRRDUNKNOWN;

		*pu8Dsfid = pVar->pu8DataBuf[1];
		memcpy(pu8Uid, pVar->pu8DataBuf + 2, ISO15693_UID_LEN);

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693StayQuiet(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693STAYQUIET;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693STAYQUIET)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Select(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693SELECT;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693SELECT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Reset2Ready(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693RESET2READY;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693RESET2READY)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Read(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8BlockStart, unsigned char u8BlockCount, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693READ;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8BlockStart;
		pVar->pu8DataBuf[i++] = u8BlockCount;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693READ)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, ISO15693_BLOCK_LEN *u8BlockCount);

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Write(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Block, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693WRITE;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8Block;
		memcpy(pVar->pu8DataBuf + i, pu8Data, ISO15693_BLOCK_LEN);
		i += ISO15693_BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693WRITE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_ICODE1Request(PLIBVAR pVar, unsigned char *pu8Uid)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_ICODE1REQUEST;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ICODE1REQUEST)
			return LRRDUNKNOWN;

		memset(pu8Uid, 0, sizeof(unsigned char) *8);

		memcpy(pu8Uid, pVar->pu8DataBuf + 1, 8);

		return LRSUCCESS;
	}

	unsigned short MpRd_ICODE1Read(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_ICODE1READ;
		pVar->pu8DataBuf[i++] = u8Block;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ICODE1READ)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, ISO15693_BLOCK_LEN);

		return LRSUCCESS;
	}

	unsigned short MpRd_ICODE1Write(PLIBVAR pVar, unsigned char u8Block, unsigned char *pu8Data)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_ICODE1WRITE;
		pVar->pu8DataBuf[i++] = u8Block;
		memcpy(pVar->pu8DataBuf + i, pu8Data, ISO15693_BLOCK_LEN);
		i += ISO15693_BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ICODE1WRITE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_ICODE1Halt(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_ICODE1HALT;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ICODE1HALT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_ISO15693UserCommand(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Manufacturer, unsigned char u8WaitTime, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8Recv, unsigned char *pu8RecvLen)
	{
		int i;
		unsigned long t = 302L;

		if (u8WaitTime != 0xFF)
		{
			for (i = 0; i < u8WaitTime; i++)
				t *= 2L;

			t /= 1000L;
			t += g_DelayTime;
		}
		else
			t = g_DelayTime;

		i = 0;
		pVar->pu8DataBuf[i++] = JIN_ISO15693USERCOMMAND;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8Manufacturer;
		memcpy(pVar->pu8DataBuf + i, pu8Cmd, u8CmdLen);
		i += u8CmdLen;

		if (!(i = DoCommand(pVar, i, t)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693USERCOMMAND)
			return LRRDUNKNOWN;

		*pu8RecvLen = i - 2;
		memcpy(pu8Recv, pVar->pu8DataBuf + 2,  *pu8RecvLen);

		return LRSUCCESS;
	}

	unsigned short MpRd_ISO15693LockBlock(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8Block)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693LOCKBLOCK;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8Block;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693LOCKBLOCK)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693WriteAfi(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8AfiValue)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693WRITEAFI;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8AfiValue;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693WRITEAFI)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693LockAfi(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693LOCKAFI;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693LOCKAFI)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693WriteDsfid(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8DsfidValue)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693WRITEDSFID;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8DsfidValue;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693WRITEDSFID)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693LockDsfid(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693LOCKDSFID;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693LOCKDSFID)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693GetSysInf(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char *pu8SysInf)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693GETSYSINF;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693GETSYSINF)
			return LRRDUNKNOWN;

		memcpy(pu8SysInf, pVar->pu8DataBuf + 1, ISO15693_SYSINF_LEN);
		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693GetBlockSecurity(PLIBVAR pVar, unsigned char u8Flag, unsigned char *pu8Uid, unsigned char u8BlockStart, unsigned char u8BlockCount, unsigned char *pu8SecurityData)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693GETBLOCKSECURITY;
		pVar->pu8DataBuf[i++] = u8Flag;

		memcpy(pVar->pu8DataBuf + i, pu8Uid, ISO15693_UID_LEN);
		i += ISO15693_UID_LEN;

		pVar->pu8DataBuf[i++] = u8BlockStart;
		pVar->pu8DataBuf[i++] = u8BlockCount;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693GETBLOCKSECURITY)
			return LRRDUNKNOWN;

		memcpy(pu8SecurityData, pVar->pu8DataBuf + 1, i - 1);
		return LRSUCCESS;
	}


	unsigned short MpRd_ISO14443A4Reset(PLIBVAR pVar, unsigned char *pu8GRLCardInf)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO14443A4RESET;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO14443A4RESET)
			return LRRDUNKNOWN;

		*pu8GRLCardInf = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_ISO14443B4Reset(PLIBVAR pVar, unsigned char u1FindType, unsigned char u8SlotNum, unsigned char u8Afi, unsigned char *pu8GRLCardInf)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO14443B4RESET;
		pVar->pu8DataBuf[i++] = u1FindType;
		pVar->pu8DataBuf[i++] = u8SlotNum;
		pVar->pu8DataBuf[i++] = u8Afi;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO14443B4RESET)
			return LRRDUNKNOWN;

		*pu8GRLCardInf = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_ISO14443AB4COS(PLIBVAR pVar, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8GRLCos)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_ISO14443AB4COS;
		memcpy(pVar->pu8DataBuf + i, pu8Cmd, u8CmdLen);
		i += u8CmdLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO14443AB4COS)
			return LRRDUNKNOWN;

		*pu8GRLCos = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_STCardSelect(PLIBVAR pVar, unsigned char *pu8IDNum)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_STCARDSELECT;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_STCARDSELECT)
			return LRRDUNKNOWN;

		*pu8IDNum = pVar->pu8DataBuf[1];
		return LRSUCCESS;
	}


	unsigned short MpRd_STCardIntoDeactive(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_STCARDINTODEACTIVE;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_STCARDINTODEACTIVE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SamReset(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMRESET;
		pVar->pu8DataBuf[i++] = u8SlotNo;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SAMRESET)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;
		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);

		return LRSUCCESS;
	}


	unsigned short MpRd_SamApdu(PLIBVAR pVar, unsigned char u8SlotNo, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMAPDU;
		pVar->pu8DataBuf[i++] = u8SlotNo;

		memcpy(pVar->pu8DataBuf + i, pu8ApduData, u16ApduLen);
		i += u16ApduLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SAMAPDU)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;

		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);
		return LRSUCCESS;
	}

	// Fong 20090629 Start
	unsigned short MpRd_SamBaudrate_1(PLIBVAR pVar, unsigned char u8Baud)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMBAUDRATE;
		pVar->pu8DataBuf[i++] = u8Baud;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != JIN_SAMBAUDRATE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_SamReset_1(PLIBVAR pVar, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMRESET;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != JIN_SAMRESET)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;
		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);

		return LRSUCCESS;
	}


	unsigned short MpRd_SamApdu_1(PLIBVAR pVar, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMAPDU;

		memcpy(pVar->pu8DataBuf + i, pu8ApduData, u16ApduLen);
		i += u16ApduLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SAMAPDU)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;

		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);

		return LRSUCCESS;
	}

	// Fong 20090629 End
	// Fong 20110310 Start
	unsigned short MpRd_SamBaudrate_2(PLIBVAR pVar, unsigned char u8Baud)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMBAUDRATE_2;
		pVar->pu8DataBuf[i++] = u8Baud;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != JIN_SAMBAUDRATE_2)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_SamReset_2(PLIBVAR pVar, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMRESET_2;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != JIN_SAMRESET_2)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;
		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);

		return LRSUCCESS;
	}


	unsigned short MpRd_SamApdu_2(PLIBVAR pVar, unsigned char *pu8ApduData, unsigned short u16ApduLen, unsigned char *pu8Resp, unsigned short *pu16RespLen)
	{
		int i = 0;

		if (g_DelayTime < 5000)
			g_DelayTime = 5000;

		pVar->pu8DataBuf[i++] = JIN_SAMAPDU_2;

		memcpy(pVar->pu8DataBuf + i, pu8ApduData, u16ApduLen);
		i += u16ApduLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SAMAPDU_2)
			return LRRDUNKNOWN;

		*pu16RespLen = (unsigned short)i - 1;

		memcpy(pu8Resp, pVar->pu8DataBuf + 1, i - 1);

		return LRSUCCESS;
	}

	// Fong 20110310 End
	unsigned short MpRd_RC632RegRead(PLIBVAR pVar, unsigned char u8RegSelect, unsigned char *pu8RegData)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_RC632REGREAD;
		pVar->pu8DataBuf[i++] = u8RegSelect;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_RC632REGREAD)
			return LRRDUNKNOWN;

		*pu8RegData = pVar->pu8DataBuf[1];

		return LRSUCCESS;
	}


	unsigned short MpRd_RC632RegWrite(PLIBVAR pVar, unsigned char u8RegSelect, unsigned char u8RegData)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_RC632REGWRITE;
		pVar->pu8DataBuf[i++] = u8RegSelect;
		pVar->pu8DataBuf[i++] = u8RegData;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_RC632REGWRITE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_RC632Cmd(PLIBVAR pVar, unsigned char u8WaitTime, unsigned char u8CmdType, unsigned char *pu8Cmd, unsigned char u8CmdLen, unsigned char *pu8Recv, unsigned char *pu8RecvLen)
	{
		int i;
		unsigned long t = 302L;

		if (u8WaitTime != 0xFF)
		{
			for (i = 0; i < u8WaitTime; i++)
				t *= 2L;

			t /= 1000L;
			t += g_DelayTime;
		}
		else
			t = g_DelayTime;

		i = 0;
		pVar->pu8DataBuf[i++] = JIN_RC632CMD;
		pVar->pu8DataBuf[i++] = u8WaitTime;
		pVar->pu8DataBuf[i++] = u8CmdType;
		pVar->pu8DataBuf[i++] = u8CmdLen;
		memcpy(pVar->pu8DataBuf + i, pu8Cmd, u8CmdLen);
		i += u8CmdLen;

		if (!(i = DoCommand(pVar, i, t)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_RC632CMD)
			return LRRDUNKNOWN;

		*pu8RecvLen = pVar->pu8DataBuf[1];
		memcpy(pu8Recv, pVar->pu8DataBuf + 2,  *pu8RecvLen);

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693Cmd(PLIBVAR pVar, unsigned char u8WaitTime, unsigned char u8CmdLen, unsigned char *pu8Cmd)
	{
		int i;
		unsigned long t = 302L;


		if (u8WaitTime != 0xFF)
		{
			for (i = 0; i < u8WaitTime; i++)
				t *= 2L;

			t /= 1000L;
			t += g_DelayTime;
		}
		else
			t = g_DelayTime;

		i = 0;
		pVar->pu8DataBuf[i++] = JIN_ISO15693CMD;
		pVar->pu8DataBuf[i++] = u8WaitTime;
		pVar->pu8DataBuf[i++] = u8CmdLen;

		memcpy(pVar->pu8DataBuf + i, pu8Cmd, u8CmdLen);
		i += u8CmdLen;

		if (!(i = DoCommand(pVar, i, t)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693CMD)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_AntennaAmount(PLIBVAR pVar, unsigned char u8AntennaSwitch)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ANTENNAAMOUNT;
		pVar->pu8DataBuf[i++] = u8AntennaSwitch;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ANTENNAAMOUNT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693AutoInventoryParam(PLIBVAR pVar, unsigned char u8Flag, unsigned char u8Afi, unsigned char u8MaskLen, unsigned char *pu8Mask)
	{
		int i = 0;



		pVar->pu8DataBuf[i++] = JIN_ISO15693AUTOINVENTORYPARAM;
		pVar->pu8DataBuf[i++] = u8Flag;
		pVar->pu8DataBuf[i++] = u8Afi;
		pVar->pu8DataBuf[i++] = u8MaskLen;

		memset(pVar->pu8DataBuf + i, 0, ISO15693_MASK_LEN);
		memcpy(pVar->pu8DataBuf + i, pu8Mask, u8MaskLen);
		i += ISO15693_MASK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693AUTOINVENTORYPARAM)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ISO15693AutoInventory4Antennas(PLIBVAR pVar, unsigned char *pu8GRLUid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ISO15693AUTOINVENTORY4ANTENNAS;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ISO15693AUTOINVENTORY4ANTENNAS)
			return LRRDUNKNOWN;

		*pu8GRLUid = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_MifareAutoRequest4Antennas(PLIBVAR pVar, unsigned char *pu8GRLCS)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_MIFAREAUTOREQUEST4ANTENNAS;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_MIFAREAUTOREQUEST4ANTENNAS)
			return LRRDUNKNOWN;

		*pu8GRLCS = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_GetUID(PLIBVAR pVar, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_GETUID;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_GETUID)
			return LRRDUNKNOWN;

		memcpy(pu8Uid, pVar->pu8DataBuf + 1, UID_LEN - 1);

		return LRSUCCESS;
	}


	unsigned short MpRd_RollCodeEnable(PLIBVAR pVar, unsigned char u1Enable)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_ROLLCODEENABLE;
		pVar->pu8DataBuf[i++] = u1Enable;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_ROLLCODEENABLE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_ReadModuleType(PLIBVAR pVar, unsigned char *pu8ModuleType)
	{
		int i = 0;

		//20150115	START
		unsigned short rtn = 0;

		BYTE pu8Recv[255], pu8CMD[2];
		pu8CMD[0] = 1; //LEN
		pu8CMD[1] = JIN_READMODULENAME;
		//get ModuleName of HW code
		rtn = MpRd_EngineerMode(pVar, pu8CMD, pu8Recv, 500);

		if( 0 == rtn /*&& pu8Recv[ 2 ] == 6*/ 
			&& pu8Recv[ 3 ] == 'M'
			&& pu8Recv[ 4 ] == 'P'
			&& pu8Recv[ 5 ] == '-'
			&& pu8Recv[ 6 ] == '5'
			&& pu8Recv[ 7 ] == '0'
			&& pu8Recv[ 8 ] == '1'			
			)//6：MP-501 with RC632
		{
			*pu8ModuleType = 4;
			return LRSUCCESS;
		}
		//20150115	END

		pVar->pu8DataBuf[i++] = JIN_READMODULETYPE;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_READMODULETYPE)
			return LRRDUNKNOWN;

		*pu8ModuleType = pVar->pu8DataBuf[1];

		return LRSUCCESS;
	}


	unsigned short MpRd_SetUnlockDock(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SETUNLOCKDOCK;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SETUNLOCKDOCK)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SetTimeOut(PLIBVAR pVar)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SETTIMEOUT;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SETTIMEOUT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SetBuzzerLed(PLIBVAR pVar, unsigned char u8TimeDelay)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SETBUZZERLED;
		pVar->pu8DataBuf[i++] = u8TimeDelay;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SETBUZZERLED)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	/*
	unsigned short MpRd_SetReadTagPulse(PLIBVAR pVar)
	{
	int i = 0;


	pVar->pu8DataBuf[i++]=JIN_SETREADTAGPULSE;

	if(!(i=DoCommand(pVar,i,RECV_DELAY))) NoResponseProcess();
	if(pVar->pu8DataBuf[0]!=JIN_SETREADTAGPULSE) return LRRDUNKNOWN;

	return LRSUCCESS;
	}
	*/

	unsigned short MpRd_CmdDispatch(PLIBVAR pVar, unsigned char *pu8SendCmd, unsigned char u8SendCmdLen, unsigned char *pu8GRLCmd)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_CMDDISPATCH;

		memcpy(pVar->pu8DataBuf + i, pu8SendCmd, u8SendCmdLen);
		i += u8SendCmdLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_CMDDISPATCH)
			return LRRDUNKNOWN;

		*pu8GRLCmd = i - 1;
		return LRSUCCESS;
	}


	unsigned short MpRd_ReaderRespTimeOut(PLIBVAR pVar, unsigned char u8TimeOut)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_READERRESPTIMEOUT;
		pVar->pu8DataBuf[i++] = u8TimeOut;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_READERRESPTIMEOUT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_EngineerMode(PLIBVAR pVar, unsigned char *pu8CMD, unsigned char *pu8Recv, unsigned long u32DelayMs)
	{
		int i = 0;


		memcpy(pVar->pu8DataBuf, pu8CMD + 1, pu8CMD[0]);
		i += pu8CMD[0];
		if (!(i = DoCommand(pVar, i, u32DelayMs)))
			NoResponseProcess();

		pu8Recv[0] = i + 1;
		memcpy(pu8Recv + 1, pVar->pu8DataBuf, i);
		return LRSUCCESS;
	}


	unsigned char MpRd_GetReturnData(PLIBVAR pVar, unsigned char u8Indx)
	{
		return pVar->pu8DataBuf[1+u8Indx];
	}


	void MpRd_GetReturnDataArray(PLIBVAR pVar, unsigned char u8Indx, unsigned char u8Offset, unsigned char *pu8Data)
	{
		memcpy(pu8Data, pVar->pu8DataBuf + 1+u8Indx, u8Offset);
	}


	void MpRd_SetLibAccessCode(PLIBVAR pVar, unsigned char *pu8AccessCode)
	{
		memcpy(pVar->pu8AccessCode, pu8AccessCode, PASSWD_LEN);
	}

	/*
	unsigned short MpRd_SetApdu(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned char u8Lc, void *pData, unsigned char *pu8Apdu)
	{
	pu8Apdu[ 0 ] = u8CLS;
	pu8Apdu[ 1 ] = u8INS;
	pu8Apdu[ 2 ] = u8P1;
	pu8Apdu[ 3 ] = u8P2;

	if( u8Lc == 0 ) return 4;
	pu8Apdu[ 4 ] = u8Lc;
	memcpy(&pu8Apdu[ 5 ],pData,u8Lc);

	return ((unsigned short)u8Lc + 5);
	}


	unsigned short MpRd_SetApduU32(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned long u32Data, unsigned char *pu8Apdu)
	{
	pu8Apdu[ 0 ] = u8CLS;
	pu8Apdu[ 1 ] = u8INS;
	pu8Apdu[ 2 ] = u8P1;
	pu8Apdu[ 3 ] = u8P2;

	if( u8Lc == 0 ) return 4;
	pu8Apdu[ 4 ] = 4;
	Reverse(&u32Data,4);
	memcpy(&pu8Apdu[ 5 ],&u32Data,4);

	return 9;
	}


	unsigned short MpRd_SetApduU16(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned short u16Data, unsigned char *pu8Apdu)
	{
	pu8Apdu[ 0 ] = u8CLS;
	pu8Apdu[ 1 ] = u8INS;
	pu8Apdu[ 2 ] = u8P1;
	pu8Apdu[ 3 ] = u8P2;

	if( u8Lc == 0 ) return 4;
	pu8Apdu[ 4 ] = 2;
	Reverse(&u16Data,2);
	memcpy(&pu8Apdu[ 5 ],&u16Data,2);

	return 7;
	}


	unsigned short MpRd_SetApduU8(unsigned u8CLS, unsigned char u8INS, unsigned char u8P1, unsigned char u8P2, unsigned char u8Data, unsigned char *pu8Apdu)
	{
	pu8Apdu[ 0 ] = u8CLS;
	pu8Apdu[ 1 ] = u8INS;
	pu8Apdu[ 2 ] = u8P1;
	pu8Apdu[ 3 ] = u8P2;

	if( u8Lc == 0 ) return 4;
	pu8Apdu[ 4 ] = 1;
	pu8Apdu[ 5 ] = u8Data;

	return 6;
	}
	*/

	unsigned short MpRd_SR176ReadBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SR176READBLOCK;
		pVar->pu8DataBuf[i++] = u8BlkNo;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SR176READBLOCK)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, i - 1);
		return LRSUCCESS;
	}


	unsigned short MpRd_SR176WriteBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SR176WRITEBLOCK;
		pVar->pu8DataBuf[i++] = u8BlkNo;

		memcpy(pVar->pu8DataBuf + i, pu8Data, SR176_BLOCK_LEN);
		i += SR176_BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SR176WRITEBLOCK)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SR176LockBlock(PLIBVAR pVar, unsigned char u8BlkNo)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SR176LOCKBLOCK;
		pVar->pu8DataBuf[i++] = u8BlkNo;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SR176LOCKBLOCK)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SRIX4KReadBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SRIX4KREADBLOCK;
		pVar->pu8DataBuf[i++] = u8BlkNo;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SRIX4KREADBLOCK)
			return LRRDUNKNOWN;

		memcpy(pu8Data, pVar->pu8DataBuf + 1, i - 1);
		return LRSUCCESS;
	}


	unsigned short MpRd_SRIX4KWriteBlock(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Data)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SRIX4KWRITEBLOCK;
		pVar->pu8DataBuf[i++] = u8BlkNo;

		memcpy(pVar->pu8DataBuf + i, pu8Data, SRIX4K_BLOCK_LEN);
		i += SRIX4K_BLOCK_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SRIX4KWRITEBLOCK)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SRIX4KAuth(PLIBVAR pVar, unsigned char *pu8Auth)
	{
		int i = 0;


		pVar->pu8DataBuf[i] = JIN_SRIX4KAUTH;

		memcpy(pVar->pu8DataBuf + i, pu8Auth, SRIX4K_RND_LEN);
		i += SRIX4K_RND_LEN;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SRIX4KAUTH)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}


	unsigned short MpRd_SRIX4KReadUID(PLIBVAR pVar, unsigned char *pu8Uid)
	{
		int i = 0;


		pVar->pu8DataBuf[i++] = JIN_SRIX4KREADUID;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_SRIX4KREADUID)
			return LRRDUNKNOWN;

		memcpy(pu8Uid, pVar->pu8DataBuf + 1, i - 1);
		return LRSUCCESS;
	}
	unsigned short MpRd_LCDClear(PLIBVAR pVar, unsigned char u8RowNo)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_LCDCLEAR;
		pVar->pu8DataBuf[i++] = u8RowNo;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_LCDCLEAR)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_LCDBacklight(PLIBVAR pVar, unsigned char u8Mode, unsigned char u8Time)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_LCDBACKLIGHT;
		pVar->pu8DataBuf[i++] = u8Mode;
		pVar->pu8DataBuf[i++] = u8Time;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_LCDBACKLIGHT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_LCDDisplayString(PLIBVAR pVar, unsigned char *pu8SendCmd, unsigned char u8SendCmdLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_LCDDIPLAYSTRING;

		memcpy(pVar->pu8DataBuf + i, pu8SendCmd, u8SendCmdLen);
		i += u8SendCmdLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_LCDDIPLAYSTRING)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}

	unsigned short MpRd_LCDDisplayPicture(PLIBVAR pVar, unsigned char u8Row, unsigned char u8Column, unsigned char u8Highlight, unsigned char u8High, unsigned char u8Width, unsigned char *pu8Picture, unsigned short u16PictureLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_LCDDIPLAYPICTURE;
		pVar->pu8DataBuf[i++] = u8Row - 1;
		pVar->pu8DataBuf[i++] = u8Column - 1;
		pVar->pu8DataBuf[i++] = u8Highlight;
		pVar->pu8DataBuf[i++] = u8High - 1;
		pVar->pu8DataBuf[i++] = u8Width - 1;

		memcpy(pVar->pu8DataBuf + i, pu8Picture, u16PictureLen);
		i += u16PictureLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_LCDDIPLAYPICTURE)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}



	// 檢查是否進入Isp模式
	unsigned short Up_ST_CheckInIsp(PLIBVAR pVar, unsigned char *pu8Mode)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_CHECK_IN_ISP;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_CHECK_IN_ISP)
			return LRRDUNKNOWN;

		*pu8Mode = pVar->pu8DataBuf[1];

		return LRSUCCESS;
	}
	// Jump to Isp
	unsigned short Up_ST_JumpToIsp(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_JUMP_TO_ISP;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_JUMP_TO_ISP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	// Jump to Application
	unsigned short Up_ST_JumpToApplication(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_JUMP_TO_AP;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_JUMP_TO_AP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	// Write Isp
	unsigned short Up_ST_WriteIsp(PLIBVAR pVar, unsigned short u8Page)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_WRITE_ISP;
		pVar->pu8DataBuf[i++] = (u8Page >> 8) &0xFF;
		pVar->pu8DataBuf[i++] = u8Page &0xFF;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_WRITE_ISP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	// Write Application
	unsigned short Up_ST_WriteAppliction(PLIBVAR pVar, unsigned short u8Page)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_WRITE_AP;
		pVar->pu8DataBuf[i++] = (u8Page >> 8) &0xFF;
		pVar->pu8DataBuf[i++] = u8Page &0xFF;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_WRITE_AP)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	// Check write ISP is OK
	unsigned short Up_ST_WriteIspOK(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_WRITE_ISP_OK;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_WRITE_ISP_OK)
			return LRRDUNKNOWN;

		if (pVar->pu8DataBuf[1] == 1)
			return LRSUCCESS;

		return LRRDUNKNOWN;
	}
	// Check write Application is OK
	unsigned short Up_ST_WriteApplicationOK(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_WRITE_AP_OK;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_WRITE_AP_OK)
			return LRRDUNKNOWN;

		if (pVar->pu8DataBuf[1] == 1)
			return LRSUCCESS;

		return LRRDUNKNOWN;
	}
	// Send Data
	unsigned short Up_ST_SendData(PLIBVAR pVar, unsigned char u8BlockNo, unsigned char *pu8Data)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = JIN_FWAP_SEND_DATA;
		pVar->pu8DataBuf[i++] = u8BlockNo;
		memcpy(pVar->pu8DataBuf + i, pu8Data, 128);
		i += 128;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		if (pVar->pu8DataBuf[0] != JIN_FWAP_SEND_DATA)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	//
	unsigned short NFC_StartAutoDetect(PLIBVAR pVar, unsigned char u8WorkMode)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_RCDETECT_ON;
		pVar->pu8DataBuf[i++] = u8WorkMode;

		if (!(i = DoCommand(pVar, i, /*g_DelayTime*/2000)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_RCDETECT_ON)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	//
	unsigned short NFC_StopAutoDetect(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_RCDETECT_OFF;

		if (!(i = DoCommand(pVar, i, /*g_DelayTime*/2000)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_RCDETECT_OFF)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	//
	unsigned short NFC_ResetAutoDetect(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_RCDETECT_RST;

		if (!(i = DoCommand(pVar, i, /*g_DelayTime*/ 2000 )))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_RCDETECT_RST)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	//
	unsigned short NFC_EventRaise(PLIBVAR pVar, unsigned char *pu8Data, unsigned short *pu16DataLen)
	{
		int i = 0;

		if (!(i = ReceiveEventRaise(pVar, g_DelayTime)))
			//return LRRDNORESP;
			return LRRDBUFFERNODATA;

		if (pVar->pu8DataBuf[0] != MPRD_RCDETECT)
			return LRRDUNKNOWN;

		*pu16DataLen = i - 2;
		memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendMifareCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_MIFARE_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_MIFARE_CMD)
			return LRRDUNKNOWN;

		*pu32DataLen = i - 2;
		memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO14443TypeACommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443A_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443A_CMD)
			return LRRDUNKNOWN;

		*pu32DataLen = i - 2;
		memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO14443TypeAActivated(PLIBVAR pVar)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443A_ACT;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443A_ACT)
			return LRRDUNKNOWN;

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO14443TypeBCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443B_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443B_CMD)
			return LRRDUNKNOWN;

		*pu32DataLen = i - 2;
		memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendFelicaCommand(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_FELICA_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_FELICA_CMD)
			return LRRDUNKNOWN;

		*pu32DataLen = i - 1;
		//R20131007.01 Shawn 
		//memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);
		memcpy(pu8Data, &pVar->pu8DataBuf[1], i - 1);


		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO15693Command(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO15693_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO15693_CMD)
			return LRRDUNKNOWN;

		*pu32DataLen = i - 2;
		memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);

		return LRSUCCESS;
	}
	//

	#define P2P_CMD_WAITTIME	(5000)
	unsigned short NFC_SendNFCIPData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;
		unsigned int	data_len = *pu32DataLen;

#if 0
		pVar->pu8DataBuf[i++] = MPRD_P2P_SEND_EVT_CMD;

		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();
		
		if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_EVT_CMD)
			return LRRDUNKNOWN;
#else
		unsigned short miu = 0;
		unsigned int j;


		pVar->pu8DataBuf[i++] = MPRD_P2P_SEND_CONN_CMD;

		if (!(i = DoCommand(pVar, i, P2P_CMD_WAITTIME)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_CONN_CMD)
			return LRRDUNKNOWN;

		miu = *(unsigned short *)&pVar->pu8DataBuf[1];

		if( miu == 0 )
		{
			return LRRDUNKNOWN;
		}

		
		if( (data_len + 6) > miu )
		{
			// START
			i = 0;
			pVar->pu8DataBuf[ i++ ] = NDEF_BODY_POS_START;
			pVar->pu8DataBuf[ i++ ] = 0;
			pVar->pu8DataBuf[ i++ ] = 0;
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 24 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 16 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 8 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 0 ) & 0xFF);

			memcpy(pVar->pu8DataBuf + i, pu8Data,  miu - 6);
			i +=  (miu - 6);

			if (!(i = DoCommand_NFC_P2P(pVar, i, P2P_CMD_WAITTIME)))
				NoResponseProcess();

			if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_EVT_CMD)
				return LRRDUNKNOWN;

			for( j=(miu - 6); j<data_len; j += miu )
			{
				i = 0;
				if( (j + miu) >= data_len )
				{
					pVar->pu8DataBuf[ i++ ] = NDEF_BODY_POS_END;
					memcpy(pVar->pu8DataBuf + i, pu8Data + j,  data_len - j);
					i +=  (data_len - j);

					if (!(i = DoCommand_NFC_P2P(pVar, i, P2P_CMD_WAITTIME)))
						NoResponseProcess();

					if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_EVT_CMD)
						return LRRDUNKNOWN;
					break;
				}

				pVar->pu8DataBuf[ i++ ] = NDEF_BODY_POS_CONTINUE;
				memcpy(pVar->pu8DataBuf + i, pu8Data + j,  miu);
				i +=  miu;

				if (!(i = DoCommand_NFC_P2P(pVar, i, P2P_CMD_WAITTIME)))
					NoResponseProcess();

				if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_EVT_CMD)
					return LRRDUNKNOWN;
			}
		}
		else
		{
			i = 0;
			pVar->pu8DataBuf[ i++ ] = NDEF_BODY_POS_1_PKG;
			pVar->pu8DataBuf[ i++ ] = 0;
			pVar->pu8DataBuf[ i++ ] = 0;
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 24 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 16 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 8 ) & 0xFF);
			pVar->pu8DataBuf[ i++ ] = ( ( data_len >> 0 ) & 0xFF);

			memcpy(pVar->pu8DataBuf + i, pu8Data,  data_len);
			i +=  data_len;

			if (!(i = DoCommand_NFC_P2P(pVar, i, P2P_CMD_WAITTIME)))
				NoResponseProcess();

			if (pVar->pu8DataBuf[0] != MPRD_P2P_SEND_EVT_CMD)
				return LRRDUNKNOWN;
		}
#endif
		

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO14443TypeACardData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443A_CD_SEND_EVT_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443A_CD_SEND_EVT_CMD)
			return LRRDUNKNOWN;

		//20131015 Shawn start
		//     *pu32DataLen = i - 2;
		//     memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);
		if (1 > i)
		{
			return LRRDUNKNOWN;
		}
		*pu32DataLen = i - 1;
		memcpy(pu8Data, &pVar->pu8DataBuf[1], i - 1);
		//20131015 Shawn end

		return LRSUCCESS;
	}
	//
	unsigned short NFC_SendISO14443TypeBCardData(PLIBVAR pVar, unsigned char *pu8Data, unsigned int *pu32DataLen)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443B_CD_SEND_EVT_CMD;
		memcpy(pVar->pu8DataBuf + i, pu8Data,  *pu32DataLen);
		i +=  *pu32DataLen;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443B_CD_SEND_EVT_CMD)
			return LRRDUNKNOWN;

		//20131015 Shawn start
		//     *pu32DataLen = i - 2;
		//     memcpy(pu8Data, &pVar->pu8DataBuf[2], i - 2);
		if (1 > i)
		{
			return LRRDUNKNOWN;
		}
		*pu32DataLen = i - 1;
		memcpy(pu8Data, &pVar->pu8DataBuf[1], i - 1);
		//20131015 Shawn end

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaPolling(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8PMm, unsigned char *pu8RequestData)
	{
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[30] = 
		{
			0xFF, 0x00, 0x06, 0x00, 0xFF, 0xFF, 0x01, 0x00
		};

		i = 8;

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 0;
		if (pu8Data[i] != 0x00 || pu8Data[i + 2] != FELICA_POLLING + 1)
			return LRFELICACMDERROR;

		i += 3;
		memcpy(pu8IDm, pu8Data + i, 8);
		i += 8;
		memcpy(pu8PMm, pu8Data + i, 8);
		i += 8;
		memcpy(pu8RequestData, pu8Data + i, 2);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaRequestService(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8Count, unsigned short *pu16NodeCodeKeyVerList)
	{
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			//0
			//time,status,len,cmd,...data                                    number of node + node code list
			0xFF, 0x00, 0x01, 0x00, 0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11,  0x00, 0x00, 0x00
		};

		i += 3;
		pu8Data[i++] = FELICA_REQ_SERVICE;
		memcpy(pu8Data + i, pu8IDm, 8);

		i += 8;
		pu8Data[i++] =  *pu8Count;
		memcpy(pu8Data + i, pu16NodeCodeKeyVerList, (*pu8Count) *sizeof(unsigned short));

		i+=(*pu8Count) *sizeof(unsigned short);

		pu8Data[2] = i - 2;

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 2;
		if (pu8Data[i++] != FELICA_REQ_SERVICE + 1)
			return LRFELICACMDERROR;

		memcpy(pu8IDm, pu8Data + i, 8);
		i += 8;

		*pu8Count = pu8Data[i++];
		memcpy(pu16NodeCodeKeyVerList, pu8Data + i, (*pu8Count) *sizeof(unsigned short));

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaRequestResponse(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8Mode)
	{
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			0xFF, 0x00, 0x0A, 0x00, 0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11
		};

		//20131011 Shawn start
		i += 3;
		pu8Data[i++] = FELICA_REQ_RESPONSE;
		memcpy(pu8Data + i, pu8IDm, 8);
		//20131011 Shawn end
		i += 8;

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 0;
		if (pu8Data[i] != 0x00 || pu8Data[i + 2] != FELICA_REQ_RESPONSE + 1)
			return LRFELICACMDERROR;

		i += 3;

		memcpy(pu8IDm, pu8Data + i, 8);

		i += 8; //20131011 Shawn 

		*pu8Mode = pu8Data[i];

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaEnumSystemCode(PLIBVAR pVar, unsigned char *pu8IDm, unsigned short *pu16Systemcode, unsigned char *pu8Count)
	{
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			0xFF, 0x00, 0x0A, 0x0C, 0x01, 0x01, 0x07, 0x01, 0xF9, 0x0E, 0xF6, 0x00
		};

		//20131014 Shawn start
		i = 3;
		pu8Data[i++] = FELICA_REQ_SYS_CODE;
		memcpy(pu8Data + i, pu8IDm, 8);

		i += 8;

		pu8Data[2] = i - 2;
		//20131014 Shawn end

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);

		if (ret != LRSUCCESS)
			return ret;

		i = 2;
		if (pu8Data[i++] != FELICA_REQ_SYS_CODE + 1)
			return LRFELICACMDERROR;

		memcpy(pu8IDm, pu8Data + i, 8);
		i += 8;
		*pu8Count = pu8Data[i++];
		memcpy(pu16Systemcode, pu8Data + i, (*pu8Count) *sizeof(unsigned short));
		//i += (*pu8Count) *sizeof(unsigned short);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaEnumService(PLIBVAR pVar, unsigned char *pu8IDm, unsigned short *pu16Systemservice, unsigned char *pu8Count)
	{
		// 	  //20131015	start
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			0xFF, 0x00, 0x0A, 0x0A, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE
		};

		i = 3;
		pu8Data[i++] = 0x0A;
		memcpy(pu8Data + i, pu8IDm, 8);

		i += 8;
		//MEMO: 目前只對DES的卡有回應成功，文件沒寫，這兩個BYTE 是矇的，代何值 Reader 都會有回應，但都回 FF FF
		pu8Data[i++] = 0xFF;
		pu8Data[i++] = 0xFF;

		i -= 2;
		pu8Data[2] = i;

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		// 	  //20131015	end

		return LRUNKNOWN; // 文件沒資料故未實現. (注: NFC Felica 原始碼有實現此功能, command = 0x0A)
	}
	//
	unsigned short NFC_FelicaReadWithoutEncryption(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char u8ServiceCodeListCount, unsigned short *pu16ServiceCodeList, unsigned char u8BlockListCount, unsigned char *pu8BlockList, unsigned char *pu8StatusFlag1, unsigned char *pu8StatusFlag2, unsigned char *pu8BlockCount, unsigned char *pu8BlockData)
	{
		//unsigned int i = 0; //, j, k;
		unsigned int i = 0, j = 0, k = 0;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			0xFF, 0x00, 0x12, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x02, 0x80, 0x05, 0x80, 0x81
		};

		unsigned int rtn_len = 0; //R20131007.02	Shawn

		//R20131007.02	Shawn	start

		//pu8Data[i++] = FELICA_READ_NO_ENCRYP;
		//memcpy(pu8Data + i, pu8IDm, 8);
		//i += 8;
		//pu8Data[i++] = u8ServiceCodeListCount;
		//memcpy(pu8Data + i, pu16ServiceCodeList, u8ServiceCodeListCount * sizeof(unsigned short));
		//i += u8ServiceCodeListCount * sizeof(unsigned short);
		//pu8Data[i++] = u8BlockListCount;
		//for(j = 0, k = 0; j < u8BlockListCount; j++)
		//{
		//	if( (pu8BlockList[k] & 0x80) == 0x80 )	// 2 Bytes
		//	{
		//		memcpy(pu8Data + i, pu8BlockList, 2);
		//		i += 2;
		//		k += 2;
		//	}
		//	else	// 3 Bytes
		//	{
		//		memcpy(pu8Data + i, pu8BlockList, 3);
		//		i += 3;
		//		k += 3;
		//	}
		//}
		i+=3;
		pu8Data[i] = FELICA_READ_NO_ENCRYP;
		i++;
		memcpy(pu8Data + i, pu8IDm, 8);
		i += 8;
		pu8Data[i++] = u8ServiceCodeListCount;

		memcpy(pu8Data + i, pu16ServiceCodeList, u8ServiceCodeListCount * sizeof(unsigned short) );
		i += u8ServiceCodeListCount * sizeof(unsigned short);

		//i+=2;
		pu8Data[i++] = u8BlockListCount;

		for( ; j < u8BlockListCount; j++)
		{
			if( (pu8BlockList[k] & 0x80) == 0x80 )	// 2 Bytes
			{
				memcpy(pu8Data + i, pu8BlockList + k, 2);
				i += 2;
				k += 2;
			}
			else	// 3 Bytes
			{
				memcpy(pu8Data + i, pu8BlockList + k, 3);
				i += 3;
				k += 3;
			}
		}	

		//i = 20;
		pu8Data[2] = i - 2;

		//R20131007.02	Shawn	end
		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		rtn_len = i;
		//i = 0;
		i = 2;
		if (pu8Data[i++] != FELICA_READ_NO_ENCRYP + 1)
			return LRFELICACMDERROR;

		memcpy(pu8IDm, pu8Data + i, 8);
		i += 8;
		*pu8StatusFlag1 = pu8Data[i++];
		*pu8StatusFlag2 = pu8Data[i++];
		*pu8BlockCount = pu8Data[i++];

		// 	if ( *pu8StatusFlag1 != 0 || *pu8StatusFlag2 != 0)
		// 	{
		// 		return LRFELICACMDERROR;
		// 	}

		memcpy(pu8BlockData, pu8Data + i, (*pu8BlockCount * 16));
		*pu8BlockCount = (*pu8BlockCount * 16);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaWriteWithoutEncryption(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char u8ServiceCodeListCount, unsigned short *pu16ServiceCodeList,  unsigned char u8BlockCount, unsigned char *pu8BlockList, unsigned char *pu8BlockData, unsigned char *pu8StatusFlag1, unsigned char *pu8StatusFlag2)
	{
		unsigned int i = 0, j, k;
		unsigned short ret;
		unsigned char pu8Data[128] = 
		{
			//0
			0xFF, 0x00, 0x12, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x02, 0x80, 0x05, 0x80, 0x81
		};

		i = 3;

		pu8Data[i++] = FELICA_WRITE_NO_ENCRYP;
		memcpy(pu8Data + i, pu8IDm, 8);
		i += 8;
		pu8Data[i++] = u8ServiceCodeListCount;

		memcpy(pu8Data + i, pu16ServiceCodeList, u8ServiceCodeListCount *sizeof(unsigned short));
		i += u8ServiceCodeListCount * sizeof(unsigned short);

		pu8Data[i++] = u8BlockCount;
		for (j = 0, k = 0; j < u8BlockCount; j++)
		{
			if ((pu8BlockList[k] &0x80) == 0x80)
				// 2 Bytes
			{
				memcpy(pu8Data + i, pu8BlockList, 2);
				i += 2;
				k += 2;
			}
			else
				// 3 Bytes
			{
				memcpy(pu8Data + i, pu8BlockList, 3);
				i += 3;
				k += 3;
			}
		}
		memcpy(pu8Data + i, pu8BlockData, 16 *u8BlockCount);
		i += 16 * u8BlockCount;

		pu8Data[2] = i - 2;

		ret = NFC_SendFelicaCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 2;
		if (pu8Data[i++] != FELICA_WRITE_NO_ENCRYP + 1)
			return LRFELICACMDERROR;

		memcpy(pu8IDm, pu8Data + i, 8);
		i += 8;
		*pu8StatusFlag1 = pu8Data[i++];
		*pu8StatusFlag2 = pu8Data[i++];

		// 	if ( *pu8StatusFlag1 != 0 || *pu8StatusFlag2 != 0)
		// 	{
		// 		return LRFELICACMDERROR;
		// 	}

		return LRSUCCESS;
	}
	//
	unsigned short NFC_ISO14443BPolling(PLIBVAR pVar, unsigned char *pu8Pupi)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_ISO14443B_GETCARD;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_ISO14443B_GETCARD)
			return LRRDUNKNOWN;

		memcpy(pu8Pupi, &pVar->pu8DataBuf[2], 8);

		return LRSUCCESS;
	}
	//
	unsigned short NFC_FelicaGetCard(PLIBVAR pVar, unsigned char *pu8IDm, unsigned char *pu8PMm)
	{
		int i = 0;

		pVar->pu8DataBuf[i++] = MPRD_FELICA_GETCARD;

		if (!(i = DoCommand(pVar, i, g_DelayTime)))
			NoResponseProcess();

		if (pVar->pu8DataBuf[0] != MPRD_FELICA_GETCARD)
			return LRRDUNKNOWN;

		//R20131011.01 Shawn START
		//     memcpy(pu8IDm, &pVar->pu8DataBuf[2], 8);
		//     memcpy(pu8PMm, &pVar->pu8DataBuf[2+8], 8);
		memcpy(pu8IDm, &pVar->pu8DataBuf[1], 8);
		memcpy(pu8PMm, &pVar->pu8DataBuf[1+8], 8);

		//R20131011.01 Shawn END

		return LRSUCCESS;
	}

	//20130926	Shawn	START
	unsigned short NFC_MifareAuthenticationKeyA(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Uid , unsigned char *pu8Key)
	{	  //60 04 4514B70C FFFFFFFFFFFF
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[30] = {0};

		pu8Data[i] = 0x60;
		++i;

		pu8Data[i] = u8BlkNo;
		++i;

		memcpy(pu8Data + i, pu8Uid, 4);
		i+=4;

		memcpy(pu8Data + i, pu8Key, 6);
		i+=6;

		//cmd blkNo Uid   Key
		// 1 + 1  +  4  +  6
		//i = 12;
		ret = NFC_SendMifareCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 0;
		// 	  if (pu8Data[i] != 0x00 )
		// 		  return LRMIFARECMDERROR;

		return LRSUCCESS;
	}

	unsigned short NFC_MifareAuthenticationKeyB(PLIBVAR pVar, unsigned char u8BlkNo, unsigned char *pu8Uid , unsigned char *pu8Key)
	{	  
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[30] = {0};

		pu8Data[i] = 0x61;
		++i;

		pu8Data[i] = u8BlkNo;
		++i;

		memcpy(pu8Data + i, pu8Uid, 4);
		i+=4;

		memcpy(pu8Data + i, pu8Key, 6);
		i+=6;

		//cmd blkNo Uid   Key
		// 1 + 1  +  4  +  6
		//i = 12;
		ret = NFC_SendMifareCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 0;
		// 	  if (pu8Data[i] != 0x00 )
		// 		  return LRMIFARECMDERROR;

		return LRSUCCESS;
	}

	unsigned short NFC_MifareOneReadBlock(PLIBVAR pVar, unsigned char u1KeyType , unsigned char u8BlkNo, unsigned char *pu8BlkData , unsigned char *pu8Uid , unsigned char *pu8Key)
	{	  
		unsigned int i = 0;
		unsigned short ret;
		unsigned char pu8Data[30] = {0};

		if (u1KeyType == '0')//(unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_A
		{
			ret = NFC_MifareAuthenticationKeyA ( pVar , u8BlkNo, pu8Uid , pu8Key);
		}
		else if (u1KeyType == '1')// (unsigned char) RDINT.CARD_KEY_TYPE.CARD_KEY_B
		{
			ret = NFC_MifareAuthenticationKeyB ( pVar , u8BlkNo, pu8Uid , pu8Key);
		}
		else
		{
			return LRKEYCARD;
		}

		if(ret != 0)//RDINT_RESULT.LRSUCCESS
		{
			return ret;
		}

		pu8Data[i] = 0x30;
		++i;

		pu8Data[i] = u8BlkNo;
		++i;

		memcpy(pu8Data + i, pu8Uid, 4);
		i+=4;

		memcpy(pu8Data + i, pu8Key, 6);
		i+=6;

		//cmd blkNo Uid   Key
		// 1 + 1  +  4  +  6
		//i = 12;
		ret = NFC_SendMifareCommand(pVar, pu8Data, &i);
		if (ret != LRSUCCESS)
			return ret;

		i = 0;
		// 	  if (pu8Data[i] != 0x00 )
		// 		  return LRMIFARECMDERROR;

		memcpy ( pu8BlkData, pu8Data , 16 ); //RDINT.MIFARE_ONE_DEF.BLOCK_LEN

		return LRSUCCESS;
	}

	//20130926	Shawn	END
	//
	

#ifdef __cplusplus
}
#endif
