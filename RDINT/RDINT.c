// RDINT.cpp : 定義 DLL 應用程式的匯出函式。

#include <stdio.h>
#include <string.h>
#include "RDINT.h"

#include "RDINT_DEF.h"
#include "RDLib.h"
#include "RDLib_Extern.h"


#ifdef __cplusplus
extern "C"
{
#endif

    LIBVAR g_libVar;

    uint8_t g_strAPI_AccessCode[] = "\x7B\x99\x70\x82--TsaiKenny";
    //PRDINF g_pRdinf = NULL;
    //PRDINF g_pRdinf_v1 = NULL;

    uint8_t u1AlreadyLoadKeyFile = 0;
    uint8_t g_u8Protocol = 0;
    int32_t g_WorkType = WT_ISO14443_TYPEA;
    //int32_t g_DelayTime = 500L;
    int32_t g_DelayTime = 1000L;

    // 這是匯出函式的範例。
    RDINT_API int32_t RDINTsys_OpenReader(uint8_t u8COMPort, uint32_t u32Baudrate, char *strAccessCode, uint8_t u8SecurityMode, uint32_t u32OpenDelayMs, uint32_t *pu32Baudrate)
    {
        int i, ret = LRBADCOMPORT;
        uint8_t AccessCode[4];
        char szAccessCode[3];
        uint32_t Baudrate = 0L, t;


        if (u32OpenDelayMs <= 0L)
            u32OpenDelayMs = 200L;
        if (pu32Baudrate != NULL)
            *pu32Baudrate = 0L;

        if (u32OpenDelayMs <= 0L)
            g_DelayTime = 250L;
        else
            g_DelayTime = u32OpenDelayMs;


        if (g_libVar.u16DeviceNum == (uint16_t)u8COMPort)
        {
            if (g_libVar.u8RdID)
            {
                return LRSUCCESS;
            }
        }

        if (!strAccessCode)
        {
            for (i = 0; i < 4; i++)
                AccessCode[i] = g_strAPI_AccessCode[i];
        }
        else
            //if(!strAccessCode)
        {

            if (strlen(strAccessCode) < 8)
            {
                for (i = 0; i < 4; i++)
                    AccessCode[i] = g_strAPI_AccessCode[i];
            }
            else
                //if(strlen(strAccessCode)<8)
            {
                for (i = 0; i < 4; i++)
                {
                    strncpy(szAccessCode, &strAccessCode[i * 2], 2);
#if defined(_WINDOWS) || defined(WIN32)
                    szAccessCode[2] = (char)NULL;
#else 
                    szAccessCode[2] = 0;
#endif 
                    AccessCode[i] = AsciiToUINTForHexA(szAccessCode);
                }
            } //if(strlen(strAccessCode)<8)
        } //if(!strAccessCode)

        if (u32Baudrate != 0)
        {
            CloseCOM(u8COMPort);

            g_libVar.u16DeviceNum = (uint16_t)u8COMPort;
            g_libVar.u8RdID = 0x01;

            if (OpenCOM(u8COMPort, u32Baudrate))
            {
                if (u32OpenDelayMs > 0L)
                {
                    //t = GetTickCount();
                    //while( (GetTickCount() - t) < (DWORD)u32OpenDelayMs )
                    //	if( t > GetTickCount() ) t = GetTickCount();
                    t = GetTickFunc();
                    while ((GetTickFunc() - t) < (uint32_t)u32OpenDelayMs)
                        if (t > GetTickFunc())
                            t = GetTickFunc();
                }

                if (pu32Baudrate != NULL)
                    *pu32Baudrate = u32Baudrate;
                if ((ret = Sys_OpenReader(&g_libVar, AccessCode, u8SecurityMode)) == LRSUCCESS)
                    return LRSUCCESS;
            }
            else
            {
                ret = LRBADCOMPORT;
                goto errOpenCOM;
            }
        }

        for (i = 0; i < 4; i++)
        {
#ifdef WINCE
            switch (i)
            {
            case 0:
                Baudrate = 115200L;
                break;
            case 1:
                Baudrate = 38400L;
                break;
            case 2:
                Baudrate = 19200L;
                break;
            case 3:
                Baudrate = 9600L;
                break;
            }
#else 
            switch (i)
            {
            case 0:
                Baudrate = 38400L;
                break;
            case 1:
                Baudrate = 115200L;
                break;
            case 2:
                Baudrate = 19200L;
                break;
            case 3:
                Baudrate = 9600L;
                break;
            }
#endif 
            if (Baudrate == u32Baudrate)
                continue;

            CloseCOM(u8COMPort);

            g_libVar.u16DeviceNum = (uint16_t)u8COMPort;
            g_libVar.u8RdID = 0x01;

            if (!OpenCOM(u8COMPort, Baudrate))
            {
                ret = LRBADCOMPORT;
                goto errOpenCOM;
            }

            if (u32OpenDelayMs > 0L)
            {
                //t = GetTickCount();
                //while( (GetTickCount() - t) < (DWORD)u32OpenDelayMs )
                //	if( t > GetTickCount() ) t = GetTickCount();

                t = GetTickFunc();
                while ((GetTickFunc() - t) < (uint32_t)u32OpenDelayMs)
                    if (t > GetTickFunc())
                        t = GetTickFunc();
            }

            if (pu32Baudrate != NULL)
                *pu32Baudrate = Baudrate;
            if ((ret = Sys_OpenReader(&g_libVar, AccessCode, u8SecurityMode)) != LRSUCCESS)
                continue;

            break;
        }

        if (LRSUCCESS != ret)
            goto errOpenCOM;

        return LRSUCCESS;

    errOpenCOM:
        CloseCOM(u8COMPort);

        if (pu32Baudrate != NULL)
            *pu32Baudrate = 0L;

        memset(&g_libVar, 0, sizeof(LIBVAR));
        return ret;
    }

    RDINT_API int32_t RDINTsys_CloseReader(uint8_t u8COMPort)
    {
        int32_t ret;

        ret = Sys_CloseReader(&g_libVar);
        CloseCOM(u8COMPort);

        memset(&g_libVar, 0, sizeof(LIBVAR));
        return ret;
    }
    
    RDINT_API int32_t RDINT_NFC_OpenReader(uint8_t u8COMPort, uint32_t u32Baudrate, char *strAccessCode, uint8_t u8SecurityMode, uint32_t u32OpenDelayMs, uint32_t *pu32Baudrate, uint8_t u8Identity)
    {
        int32_t i, ret = LRBADCOMPORT;
        uint8_t AccessCode[4];
        char szAccessCode[3];
        uint32_t Baudrate = 0L, t;

        if (u32OpenDelayMs <= 0L)
            u32OpenDelayMs = 200L;
        if (pu32Baudrate != NULL)
            *pu32Baudrate = 0L;

        if (u32OpenDelayMs <= 0L)
            g_DelayTime = 250L;
        else
            g_DelayTime = u32OpenDelayMs;



        if (g_libVar.u16DeviceNum == (UINT16)u8COMPort)
        {
            if (g_libVar.u8RdID)
            {
                return LRSUCCESS;
            }
        }

        if (!strAccessCode)
        {
            for (i = 0; i < 4; i++)
                AccessCode[i] = g_strAPI_AccessCode[i];
        }
        else
            //if(!strAccessCode)
        {

            if (strlen(strAccessCode) < 8)
            {
                for (i = 0; i < 4; i++)
                    AccessCode[i] = g_strAPI_AccessCode[i];
            }
            else
                //if(strlen(strAccessCode)<8)
            {
                for (i = 0; i < 4; i++)
                {
                    strncpy(szAccessCode, &strAccessCode[i * 2], 2);
#if defined(_WINDOWS) || defined(WIN32)
                    szAccessCode[2] = (char)NULL;
#else 
                    szAccessCode[2] = 0;
#endif 
                    AccessCode[i] = AsciiToUINTForHexA(szAccessCode);
                }
            } //if(strlen(strAccessCode)<8)
        } //if(!strAccessCode)

        if (u32Baudrate != 0)
        {
            CloseCOM(u8COMPort);

            g_libVar.u16DeviceNum = (UINT16)u8COMPort;
            g_libVar.u8RdID = 0x01;

            if (OpenCOM(u8COMPort, u32Baudrate))
            {
                if (u32OpenDelayMs > 0L)
                {
                    t = GetTickFunc();
                    while ((GetTickFunc() - t) < (DWORD)u32OpenDelayMs)
                        if (t > GetTickFunc())
                            t = GetTickFunc();
                }

                if (pu32Baudrate != NULL)
                    *pu32Baudrate = u32Baudrate;
                if ((ret = NFC_Sys_OpenReader(&g_libVar, AccessCode, u8SecurityMode, u8Identity)) == LRSUCCESS)
                    return LRSUCCESS;
            }
            else
            {
                ret = LRBADCOMPORT;
                goto errOpenCOM;
            }
        }

        for (i = 0; i < 4; i++)
        {
#ifdef WINCE
            switch (i)
            {
            case 0:
                Baudrate = 115200L;
                break;
            case 1:
                Baudrate = 38400L;
                break;
            case 2:
                Baudrate = 19200L;
                break;
            case 3:
                Baudrate = 9600L;
                break;
            }
#else 
            switch (i)
            {
            case 0:
                Baudrate = 38400L;
                break;
            case 1:
                Baudrate = 115200L;
                break;
            case 2:
                Baudrate = 19200L;
                break;
            case 3:
                Baudrate = 9600L;
                break;
            }
#endif 
            if (Baudrate == u32Baudrate)
                continue;

            CloseCOM(u8COMPort);

            g_libVar.u16DeviceNum = (uint16_t)u8COMPort;
            g_libVar.u8RdID = 0x01;

            if (!OpenCOM(u8COMPort, Baudrate))
            {
                ret = LRBADCOMPORT;
                goto errOpenCOM;
            }

            if (u32OpenDelayMs > 0L)
            {
                //t = GetTickCount();
                //while( (GetTickCount() - t) < (DWORD)u32OpenDelayMs )
                //	if( t > GetTickCount() ) t = GetTickCount();

                t = GetTickFunc();
                while ((GetTickFunc() - t) < (uint32_t)u32OpenDelayMs)
                    if (t > GetTickFunc())
                        t = GetTickFunc();
            }

            if (pu32Baudrate != NULL)
                *pu32Baudrate = Baudrate;
            if ((ret = NFC_Sys_OpenReader(&g_libVar, AccessCode, u8SecurityMode, u8Identity)) != LRSUCCESS)
                continue;

            break;
        }

        if (LRSUCCESS != ret)
            goto errOpenCOM;

        return LRSUCCESS;

errOpenCOM:
        CloseCOM(u8COMPort);

        if (pu32Baudrate != NULL)
            *pu32Baudrate = 0L;
        memset(&g_libVar, 0, sizeof(LIBVAR));
        return ret;
    }

    RDINT_API int32_t RDINT_OpenCard(uint8_t u8COMPort, uint8_t u1AutoFind, uint8_t *pu8Uid, uint8_t *pu8Atqa, uint8_t *pu8Sak)
    {
        if (g_WorkType != WT_ISO14443_TYPEA)
            return LRWORKTYPEERROR;

        return MpRd_OpenCard(&g_libVar, u1AutoFind, pu8Uid, pu8Atqa, pu8Sak);
    }

    RDINT_API int32_t RDINT_GetUID(uint8_t u8COMPort, uint8_t *pu8Uid)
    {
        return MpRd_GetUID(&g_libVar, pu8Uid);
    }

    RDINT_API int32_t RDINT_ReadMifareOneBlock(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Block, uint8_t *pu8Key, uint8_t *pu8Data)
    {
        if (g_WorkType != WT_ISO14443_TYPEA)
            return LRWORKTYPEERROR;

        return MpRd_ReadMifareOneBlock(&g_libVar, u1KeyType, u1DefaultKey, u8DefaultKeyIndex, u8Block, pu8Key, pu8Data);
    }

    RDINT_API int32_t RDINT_ReadMifareOneSector(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Sector, uint8_t *pu8Key, uint8_t *pu8Data)
    {
        if (g_WorkType != WT_ISO14443_TYPEA)
            return LRWORKTYPEERROR;

        return MpRd_ReadMifareOneSector(&g_libVar, u1KeyType, u1DefaultKey, u8DefaultKeyIndex, u8Sector, pu8Key, pu8Data);
  }


    RDINT_API int32_t RDINT_WriteMifareOneBlock(uint8_t u8COMPort, uint8_t u1KeyType, uint8_t u1DefaultKey, uint8_t u8DefaultKeyIndex, uint8_t u8Block, uint8_t *pu8Key, uint8_t *pu8Data)
    {
        if (g_WorkType != WT_ISO14443_TYPEA)
            return LRWORKTYPEERROR;

        return MpRd_WriteMifareOneBlock(&g_libVar, u1KeyType, u1DefaultKey, u8DefaultKeyIndex, u8Block, pu8Key, pu8Data);
    }

    RDINT_API int32_t RDINT_DeviceBlinkControl(uint8_t u8COMPort, uint8_t u8Type, uint8_t u8RedMs100, uint8_t u8GreenMs100, uint8_t u8YellowMs100, uint8_t u8SoundMs100)
    {
        return MpRd_DeviceBlinkControl(&g_libVar, u8Type, u8RedMs100, u8GreenMs100, u8YellowMs100, u8SoundMs100);
    }

#ifdef __cplusplus
}
#endif
