#if defined(_WINDOWS) || defined(WIN32)
	#include <windows.h>
	#include <tchar.h>

	#ifdef _DEBUG
		#ifndef WINCE
			#include <stdio.h>
		#endif 
	#endif 
#else 
	#include <stdio.h>		//標準輸入輸出定義
	#include <unistd.h>		//Unix標準函數定義
	#include <termios.h>	//PPSIX終端控制定義
	#include <errno.h>		//錯誤號定義
	#include <fcntl.h>		//檔控制定義

	#include <stdlib.h>     //標準函數庫定義
	#include <sys/types.h>  //
	#include <sys/stat.h>   //
	#include <string.h>
	#include <sys/select.h>

	//#define _DEBUG
	//	#include <sys/ioctl.h>

	#include <time.h>

	#include <wchar.h>
	#include <string.h>

	#define MAX_COMPORT	255

	// 決定 COM 的開啟方式
	//#define _MATRIX500
	//#define _MT180ARM


	#define _PARKING_COLUMN_3RD // 第三代停車柱
	#define TTY "/dev/ttyS"
	//#define TTY "/dev/tty"
char *ttyName = NULL;
int ____SetCOMName____(char *COMName);
int OpenDev(char *Dev);
int setport(int fd, int baud);
#endif 

#include "RDINT_DEF.h"
#include "RDLib_Extern.h"

#ifdef __cplusplus
extern "C"
{
#endif 

#if defined(_WINDOWS) || defined(WIN32)
	static HANDLE hCOM[255] = 
	{
		NULL
	};

#ifdef _DEBUG
	static TCHAR s[256];
	static int is;
#endif // #ifdef _DEBUG
#else 
	static int hCOM[255] = 
	{
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
		-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
	};
	struct libusb_device_handle *handle = NULL;
#ifdef _DEBUG
	static CHAR s[256];
	static int is;
#endif // #ifdef _DEBUG

#endif 

	UINT32 SendFunc(UINT16 u16COMPort, BYTE *pcu8Data, UINT32 u32DataLen)
	{
#if defined(_WINDOWS) || defined(WIN32)

		DWORD sendLen;
	#ifdef _DEBUG
		DWORD i;
	#endif 


	#ifdef _DEBUG
		#ifndef WINCE
		HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
		#endif 
	#endif 

		{
			// 清 buffer
			PurgeComm(hCOM[u16COMPort - 1], PURGE_RXCLEAR);
			WriteFile(hCOM[u16COMPort - 1], pcu8Data, (DWORD)u32DataLen, &sendLen, NULL);
		}

		//WriteFile(hCOM[u16COMPort - 1], pcu8Data, (DWORD)u32DataLen, &sendLen, NULL);

	#ifdef _DEBUG

		is = wsprintf(s, _T("[%d]---Send:"), u16COMPort);
		#ifndef WINCE
		WriteConsole(stdOut, s, is, NULL, NULL);
		#else 
		_tprintf(s);
		#endif 

		//20140403	Shawn	start
		//回應失敗就是回傳 0
		#ifndef WINCE
		if( (0 == u16COMPort) && ((UINT32) INVALID_HANDLE_VALUE == sendLen))
		{			
			is = wsprintf(s, _T("System Error code [ %d ] \n"), GetLastError());
			WriteConsole(stdOut, s, is, NULL, NULL);

			//TODO: 測試如果 失敗後 restart是否能恢復連線。-
			return 0;
		}
		#endif
		//20140403	Shawn	end

		for (i = 0; i < sendLen; i++)
		{
			is = wsprintf(s, _T("%02X "), pcu8Data[i]);
		#ifndef WINCE
			WriteConsole(stdOut, s, is, NULL, NULL);
		#else 
			_tprintf(s);
		#endif 
		}
		is = wsprintf(s, _T("\n"));
		#ifndef WINCE
		WriteConsole(stdOut, s, is, NULL, NULL);
		#else 
		_tprintf(s);
		#endif 

	#endif 

		return sendLen;

#else // #if defined(_WINDOWS) || defined(WIN32)
		int i = 0, j;
#ifdef _DEBUG
		int k;
#endif // #ifdef _DEBUG

		fd_set writefds;
		struct timeval t;
		//UINT32 SendFunc(UINT16 u16COMPort, BYTE *pcu8Data, UINT32 u32DataLen)

		if (!u16COMPort || (u16COMPort > MAX_COMPORT))
			return 0;

		FD_ZERO(&writefds);
		FD_SET(hCOM[u16COMPort - 1], &writefds);

		bzero(&t, sizeof(struct timeval));
		t.tv_sec = 0;
		t.tv_usec = 0;

#ifdef _DEBUG
		is = sprintf(s, "[%d]---Send:", u16COMPort);
		printf("%s", s);
#endif // #ifdef _DEBUG

		do
		{
			if (select(hCOM[u16COMPort - 1] + 1, NULL, &writefds, NULL, &t) < 0)
				return 0;
			if (!FD_ISSET(hCOM[u16COMPort - 1], &writefds))
				break;

			j = write(hCOM[u16COMPort - 1], pcu8Data + i, u32DataLen - i);
			if (j > 0)
			{
#ifdef _DEBUG
				for (k = i; k < (i + j); k++)
				{
					is = sprintf(s, "%02X ", pcu8Data[k]);
					printf("%s", s);
				} 
#endif // #ifdef _DEBUG
				i += j;
			}
		}
		while ((u32DataLen > i) && (j > 0));

#ifdef _DEBUG
		is = sprintf(s, "\n");
		printf("%s", s);
#endif // #ifdef _DEBUG

		return i;
#endif // #if defined(_WINDOWS) || defined(WIN32)
	}

	UINT32 RecvFunc(UINT16 u16COMPort, BYTE *pu8Data, UINT32 u32DataLen)
	{
#if defined(_WINDOWS) || defined(WIN32)


		DWORD getLen = 0, totalLen = 0;
		int cnt = 0;

#ifdef _DEBUG
		BOOL b = FALSE;
	#ifndef WINCE
		HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif 

#endif // #ifdef _DEBUG

		//UART
		while (ReadFile(hCOM[u16COMPort - 1], (LPVOID) &pu8Data[totalLen], (DWORD)1, &getLen, NULL) && (totalLen < u32DataLen))
		{
			if (getLen)
			{
#ifdef _DEBUG
				if (b)
				{
					is = 0;
				}
				else
				{
					is = wsprintf(s, _T("[%d]---Get:"), u16COMPort);
					b = TRUE;
				}

	#ifndef WINCE
				WriteConsole(stdOut, s, is, NULL, NULL);
	#else 
				_tprintf(s);
	#endif 

				is = wsprintf(s, _T("%02X "), pu8Data[totalLen]);
	#ifndef WINCE
				WriteConsole(stdOut, s, is, NULL, NULL);
	#else 
				_tprintf(s);
	#endif 

#endif // #ifdef _DEBUG

				totalLen += getLen;
				getLen = 0;
			}
			else
			{
#ifdef _DEBUG
				if (b)
				{
					is = wsprintf(s, _T("\n"));
	#ifndef WINCE
					WriteConsole(stdOut, s, is, NULL, NULL);
	#else 
					_tprintf(s);
	#endif 
				}
#endif // #ifdef _DEBUG
				return totalLen;
			}

#ifdef WINCE
			Sleep(1);	// WinCE 需等待 1ms 否則會沒收到資料
#endif // WINCE
		}

#ifdef _DEBUG
		if (b)
		{
			is = wsprintf(s, _T("\n"));
	#ifndef WINCE
			WriteConsole(stdOut, s, is, NULL, NULL);
	#else 
			_tprintf(s);
	#endif 
		}
#endif // #ifdef _DEBUG
		return (UINT32)totalLen;

#else // #if defined(_WINDOWS) || defined(WIN32)
		int i = 0, j;
		fd_set readfds;
		struct timeval t;
		//UINT32 RecvFunc(UINT16 u16COMPort, BYTE *pu8Data, UINT32 u32DataLen)

		if (u16COMPort <= 0 || (u16COMPort > MAX_COMPORT))
			return 0;

		FD_ZERO(&readfds);
		FD_SET(hCOM[u16COMPort - 1], &readfds);

		bzero(&t, sizeof(struct timeval));
		t.tv_sec = 0L;
		t.tv_usec = 0L; //1L;

		do
		{
			if (select(hCOM[u16COMPort - 1] + 1, &readfds, NULL, NULL, &t) < 0)
				return 0;

			if (!FD_ISSET(hCOM[u16COMPort - 1], &readfds))
				break;

			j = read(hCOM[u16COMPort - 1], pu8Data + i, u32DataLen - i);
			if (j > 0)
				i += j;
		} 
		while ((u32DataLen > i) && (j > 0));

#ifdef _DEBUG
		if (i > 0)
		{
			is = sprintf(s, "[%d]---Get:", u16COMPort);
			printf("%s", s);

			for (j = 0; j < i; j++)
			{
				is = sprintf(s, "%02X ", pu8Data[j]);
				printf("%s", s);
			}
			is = sprintf(s, "\n");
			printf("%s", s);
		}
#endif // #ifdef _DEBUG
		return i;
#endif // #if defined(_WINDOWS) || defined(WIN32)
	}

	ULONG GetUnixTimeForNow(VOID)
	{
#if defined(_WINDOWS) || defined(WIN32)
		SYSTEMTIME st;
		FILETIME ft;
		LONGLONG ll;


		GetLocalTime(&st);
		st.wSecond = 0;
		st.wMilliseconds = 0;
		st.wMinute = 0;
		st.wHour = 0;
		st.wSecond = 0;
		if (!SystemTimeToFileTime(&st, &ft))
			return LRSYSTEM;

		ll = (LONGLONG)ft.dwLowDateTime;
		ll += (LONGLONG)ft.dwHighDateTime << 32;
		ll -= 116444736000000000;
		ll /= 10000000;

		return (ULONG)ll;
#else // #if defined(_WINDOWS) || defined(WIN32)
		return time(NULL);
#endif // #if defined(_WINDOWS) || defined(WIN32)
	}


	ULONG GetTickFunc(VOID)
	{
#if defined(_WINDOWS) || defined(WIN32)
		return (ULONG)GetTickCount();
#else // #if defined(_WINDOWS) || defined(WIN32)
		//return clock() / (CLOCKS_PER_SEC / 1000);
		struct timespec now;
		clock_gettime(CLOCK_MONOTONIC, &now);
		return (now.tv_sec * 1000 + now.tv_nsec / 1000000);
#endif // #if defined(_WINDOWS) || defined(WIN32)
	}


	UINT AsciiToUINTForHexA(LPCSTR lpStr)
	{
		int i;
		UINT ui = 0L;

		for (i = 0; i < (int)strlen(lpStr); i++)
		{
			ui <<= 4;
			if (lpStr[i] >= 'A' && lpStr[i] <= 'F')
				ui += (UINT)(lpStr[i] - 'A' + 0x0A);
			else if (lpStr[i] >= 'a' && lpStr[i] <= 'f')
				ui += (UINT)(lpStr[i] - 'a' + 0x0A);
			else if (lpStr[i] >= '0' && lpStr[i] <= '9')
				ui += (UINT)(lpStr[i] - '0');
			else
				return 0L;
		}

		return ui;
	}


	UINT AsciiToUINTForHexW(LPCWSTR lpStr)
	{
		int i;
		UINT ui = 0L;

		for (i = 0; i < (int)wcslen(lpStr); i++)
		{
			ui <<= 4;
			if (lpStr[i] >= 'A' && lpStr[i] <= 'F')
				ui += (UINT)(lpStr[i] - 'A' + 0x0A);
			else if (lpStr[i] >= 'a' && lpStr[i] <= 'f')
				ui += (UINT)(lpStr[i] - 'a' + 0x0A);
			else if (lpStr[i] >= '0' && lpStr[i] <= '9')
				ui += (UINT)(lpStr[i] - '0');
			else
				return 0L;
		}

		return ui;
	}


	int OpenCOM(UINT16 u16COMPort, DWORD u32BaudRate)
	{
#if defined(_WINDOWS) || defined(WIN32)
		DCB dcb;
		TCHAR szCOM[11];
		COMMTIMEOUTS timeOut;


		if (!u16COMPort)
			return 0;

		wsprintf(szCOM, _T("COM%d:"), u16COMPort);
		hCOM[u16COMPort - 1] = CreateFile(szCOM, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

#ifdef WINCE
		
		if (hCOM[u16COMPort - 1] == INVALID_HANDLE_VALUE)
		{
			wsprintf(szCOM, _T("$device\\COM%d"), u16COMPort);
			hCOM[u16COMPort - 1] = CreateFile(szCOM, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		}
		Sleep(300);
#endif 

		if (hCOM[u16COMPort - 1] == INVALID_HANDLE_VALUE)
		{
			wsprintf(szCOM, _T("\\\\.\\COM%d"), u16COMPort);
			hCOM[u16COMPort - 1] = CreateFile(szCOM, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		}

		/*
		#ifndef WINCE
		wsprintf(szCOM,_T("\\\\.\\COM%d"),u16COMPort);
		#else
		wsprintf(szCOM,_T("COM%d:"),u16COMPort);
		#endif

		hCOM[u16COMPort-1] = CreateFile(szCOM, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL );
		*/
		if (hCOM[u16COMPort - 1] == INVALID_HANDLE_VALUE)
		{
			//printf ("CreateFile failed with error %d.\n", GetLastError());
			return 0;

		}

		if (!GetCommState(hCOM[u16COMPort - 1], &dcb))
		{
			//printf("Error GetCommState   %d\n",GetLastError());
			//CloseHandle(hCOM);
			CloseCOM(u16COMPort);
			return 0;
		}

		dcb.BaudRate = u32BaudRate; //CBR_19200;
		dcb.ByteSize = 8;
		dcb.Parity = NOPARITY;
		dcb.StopBits = ONESTOPBIT;
		dcb.fOutX = 0; // 關閉Handshake(Xon/Xoff)
		dcb.fInX = 0;

		if (!SetCommState(hCOM[u16COMPort - 1], &dcb))
		{
			//printf("Error SetCommState   %d\n",GetLastError());
			//CloseHandle(hCOM);
			CloseCOM(u16COMPort);
			return 0;
		}


		// 設定 RS232 的 Buffer 大小
		if (!SetupComm(hCOM[u16COMPort - 1], 2048, 2048))
			if (!SetupComm(hCOM[u16COMPort - 1], 512, 512))
				if (!SetupComm(hCOM[u16COMPort - 1], 128, 128))
					if (!SetupComm(hCOM[u16COMPort - 1], 32, 32))


						//if(!SetupComm(hCOM[u16COMPort-1],2048,2048))
						//{
						//	//printf("Error SetupComm  %d\n",GetLastError());
						//	//CloseHandle(hCOM);
						//	CloseCOM(u16COMPort);
						//	return 0;
						//}

		timeOut.ReadIntervalTimeout = 1;
		timeOut.ReadTotalTimeoutConstant = 1;
		timeOut.ReadTotalTimeoutMultiplier = 1;
		timeOut.WriteTotalTimeoutConstant = 500;
		timeOut.WriteTotalTimeoutMultiplier = 50;

		SetCommTimeouts(hCOM[u16COMPort - 1], &timeOut);

		return 1;

#else // #if defined(_WINDOWS) || defined(WIN32)
		char szCOMName[16];
		int r;
		//int OpenCOM(UINT16 u16COMPort, DWORD u32BaudRate)
	
		if (!u16COMPort || (u16COMPort > MAX_COMPORT))
			return 0;

		if (hCOM[u16COMPort - 1] !=  - 1)
			return 0;
		//____SetCOMName____("/dev/ttyUSB0");
		if ( ttyName != NULL ){
			sprintf(szCOMName, "%s", ttyName); // Matrix 500 用
		}
		else
		{
#ifdef _MATRIX500
		    sprintf(szCOMName, "/dev/ttyS%d", u16COMPort); // Matrix 500 用
#elif defined(_MT180ARM)
		    sprintf(szCOMName, "/dev/ttySAC%d", u16COMPort); // MT 180 ARM 用
#elif defined(_x86)
		    sprintf(szCOMName, "/dev/ttyS%d", (u16COMPort - 1)); // x86 用
#elif defined(_MT180L)
		    sprintf(szCOMName, "/dev/ttyO%d", (u16COMPort)); // _MT180L 用
#elif defined(_PARKING_COLUMN_3RD)
            sprintf(szCOMName, "/dev/ttymxc%d", (u16COMPort - 1)); // _MT180L 用
#else
            sprintf(szCOMName, "%s%d", TTY, u16COMPort - 1);
#endif
		}
#ifdef _DEBUG
		printf("szCOMName: %s\n", szCOMName);
#endif
		r = OpenDev(szCOMName);
		if (r < 0)
			return 0;

		hCOM[u16COMPort - 1] = r;

		return setport(hCOM[u16COMPort - 1], u32BaudRate);

#endif // #if defined(_WINDOWS) || defined(WIN32)
	}


	BOOL CloseCOM(UINT16 u16COMPort)
	{
#if defined(_WINDOWS) || defined(WIN32)
		BOOL bl;

		if (!u16COMPort)
			return FALSE;

		if (!hCOM[u16COMPort - 1])
			return TRUE;
		bl = CloseHandle(hCOM[u16COMPort - 1]);
		if (bl)
			hCOM[u16COMPort - 1] = NULL;

		Sleep(20); // 讓硬體放電

		return bl;
#else // #if defined(_WINDOWS) || defined(WIN32)
		//BOOL CloseCOM(UINT16 u16COMPort)
		
		if (!u16COMPort || (u16COMPort > MAX_COMPORT))
			return 0;

		if (hCOM[u16COMPort - 1] < 0)
			return 0;
		close(hCOM[u16COMPort - 1]);

		hCOM[u16COMPort - 1] =  - 1;

		return 1;
#endif // #if defined(_WINDOWS) || defined(WIN32)
	}

#ifndef _WINDOWS

	int ____SetCOMName____(char *COMName)
	{
		if( ttyName != NULL )
		{
			free( ttyName );
		}
		int ttyNameLength = strlen(COMName)+1;
		ttyName = (char*)malloc(ttyNameLength);
		sprintf(ttyName, "%s", COMName );
		return 0;
	}

	/**
	*@breif 打開串口
	*/
	int OpenDev(char *Dev)
	{
		int fd = open(Dev, O_RDWR | O_NOCTTY | O_NDELAY);

		if ( -1 == fd)
		{
			/*設置數據位元數*/
			//DbgPrint("SerialIO---Can't Open Serial Port\n");
			return 0;
		}
		else
			return fd;

	}


	int setport(int fd, int baud)
	{
		int baudrate;
		struct termios T_new;

		switch (baud)
		{
		case 300:
			baudrate = B300;
			break;
		case 600:
			baudrate = B600;
			break;
		case 1200:
			baudrate = B1200;
			break;
		case 2400:
			baudrate = B2400;
			break;
		case 4800:
			baudrate = B4800;
			break;
		case 9600:
			baudrate = B9600;
			break;
		case 19200:
			baudrate = B19200;
			break;
		case 38400:
			baudrate = B38400;
			break;
		case 57600:
			baudrate = B57600;
			break;
		case 115200:
			baudrate = B115200;
			break;
		default:
			baudrate = B38400;
			break;
		}


		/*termios functions use to control asynchronous communications ports*/
		if (tcgetattr(fd, &T_new) != 0)
		{
			/*fetch tty state*/
#ifdef _DEBUG
			printf("tcgetattr failed. errno: %d\r\n", errno);
#endif 
			close(fd);
			return 0;
		}

		/*set 	19200bps, n81, RTS/CTS flow control,
		ignore modem status lines,
		hang up on last close,
		and disable other flags*/
		//T_new.c_cflag = (B19200 | CS8 | CREAD | CLOCAL | HUPCL | CRTSCTS);
		T_new.c_cflag = (baudrate | CS8 | CREAD | CLOCAL);
		T_new.c_oflag = 0;
		T_new.c_iflag = 0;
		T_new.c_lflag = 0;
		if (tcsetattr(fd, TCSANOW, &T_new) != 0)
		{
#ifdef _DEBUG
			printf("tcsetattr failed. errno: %d\r\n", errno);
#endif 
			close(fd);
			return 0;
		}

		return 1;
	}
#endif // #ifndef _WINDOWS

#ifdef __cplusplus
}
#endif
