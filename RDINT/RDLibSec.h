/*定義程式碼片段----Start*/
#define DO_NO_RESP
/*定義程式碼片段----End*/

#define STX		0x02
#define C_STX	0x01
#define STX_NFC	0x04
#define STX_NFC_P2P	0x05
#define ETX		0x03
#define DLE		0x10


#define JIN_ANTENNACONTROL							(0x11)
#define JIN_DEVICECONTROL							(0x13) // 永久設置
#define JIN_DEVICEBLINKCONTROL						(0x1E) // 暫時設置
#define JIN_GET_CONTROLLER_ROLL_CODE				(0x17)
#define JIN_SETCOMMUCATION							(0x18)
#define JIN_GETREADERSERIALNUM						(0x19)
#define JIN_SETREADERSERIALNUM						(0xF5)

#define JIN_OPENCARD								(0x20)
#define JIN_OPENCARD_V1_RET_LEN						(0x07)
#define JIN_CLOSECARD								(0x28)
#define JIN_READMIFAREONEBLOCK						(0x21)
#define JIN_READMIFAREONESECTOR						(0x29)
#define JIN_WRITEMIFAREONEBLOCK						(0x22)
#define JIN_MONEYINIT								(0x23)
#define JIN_MONEYREAD								(0x24)
#define JIN_MONEYINC								(0x25)
#define JIN_MONEYDEC								(0x26)
#define JIN_MONEYBACKUP								(0x27)
#define JIN_KEYCHANGE								(0x2A)

#define JIN_WRITEDEFAULEKEY							(0x2D)

#define JIN_READULTRALIGHT							(0x2E)
#define JIN_WRITEULTRALIGHT							(0x2F)

#define JIN_KBSTART									(0x1A)
#define JIN_KBSTOP									(0x1B)
#define JIN_KBREAD									(0x1C)
#define JIN_KBREAD_RET_LEN_MAX						(0x09)
#define JIN_KBREAD_RET_LEN_MIN						(0x01)
#define JIN_BUTTONCHECK								(0x01)
#define JIN_PASSWDSET								(0x1D)
#define JIN_PASSWDINIT								(0xF6)
#define JIN_WORKINGTYPE								(0x1F)

#define JIN_ISO15693INVENTORY						(0x60)
#define JIN_ISO15693STAYQUIET						(0x61)
#define JIN_ISO15693SELECT							(0x62)
#define JIN_ISO15693RESET2READY						(0x63)
#define JIN_ISO15693READ							(0x64)
#define JIN_ISO15693WRITE							(0x65)
#define JIN_ISO15693LOCKBLOCK						(0x66)
#define JIN_ISO15693WRITEAFI						(0x67)
#define JIN_ISO15693LOCKAFI							(0x68)
#define JIN_ISO15693WRITEDSFID						(0x69)
#define JIN_ISO15693LOCKDSFID						(0x6A)
#define JIN_ISO15693GETSYSINF						(0x6B)
#define JIN_ISO15693GETBLOCKSECURITY				(0x6C)

#define JIN_ICODE1REQUEST							(0x80)
#define JIN_ICODE1READ								(0x81)
#define JIN_ICODE1WRITE								(0x82)
#define JIN_ICODE1HALT								(0x83)
#define JIN_ISO15693USERCOMMAND						(0x84)

#define JIN_ISO14443A4RESET							(0x30)
#define JIN_ISO14443B4RESET							(0x31)
#define JIN_ISO14443AB4COS							(0x32)
#define JIN_STCARDSELECT							(0x41)
#define JIN_STCARDINTODEACTIVE						(0x42)

#define JIN_SAMBAUDRATE								(0x50)	// Fong 20090629
#define JIN_SAMRESET								(0x51)
#define JIN_SAMAPDU									(0x53)
#define JIN_SAMBAUDRATE_2							(0x54)
#define JIN_SAMRESET_2								(0x55)
#define JIN_SAMAPDU_2								(0x56)

#define JIN_RC632REGREAD							(0xFB)
#define JIN_RC632REGWRITE							(0xFA)
#define JIN_RC632CMD								(0xFC)
#define JIN_ISO15693CMD								(0xFF)

#define JIN_ANTENNAAMOUNT							(0x70)
#define JIN_ISO15693AUTOINVENTORYPARAM				(0x6E)
#define JIN_ISO15693AUTOINVENTORY4ANTENNAS			(0x6D)
#define JIN_MIFAREAUTOREQUEST4ANTENNAS				(0x6F)

#define JIN_GETUID									(0x73)
#define JIN_ROLLCODEENABLE							(0xF1)
#define JIN_READMODULETYPE							(0x74)
#define JIN_READMODULENAME							(0xFE)//20150115

#define JIN_SETUNLOCKDOCK							(0x02)
#define JIN_SETTIMEOUT								(0x03)
#define JIN_SETBUZZERLED							(0x05)
#define JIN_SETREADTAGPULSE							(0x04)

#define JIN_CMDDISPATCH								(0x75)
#define JIN_READERRESPTIMEOUT						(0xF9)

#define JIN_SR176READBLOCK							(0x43)
#define JIN_SR176WRITEBLOCK							(0x44)
#define JIN_SR176LOCKBLOCK							(0x45)

#define JIN_SRIX4KREADBLOCK							(0x46)
#define JIN_SRIX4KWRITEBLOCK						(0x47)
#define JIN_SRIX4KAUTH								(0x48)
#define JIN_SRIX4KREADUID							(0x49)

#define JIN_LCDCLEAR								(0xE3)
#define JIN_LCDBACKLIGHT							(0xE4)
#define JIN_LCDDIPLAYSTRING							(0xE5)
#define JIN_LCDDIPLAYPICTURE						(0xE6)

// Update Firmware
#define JIN_FWAP_CHECK_IN_ISP						(0x95)	// 檢查是否進入Isp模式
#define JIN_FWAP_JUMP_TO_ISP						(0x96)	// Jump to Isp
#define JIN_FWAP_JUMP_TO_AP							(0x96)	// Jump to Application
#define JIN_FWAP_WRITE_ISP							(0x97)	// Write Isp

// Page為 2Bytes, MSB
#define JIN_FWAP_WRITE_AP							(0x97)	// Write Application
#define JIN_FWAP_WRITE_ISP_OK						(0x98)	// Check write ISP is OK
#define JIN_FWAP_WRITE_AP_OK						(0x98)	// Check write Application is OK

// BlockNo(1 Byte) + Data[ 128 ]
#define JIN_FWAP_SEND_DATA							(0x99)	// Send Data

// RDINT_NFC(v3) Define
#define MPRD_RCDETECT_ON							(0xB0)	// Start Auto Check
#define MPRD_RCDETECT_OFF							(0xB1)	// Stop Auto Check
#define MPRD_RCDETECT_RST							(0xB2)	// Reset Auto Check
#define MPRD_RCDETECT								(0xB3)	// Event Raise
#define	MPRD_ISO14443A_CMD							(0xB4)
#define	MPRD_ISO14443A_ACT							(0xB5)
#define	MPRD_MIFARE_CMD								(0xB6)
#define	MPRD_ISO14443B_CMD							(0xB7)
#define	MPRD_ISO14443B_ACT							(0xB8)
#define	MPRD_FELICA_CMD								(0xB9)
#define	MPRD_ISO15693_CMD							(0xBA)
#define	MPRD_P2P_SEND_EVT_CMD						(0xBB)
#define	MPRD_P2P_SEND_CONN_CMD						(0xBC)
#define	MPRD_ISO14443A_CD_SEND_EVT_CMD				(0xBD)
#define	MPRD_ISO14443B_CD_SEND_EVT_CMD				(0xBE)
#define MPRD_ISO14443B_GETCARD						(0xA0)
#define MPRD_FELICA_GETCARD							(0xA1)

// Felica Command
#define FELICA_POLLING								(0x00)
#define FELICA_REQ_SERVICE							(0x02)
#define FELICA_REQ_RESPONSE							(0x04)
#define FELICA_READ_NO_ENCRYP						(0x06)
#define FELICA_WRITE_NO_ENCRYP						(0x08)
#define FELICA_REQ_SYS_CODE							(0x0C)

// NDEF
#define	NDEF_BODY_POS_START				(0)
#define	NDEF_BODY_POS_CONTINUE			(1)
#define	NDEF_BODY_POS_END				(2)
#define	NDEF_BODY_POS_1_PKG				(3)

//BaseKeyCard Header
#define BKCH_CARD_TYPE	0x00
#define BKCH_ISUDATE	0x01
#define BKCH_EXPDATE	0x05

#define BKCH_SIZE		0x09

//BaseKey SecurityKey(Satandar)
#define BKSKS_MAID			0x00
#define BKSKS_KEY_TYPE		0x02
#define BKSKS_KEY			0x03
#define BKSKS_ACONDITION	0x0A

#define BKSKS_SIZE			0x10

//BaseKeyCard SecurityCard(Format)
#define BKSKF_KEYA			0x00
#define BKSKF_ACONDITION	0x06
#define BKSKF_KEYB			0x0A

#define BKSKF_SIZE			0x10

#define RDINT_VERSION_1	(4.99f)

enum KEY_FILE_TYPE
{
  KEY_FILE_TYPE_STANDARD, KEY_FILE_TYPE_FORMAT, KEY_FILE_TYPE_STANDARD1 = 'S', KEY_FILE_TYPE_FORMAT1 = 'F'
};

enum STANDAR_KEY_TYPE
{
  STANDAR_KEYA, STANDAR_KEYB, STANDAR_KEYA1 = 'A', STANDAR_KEYB1
};

#if (defined(WIN32) || defined(WINCE))
  #include <windows.h>
#else // #if defined(_WINDOWS) || defined(WIN32)
  #include <wchar.h>

  #ifndef WINAPI
    #define WINAPI
  #endif 

  #ifndef WIN32_DEFINED
    #define WIN32_DEFINED
    #define MAX_PATH	(260)
    #define _T(x)	x

    #define TRUE	(1)
    #define FALSE	(0)

    typedef char CHAR;
    typedef char TCHAR;

    typedef unsigned char BYTE;
    typedef unsigned char BOOL;
    typedef unsigned char *PBYTE;
    typedef unsigned char *LPBYTE;

    typedef unsigned short UINT16;
    typedef unsigned short WORD;
    typedef unsigned short *PUINT16;
    typedef unsigned short *LPWORD;
    typedef unsigned short *PWORD;

    typedef short INT16;
    typedef short *PINT16;

    typedef unsigned int UINT32;
    typedef unsigned int DWORD;
    typedef unsigned int UINT;
    typedef unsigned int *PUINT32;

    typedef int INT;
    typedef int INT32;
    typedef int *PINT32;

    typedef unsigned int ULONG;
    typedef unsigned long HANDLE;
    typedef long LONG;
    typedef long LRESULT;

    typedef const char *LPCSTR;
    typedef const wchar_t *LPCWSTR;
    typedef char *LPSTR;
    typedef wchar_t *LPWSTR;
    typedef LPSTR LPTSTR;
    typedef LPCSTR LPCTSTR;

    typedef void VOID;
    typedef void *LPVOID;
  #endif // #ifndef WIN32_DEFINED
#endif // #if defined(_WINDOWS) || defined(WIN32)

extern INT16(WINAPI *g_KeyBox_FindAIDKey)(PMAID pAID, PUINT32 pu32_CardSerial, LPBYTE pu8_KeyAccessMode, LPBYTE pu8_KeyValue);
extern INT16(WINAPI *g_KeyBox_FindFormatKey)(PMAID pAID, PUINT32 pu32_CardSerial, BYTE u8_MaidType, LPBYTE pu8_SecurityBlock);
extern INT16(WINAPI *g_KeyBox_FindCommonKey)(BYTE u8_CommonKeyType, LPBYTE pu8_KeyA, LPBYTE pu8_AccessCondition, LPBYTE pu8_KeyB);
extern INT16(WINAPI *g_KeyBox_MakeKeyCard_KeyA)(LPBYTE pu8_UserPasswd, PUINT32 pu32_CardSerial, LPBYTE pu8_KeyValueA);
extern INT16(WINAPI *g_KeyBox_MakeKeyFileHeader)(PUINT32 pu32_ExpiryDate, LPBYTE pu8_UserPasswd, BYTE u8_KeyCardType);
extern INT16(WINAPI *g_KeyBox_AddKey)(PMAID pAID, BYTE u8_KeyType, LPBYTE pu8_Key, LPBYTE pu8_KeyA, LPBYTE pu8_AccessCondition, LPBYTE pu8_KeyB);

#define KB_FindAIDKey(pAID,pu32_CardSerial,pu8_KeyAccessMode,pu8_KeyValue)			g_KeyBox_FindAIDKey((PMAID)pAID,(INT32 *)pu32_CardSerial,pu8_KeyAccessMode,pu8_KeyValue)
#define KB_FindFormatKey(pAID,pu32_CardSerial,u8_MaidType,pu8_SecurityBlock)		g_KeyBox_FindFormatKey((PMAID)pAID,(INT32 *)pu32_CardSerial,u8_MaidType,pu8_SecurityBlock)
#define KB_FindCommonKey(u8_CommonKeyType,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)	g_KeyBox_FindCommonKey(u8_CommonKeyType,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)
#define KB_MakeKeyCard_KeyA(pu8_UserPasswd,pu32_CardSerial,pu8_KeyValueA)			g_KeyBox_MakeKeyCard_KeyA(pu8_UserPasswd,(INT32 *)pu32_CardSerial,pu8_KeyValueA)
#define KB_MakeKeyFileHeader(pu32_ExpiryDate,pu8_UserPasswd,u8_KeyCardType)		g_KeyBox_MakeKeyFileHeader((INT32 *)pu32_ExpiryDate,pu8_UserPasswd,u8_KeyCardType)
#define KB_AddKey(pAID,u8_KeyType,pu8_Key,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)	g_KeyBox_AddKey((PMAID)pAID,u8_KeyType,pu8_Key,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)

/*
#else
#include <stdio.h>

extern short KBLib_FindAIDKey(unsigned short u16_Maid, unsigned long *pu32_CardSerial, unsigned char *pu8_KeyAccessMode, unsigned char *pu8_KeyValue);
extern short KBLib_FindFormatKey(unsigned short u16_Maid, unsigned long *pu32_CardSerial, unsigned char u8_MaidType, unsigned char *pu8_SecurityBlock);
extern short KBLib_FindCommonKey(unsigned char u8_CommonKeyType, unsigned char *pu8_KeyA, unsigned char *pu8_AccessCondition, unsigned char *pu8_KeyB);
extern short KBLib_MakeKeyCard_KeyA(unsigned char *pu8_ReaderSerial, unsigned long *pu32_CardSerial, unsigned char *pu8_KeyValueA);
extern short KBLib_MakeKeyFileHeader(unsigned long *pu32_ExpiryDate, unsigned char *pu8_ReaderSerial, unsigned char u8_KeyCardType);
extern short KBLib_AddKey(unsigned short u16_Maid, unsigned char u8_KeyType, unsigned char *pu8_Key, unsigned char *pu8_KeyA, unsigned char *pu8_AccessCondition, unsigned char *pu8_KeyB);

#define KB_FindAIDKey(pAID,pu32_CardSerial,pu8_KeyAccessMode,pu8_KeyValue)			KBLib_FindAIDKey(*(unsigned short *)pAID,(unsigned long *)pu32_CardSerial,pu8_KeyAccessMode,pu8_KeyValue)
#define KB_FindFormatKey(pAID,pu32_CardSerial,u8_MaidType,pu8_SecurityBlock)		KBLib_FindFormatKey(*(unsigned short *)pAID,(unsigned long *)pu32_CardSerial,u8_MaidType,pu8_SecurityBlock)
#define KB_FindCommonKey(u8_CommonKeyType,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)	KBLib_FindCommonKey(u8_CommonKeyType,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)
#define KB_MakeKeyCard_KeyA(pu8_UserPasswd,pu32_CardSerial,pu8_KeyValueA)			KBLib_MakeKeyCard_KeyA(pu8_UserPasswd,(unsigned long *)pu32_CardSerial,pu8_KeyValueA)
#define KB_MakeKeyFileHeader(pu32_ExpiryDate,pu8_UserPasswd,u8_KeyCardType)			KBLib_MakeKeyFileHeader((unsigned long *)pu32_ExpiryDate,pu8_UserPasswd,u8_KeyCardType)
#define KB_AddKey(pAID,u8_KeyType,pu8_Key,pu8_KeyA,pu8_AccessCondition,pu8_KeyB)	KBLib_AddKey(*(unsigned short *)pAID,u8_KeyType,pu8_Key, pu8_KeyA,pu8_AccessCondition,pu8_KeyB)
#endif
 */

/*若是有定義REVERSE_BYTE則反轉內容*/
#ifdef REVERSE_BYTE
  #define Revs Reverse
#else 
  #define Revs(pData,u8_Len)
#endif 

extern void Reverse(void *pData, unsigned char u8_Len); //反轉副函式
