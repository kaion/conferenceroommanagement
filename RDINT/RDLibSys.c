#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RDINT_DEF.h"
#include "RDLib.h"
#include "RDLibSec.h"


#ifdef __cplusplus
extern "C"
{
#endif 

	/*
	unsigned short Sys_OpenReaderByID(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode, unsigned char u8ReaderID)
	{	


	pVar->u8RdID = u8ReaderID;

	return LRSUCCESS;
	}
	*/
	//Fong 20091218 Start
	unsigned short Sys_OpenReaderBasic(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode)
	{
		unsigned short ret;

		pVar->u8RdID = 0x01;
		pVar->u8SecurityMode = u8SecurityMode;


		if ((SECUR_MODE_NONE != u8SecurityMode) && (SECUR_MODE_READER_SEL_NONE != u8SecurityMode))
		{
			MpRd_SetLibAccessCode(pVar, pu8AccessCode);

			if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
			{
				if (LRRDNORESP == ret)
					return LRRDNORESP;
				return LRBADAPPACCESS;
			}
		}

		if ((ret = MpRd_WorkingType(pVar, WT_ISO14443_TYPEA)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
			{
				if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
				{
					if (LRRDNORESP == ret)
						return LRRDNORESP;
					return LRCHANGEWROKTYPE;
				}
			}
		}

		pVar->u1IsOpenReader = TURN_ON;

		return LRSUCCESS;
	}
	//Fong 20091218 End
	unsigned short Sys_OpenReader(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode)
	{
		unsigned short ret;

		pVar->u8RdID = 0x01;
		pVar->u8SecurityMode = u8SecurityMode;

		g_u8Protocol = 0;

		if ((SECUR_MODE_NONE != u8SecurityMode) && (SECUR_MODE_READER_SEL_NONE != u8SecurityMode))
		{
			MpRd_SetLibAccessCode(pVar, pu8AccessCode);

			if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
			{
				g_u8Protocol = 1;
				if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
				{
					if (LRRDNORESP == ret)
						return LRRDNORESP;
					return LRBADAPPACCESS;
				}
			}
		}

		if ((ret = MpRd_WorkingType(pVar, WT_ISO14443_TYPEA)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
			{
				if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
				{
					if (LRRDNORESP == ret)
						return LRRDNORESP;
					return LRCHANGEWROKTYPE;
				}
			}
		}

		// MpRd_AntennaControl 不能讓它 byPass 不然會造成 Access Code 失敗仍會過的問題
		if ((ret = MpRd_AntennaControl(pVar, ANTENNA_SELECT_AUTO_POWER_LOW | ANTENNA_SELECT_ON)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
				return LRRDNORESP;
			return LRRDUNKNOWN;
		}

		MpRd_KbStop(pVar);

		MpRd_DeviceControl(pVar, YELLOW_ON);

		pVar->u1IsOpenReader = TURN_ON;

		return LRSUCCESS;
	}

	unsigned short NFC_Sys_OpenReader(PLIBVAR pVar, unsigned char *pu8AccessCode, unsigned char u8SecurityMode, unsigned char u8Identity)
	{
		unsigned short ret;

		pVar->u8RdID = u8Identity;
		pVar->u8SecurityMode = u8SecurityMode;

		g_u8Protocol = 1;

		if ((SECUR_MODE_NONE != u8SecurityMode) && (SECUR_MODE_READER_SEL_NONE != u8SecurityMode))
		{
			MpRd_SetLibAccessCode(pVar, pu8AccessCode);

			if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
			{
				if (LRRDNORESP == ret)
					return LRRDNORESP;
				return LRBADAPPACCESS;
			}
		}

		if ((ret = MpRd_WorkingType(pVar, WT_ISO14443_TYPEA)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
			{
				if ((ret = MpRd_GetControllerRollCode(pVar)) != LRSUCCESS)
				{
					if (LRRDNORESP == ret)
						return LRRDNORESP;
					return LRCHANGEWROKTYPE;
				}
			}
		}

		// MpRd_AntennaControl 不能讓它 byPass 不然會造成 Access Code 失敗仍會過的問題
		if ((ret = MpRd_AntennaControl(pVar, ANTENNA_SELECT_AUTO_POWER_LOW | ANTENNA_SELECT_ON)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
				return LRRDNORESP;
			return LRRDUNKNOWN;
		}

		MpRd_KbStop(pVar);

		MpRd_DeviceControl(pVar, YELLOW_ON);

		pVar->u1IsOpenReader = TURN_ON;

		return LRSUCCESS;
	}

	unsigned short Sys_CloseReader(PLIBVAR pVar)
	{
		unsigned short ret;

		if (TURN_OFF == pVar->u1IsOpenReader)
			return LRRDNOINIT;

		if ((ret = MpRd_AntennaControl(pVar, TURN_OFF)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
				ret = LRRDLINKLOST;
			else
				ret = LRRDUNKNOWN;
			goto err_Sys_CloseReader;
		}

		MpRd_KbStop(pVar);
		//if( (ret=MpRd_KbStop(pVar)) != LRSUCCESS ){
		//	if( LRRDNORESP == ret ) ret=LRRDLINKLOST; else	ret=LRRDUNKNOWN;
		//	goto err_Sys_CloseReader;
		//}

		MpRd_DeviceControl(pVar, RED_ON | YELLOW_ON);
		//if( (ret=MpRd_DeviceControl(pVar,RED_ON | YELLOW_ON)) != LRSUCCESS ){
		//	if( LRRDNORESP == ret ) ret=LRRDLINKLOST; else ret=LRRDUNKNOWN;
		//	goto err_Sys_CloseReader;
		//}

err_Sys_CloseReader: pVar->u1IsOpenReader = TURN_OFF;

		return ret;
	}

	unsigned short Sys_CloseReaderBasic(PLIBVAR pVar)
	{
		unsigned short ret;

		if (TURN_OFF == pVar->u1IsOpenReader)
			return LRRDNOINIT;

		if ((ret = MpRd_AntennaControl(pVar, TURN_OFF)) != LRSUCCESS)
		{
			if (LRRDNORESP == ret)
				ret = LRRDLINKLOST;
			else
				ret = LRRDUNKNOWN;
			goto err_Sys_CloseReader;
		}

err_Sys_CloseReader: pVar->u1IsOpenReader = TURN_OFF;
		return ret;
	}

#ifdef __cplusplus
}
#endif
