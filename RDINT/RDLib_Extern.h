#ifndef __UART__
  #define __UART__

  #if defined(_WINDOWS) || defined(WIN32)
    #include <windows.h>

    #ifdef UNICODE
      #define AsciiToUINTForHex	AsciiToUINTForHexW
    #else 
      #define AsciiToUINTForHex	AsciiToUINTForHexA
    #endif 
  #else // #if defined(_WINDOWS) || defined(WIN32)
    #include <wchar.h>
    //#define _DEBUG
    //#pragma pack(1)

    #define AsciiToUINTForHex	AsciiToUINTForHexA

    #ifndef WIN32_DEFINED
      #define WIN32_DEFINED
      #define MAX_PATH	(260)
      #define _T(x)	x

      #define TRUE	(1)
      #define FALSE	(0)

      typedef char CHAR;
      typedef char TCHAR;

      typedef unsigned char BYTE;
      typedef unsigned char BOOL;
      typedef unsigned char *PBYTE;
      typedef unsigned char *LPBYTE;

      typedef unsigned short UINT16;
      typedef unsigned short WORD;
      typedef unsigned short *PUINT16;
      typedef unsigned short *LPWORD;
      typedef unsigned short *PWORD;

      typedef short INT16;
      typedef short *PINT16;

      typedef unsigned int UINT32;
      typedef unsigned int DWORD;
      typedef unsigned int UINT;
      typedef unsigned int *PUINT32;

      typedef int INT;
      typedef int INT32;
      typedef int *PINT32;

      typedef unsigned int ULONG;
      typedef unsigned long HANDLE;
      typedef long LONG;
      typedef long LRESULT;

      typedef const char *LPCSTR;
      typedef const wchar_t *LPCWSTR;
      typedef char *LPSTR;
      typedef wchar_t *LPWSTR;
      typedef LPSTR LPTSTR;
      typedef LPCSTR LPCTSTR;

      //typedef void VOID;
      #define VOID void
      typedef void *LPVOID;
    #endif // #ifndef WIN32_DEFINED
  #endif // #if defined(_WINDOWS) || defined(WIN32)


  #ifdef __cplusplus
    extern "C"
    {
    #endif 

    UINT32 SendFunc(UINT16 u16COMPort, BYTE *pcu8Data, UINT32 u32DataLen);
    UINT32 RecvFunc(UINT16 u16COMPort, BYTE *pu8Data, UINT32 u32DataLen);
    ULONG GetUnixTimeForNow(VOID);
    ULONG GetTickFunc(VOID);
    UINT AsciiToUINTForHexA(LPCSTR lpStr);
    UINT AsciiToUINTForHexW(LPCWSTR lpStr);

    int OpenCOM(UINT16 u16COMPort, DWORD BaudRate);
    BOOL CloseCOM(UINT16 u16COMPort);

    #ifdef __cplusplus
    }
  #endif 

#endif
