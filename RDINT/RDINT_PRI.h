#ifndef __RDINT_PRI__
  #define __RDINT_PRI__

  #define KEY_FILE_NAME	_T("svchost.dat")

  #if defined(_WINDOWS) || defined(WIN32)

    #include <windows.h>
    #include "RDLib.h"

    #ifdef _UNICODE
      #define RDINTsys_OpenReaderBasic		RDINTsys_OpenReaderBasicW
      #define RDINT_PRINT_ERROR				RDINT_PRINT_ERROR_W
      #define RDINT_AccessCodeSet				RDINT_AccessCodeSetW
      #define RDINT_SetLibAccessCode			RDINT_SetLibAccessCodeW
      #define RDINTsys_PrintError				RDINTsys_PrintErrorW
      #define RDINTv2_KeyFileCopyToKeyBox		RDINTv2_KeyFileCopyToKeyBoxW
      #define RDINTv2_EngineerMode			RDINTv2_EngineerModeW
      #define RDINTv2_RC632Cmd				RDINTv2_RC632CmdW
      #define RDINTv2_ISO15693Cmd				RDINTv2_ISO15693CmdW
      #define RDINTv2_OpenSAM					RDINTv2_OpenSAMW
    #else 
      #define RDINTsys_OpenReaderBasic		RDINTsys_OpenReaderBasicA
      #define RDINT_PRINT_ERROR				RDINT_PRINT_ERROR_A
      #define RDINT_AccessCodeSet				RDINT_AccessCodeSetA
      #define RDINT_SetLibAccessCode			RDINT_SetLibAccessCodeA
      #define RDINTsys_PrintError				RDINTsys_PrintErrorA
      #define RDINTv2_KeyFileCopyToKeyBox		RDINTv2_KeyFileCopyToKeyBoxA
      #define RDINTv2_EngineerMode			RDINTv2_EngineerModeA
      #define RDINTv2_RC632Cmd				RDINTv2_RC632CmdA
      #define RDINTv2_ISO15693Cmd				RDINTv2_ISO15693CmdA
      #define RDINTv2_OpenSAM					RDINTv2_OpenSAMA
    #endif 
  #else // #if defined(_WINDOWS) || defined(WIN32)
    #include "RDLib.h"

    #define RDINTsys_OpenReaderBasic		RDINTsys_OpenReaderBasicA
    #define RDINT_PRINT_ERROR				RDINT_PRINT_ERROR_A
    #define RDINT_AccessCodeSet				RDINT_AccessCodeSetA
    #define RDINT_SetLibAccessCode			RDINT_SetLibAccessCodeA
    #define RDINTsys_PrintError				RDINTsys_PrintErrorA
    #define RDINTv2_KeyFileCopyToKeyBox		RDINTv2_KeyFileCopyToKeyBoxA
    #define RDINTv2_EngineerMode			RDINTv2_EngineerModeA
    #define RDINTv2_RC632Cmd				RDINTv2_RC632CmdA
    #define RDINTv2_ISO15693Cmd				RDINTv2_ISO15693CmdA
    #define RDINTv2_OpenSAM					RDINTv2_OpenSAMA
  #endif 

  #define RDLIST_NOT_NULL	(1)
  #define RDLIST_NULL		(0)

  #ifdef __cplusplus
    extern "C"
    {
    #endif 

    enum DESFIRE_DEF
    {
      DESFIRE_DES = 1, DESFIRE_3DES = 2, 
    };

    typedef struct __LIBRD
    {
      LIBVAR libVar;

      struct __LIBRD *pPrev;
      struct __LIBRD *pNext;
      //***** ExtFunc *****
      //TI ISO15693
      unsigned char u8Uid[8];
    } LIBRD,  *PLIBRD;

    typedef struct __RDINF
    {
      PLIBRD pRdList;
      PLIBRD pRdNowP;

      BYTE u8NowRdId;

      unsigned char pu8RecvBuffer[LIB_BUFFER_MAX];
      unsigned char pu8SendBuffer[LIB_BUFFER_MAX];
      unsigned char pu8DataBuf[LIB_BUFFER_MAX];

	  unsigned char pu8EventBuffer[LIB_BUFFER_MAX];//20140710

      unsigned char pu8Apdu[256];
      unsigned char pu8ApduResp[256];

      // DESFire 專用
      unsigned char pu8DESFireSend[60];
      unsigned char pu8DESFireResp[60];

      struct __RDINF *pPrev;
      struct __RDINF *pNext;
    } RDINF,  *PRDINF;

    /*
    typedef struct __LIBRD
    {
    HLOCAL hLocal;
    LIBVAR libVar;

    struct __LIBRD *pPrev;
    struct __LIBRD *pNext;
    // ***** ExtFunc *****
    //TI ISO15693
    unsigned char u8Uid[8];
    }LIBRD, * PLIBRD;

    typedef struct __RDINF{
    HLOCAL hLocal;

    PLIBRD pRdList;
    PLIBRD pRdNowP;

    BYTE u8NowRdId;

    unsigned char pu8RecvBuffer[LIB_BUFFER_MAX];
    unsigned char pu8SendBuffer[LIB_BUFFER_MAX];
    unsigned char pu8DataBuf[LIB_BUFFER_MAX];

    unsigned char pu8Apdu[256];
    unsigned char pu8ApduResp[256];

    struct __RDINF *pPrev;
    struct __RDINF *pNext;
    }RDINF, *PRDINF;
     */


    //#if (defined(WIN32) || defined(WINCE))
#if (defined(WIN32) || defined(WINCE))
	#ifndef WINCE
		// 20131230 Start
		typedef HDEVINFO (WINAPI *NFC_SetupDiGetClassDevsW)(_In_opt_ CONST GUID *ClassGuid, _In_opt_ PCWSTR Enumerator, _In_opt_ HWND hwndParent, _In_ DWORD Flags);
		typedef HDEVINFO (WINAPI *NFC_SetupDiGetClassDevsA)(_In_opt_ CONST GUID *ClassGuid, _In_opt_ PCSTR Enumerator, _In_opt_ HWND hwndParent, _In_ DWORD Flags);
		typedef BOOL (WINAPI *NFC_SetupDiGetDeviceInterfaceDetailW)(_In_ HDEVINFO DeviceInfoSet, _In_ PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData, _Out_writes_bytes_to_opt_(DeviceInterfaceDetailDataSize, *RequiredSize) PSP_DEVICE_INTERFACE_DETAIL_DATA_W DeviceInterfaceDetailData, _In_ DWORD DeviceInterfaceDetailDataSize, _Out_opt_ _Out_range_(>=, sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_W)) PDWORD RequiredSize, _Out_opt_ PSP_DEVINFO_DATA DeviceInfoData);
		typedef BOOL (WINAPI *NFC_SetupDiGetDeviceInterfaceDetailA)(_In_ HDEVINFO DeviceInfoSet, _In_ PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData, _Inout_updates_bytes_to_opt_(DeviceInterfaceDetailDataSize, *RequiredSize) PSP_DEVICE_INTERFACE_DETAIL_DATA_A DeviceInterfaceDetailData, _In_ DWORD DeviceInterfaceDetailDataSize, _Out_opt_ _Out_range_(>=, sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_A)) PDWORD RequiredSize, _Out_opt_ PSP_DEVINFO_DATA DeviceInfoData);
		typedef BOOL (WINAPI *NFC_SetupDiEnumDeviceInfo)(_In_ HDEVINFO DeviceInfoSet,	_In_ DWORD MemberIndex,	_Out_ PSP_DEVINFO_DATA DeviceInfoData);
		typedef BOOL (WINAPI *NFC_SetupDiEnumDeviceInterfaces)(_In_ HDEVINFO DeviceInfoSet, _In_opt_ PSP_DEVINFO_DATA DeviceInfoData, _In_ CONST GUID *InterfaceClassGuid, _In_ DWORD MemberIndex, _Out_ PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData);
		typedef BOOL (WINAPI *NFC_SetupDiDestroyDeviceInfoList)(_In_ HDEVINFO DeviceInfoSet);
		typedef BOOL (WINAPI *NFC_SetupDiCallClassInstaller)(_In_ DI_FUNCTION InstallFunction, _In_ HDEVINFO DeviceInfoSet, _In_opt_ PSP_DEVINFO_DATA DeviceInfoData);
		typedef BOOL (WINAPI *NFC_SetupDiSetClassInstallParams)( _In_ HDEVINFO DeviceInfoSet, _In_opt_ PSP_DEVINFO_DATA DeviceInfoData, _In_opt_ PSP_CLASSINSTALL_HEADER ClassInstallParams, _In_ DWORD ClassInstallParamsSize);
		typedef BOOL (WINAPI *NFC_WinUsb_Initialize)(_In_ HANDLE DeviceHandle, _Out_ PWINUSB_INTERFACE_HANDLE InterfaceHandle);
		typedef BOOL (WINAPI *NFC_WinUsb_QueryDeviceInformation)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ ULONG InformationType, _Inout_ PULONG BufferLength, _Out_ PVOID Buffer);
		typedef BOOL (WINAPI *NFC_WinUsb_QueryInterfaceSettings)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR AlternateSettingNumber, _Out_ PUSB_INTERFACE_DESCRIPTOR UsbAltInterfaceDescriptor);
		typedef BOOL (WINAPI *NFC_WinUsb_QueryPipe)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR AlternateInterfaceNumber, _In_ UCHAR PipeIndex, _Out_ PWINUSB_PIPE_INFORMATION PipeInformation);
		typedef BOOL (WINAPI *NFC_WinUsb_ControlTransfer)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ WINUSB_SETUP_PACKET SetupPacket, _Out_ PUCHAR Buffer, _In_ ULONG BufferLength, _Out_opt_ PULONG LengthTransferred, _In_opt_ LPOVERLAPPED Overlapped);
		typedef BOOL (WINAPI *NFC_WinUsb_WritePipe)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR PipeID, _In_ PUCHAR Buffer, _In_ ULONG BufferLength, _Out_opt_ PULONG LengthTransferred, _In_opt_ LPOVERLAPPED Overlapped);
		typedef BOOL (WINAPI *NFC_WinUsb_ReadPipe)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR PipeID, _Out_ PUCHAR Buffer, _In_ ULONG BufferLength, _Out_opt_ PULONG LengthTransferred, _In_opt_ LPOVERLAPPED Overlapped);
		typedef BOOL (WINAPI *NFC_WinUsb_SetPipePolicy)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR PipeID, _In_ ULONG PolicyType, _In_ ULONG ValueLength, _In_ PVOID Value);
		typedef BOOL (WINAPI *NFC_WinUsb_FlushPipe)(_In_  WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_ UCHAR PipeID);
		typedef BOOL (WINAPI *NFC_WinUsb_Free)(_In_ WINUSB_INTERFACE_HANDLE InterfaceHandle);

		// 20131230 End

		//20140808
		typedef BOOL (WINAPI *NFC_SetupDiChangeState)(_In_ HDEVINFO DeviceInfoSet, _In_opt_ PSP_DEVINFO_DATA DeviceInfoData);

		typedef BOOL (WINAPI *NFC_WinUsb_ResetPipe)( _In_  WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_  UCHAR PipeID );
		typedef BOOL (WINAPI *NFC_WinUsb_AbortPipe)( _In_  WINUSB_INTERFACE_HANDLE InterfaceHandle, _In_  UCHAR PipeID );
	#endif
#endif

    typedef INT32(WINAPI *KeyBox_RegisterKeyFile)(LPTSTR strFileName);
    typedef INT32(WINAPI *KeyBox_DeleteKeyFile)(LPTSTR strFileName);
    typedef INT32(WINAPI *KeyBox_Start)(LPTSTR strFileName, PUINT32 pu32CheckDate, BYTE u1CheckContext);

    typedef INT32(WINAPI *KeyBox_CloseKeyFile)(VOID);
    typedef INT32(WINAPI *KeyBox_WriteToKeyFile)(VOID);

    typedef INT32(WINAPI *KeyBox_FindAIDKey)(PMAID pAID, PUINT32 pu32_CardSerial, LPBYTE pu8_KeyAccessMode, LPBYTE pu8_KeyValue);
    typedef INT32(WINAPI *KeyBox_FindFormatKey)(PMAID pAID, PUINT32 pu32_CardSerial, BYTE u8_MaidType, LPBYTE pu8_SecurityBlock);
    typedef INT32(WINAPI *KeyBox_FindCommonKey)(BYTE u8_CommonKeyType, LPBYTE pu8_KeyA, LPBYTE pu8_AccessCondition, LPBYTE pu8_KeyB);
    typedef INT32(WINAPI *KeyBox_MakeKeyCard_KeyA)(LPBYTE pu8_UserPasswd, PUINT32 pu32_CardSerial, LPBYTE pu8_KeyValueA);
    typedef INT32(WINAPI *KeyBox_MakeKeyFileHeader)(PUINT32 pu32_ExpiryDate, LPBYTE pu8_UserPasswd, BYTE u8_KeyCardType);
    typedef INT32(WINAPI *KeyBox_AddKey)(PMAID pAID, BYTE u8_KeyType, LPBYTE pu8_Key, LPBYTE pu8_KeyA, LPBYTE pu8_AccessCondition, LPBYTE pu8_KeyB);

    //#endif

    extern KeyBox_RegisterKeyFile g_KeyBox_RegisterKeyFile;
    extern KeyBox_DeleteKeyFile g_KeyBox_DeleteKeyFile;
    extern KeyBox_Start g_KeyBox_Start;

    extern KeyBox_CloseKeyFile g_KeyBox_CloseKeyFile;
    extern KeyBox_WriteToKeyFile g_KeyBox_WriteToKeyFile;

    extern KeyBox_FindAIDKey g_KeyBox_FindAIDKey;
    extern KeyBox_FindFormatKey g_KeyBox_FindFormatKey;
    extern KeyBox_FindCommonKey g_KeyBox_FindCommonKey;
    extern KeyBox_MakeKeyCard_KeyA g_KeyBox_MakeKeyCard_KeyA;
    extern KeyBox_MakeKeyFileHeader g_KeyBox_MakeKeyFileHeader;
    extern KeyBox_AddKey g_KeyBox_AddKey;


    extern PRDINF g_pRdinf;
    extern PRDINF g_pRdinf_v1;
    extern BOOL u1AlreadyLoadKeyFile;
    extern int g_WorkType;
    extern int g_DelayTime;

	//20140808
	extern int RestartMPUsbDevice();

    extern unsigned char g_strAPI_AccessCode[];

    PRDINF RDINF_Add(BYTE u8COMPort);
    INT RDINF_Delete(PRDINF pr);
    LRESULT RDINF_Search(PRDINF *ppr, BYTE u8COMPort);

    INT32 CheckKeyFile(VOID);
    INT32 LoadKeyBoxKey(VOID);

    typedef VOID(CALLBACK *RDINT_PRINT_ERROR_A)(LPSTR strErrStr);
    typedef VOID(CALLBACK *RDINT_PRINT_ERROR_W)(LPWSTR strErrStr);

    /************************以下為RDINT V2新增讀卡機連線傳輸未公開函式********************************************/
    RDINTAPI_INT32 RDINTsys_OpenReaderBasicA(BYTE u8COMPort, UINT32 u32Baudrate, LPSTR strAccessCode, BYTE u8SecurityMode, UINT32 u32OpenDelayMs, PUINT32 pu32Baudrate);
    RDINTAPI_INT32 RDINTsys_OpenReaderBasicW(BYTE u8COMPort, UINT32 u32Baudrate, LPWSTR strAccessCode, BYTE u8SecurityMode, UINT32 u32OpenDelayMs, PUINT32 pu32Baudrate);
    RDINTAPI_INT32 RDINTsys_CloseReaderBasic(BYTE u8COMPort);

    RDINTAPI_INT32 RDINTsys_CloseReaderAll(VOID);

    RDINTAPI_INT32 RDINTsys_OpenCOM(BYTE u8COMPort, UINT32 u32Baudrate);
    RDINTAPI_INT32 RDINTsys_CloseCOM(BYTE u8COMPort);

	RDINTAPI_VOID RDINTsys_SetProtocolMode(BYTE u8Protocol);//20150209

    RDINTAPI_INT32 RDINTsys_ReadCOM(BYTE u8COMPort, LPBYTE pu8RecvData, PUINT16 pu16Len);
    RDINTAPI_INT32 RDINTsys_WriteCOM(BYTE u8COMPort, LPBYTE pu8SendData, UINT16 u16Len);
    RDINTAPI_INT32 RDINTsys_GetTicks(UINT32 *pu32Ticks);

    RDINTAPI_VOID RDINTsys_PrintErrorA(UINT16 u16ErrorCode, UINT32 u32LocaleID, RDINT_PRINT_ERROR_A pfnPrintError);
    RDINTAPI_VOID RDINTsys_PrintErrorW(UINT16 u16ErrorCode, UINT32 u32LocaleID, RDINT_PRINT_ERROR_W pfnPrintError);
	

    /************************以下為RDINT V2新增讀卡機韌體更新未公開函式********************************************/
    RDINTAPI_INT32 RDINTup_GoIntoIspMP501(BYTE u8COMPort);
    RDINTAPI_INT32 RDINTup_GoTntoIspMP506(BYTE u8COMPort);
    RDINTAPI_INT32 RDINTup_EraseChip(BYTE u8COMPort);
    RDINTAPI_INT32 RDINTup_ProgramChip(BYTE u8COMPort, LPBYTE pu8Rom, UINT16 u16Len);
    RDINTAPI_INT32 RDINTup_RestartSystem(BYTE u8COMPort);

    // 檢查是否進入 ISP 模式, 0xC3 = AP模式, 0x3C = ISP模式
    RDINTAPI_INT32 RDINTup_ST_CheckInIsp(BYTE u8COMPort, LPBYTE pu8Mode);
    // Jump to Isp
    RDINTAPI_INT32 RDINTup_ST_JumpToIsp(BYTE u8COMPort);
    // Jump to Application
    RDINTAPI_INT32 RDINTup_ST_JumpToApplication(BYTE u8COMPort);
    // Write Isp
    RDINTAPI_INT32 RDINTup_ST_WriteIsp(BYTE u8COMPort, UINT16 u8Page);
    // Write Application
    RDINTAPI_INT32 RDINTup_ST_WriteAppliction(BYTE u8COMPort, UINT16 u8Page);
    // Check write ISP is OK
    RDINTAPI_INT32 RDINTup_ST_WriteIspOK(BYTE u8COMPort);
    // Check write Application is OK
    RDINTAPI_INT32 RDINTup_ST_WriteApplicationOK(BYTE u8COMPort);
    // Send Data
    RDINTAPI_INT32 RDINTup_ST_SendData(BYTE u8COMPort, BYTE u8BlockNo, LPBYTE pu8Data);
    /************************以下為RDINT V2新增讀卡機通訊未公開函式********************************************/
    RDINTAPI_INT32 RDINT_GetControllerRollCode(BYTE u8COMPort);
    RDINTAPI_INT32 RDINT_SetCommucation(BYTE u8COMPort, BYTE u8Type);

    RDINTAPI_INT32 RDINT_SetReaderSerialNum(BYTE u8COMPort, LPBYTE pu8SerialNum);

    RDINTAPI_INT32 RDINT_PasswordSet(BYTE u8COMPort, LPBYTE pu8OldPasswd, LPBYTE pu8NewPasswd);
    RDINTAPI_INT32 RDINT_AccessCodeSetA(BYTE u8COMPort, LPSTR strOldAccessCode, LPSTR strNewAccessCode);
    RDINTAPI_INT32 RDINT_AccessCodeSetW(BYTE u8COMPort, LPWSTR strOldAccessCode, LPWSTR strNewAccessCode);
    RDINTAPI_INT32 RDINT_PasswordInit(BYTE u8COMPort);

    //RDINTAPI_INT32 RDINT_SamReset(BYTE u8COMPort, BYTE u8SlotNo, LPBYTE pu8Resp, UINT16 *pu16RespLen);
    //RDINTAPI_INT32 RDINT_SamApdu(BYTE u8COMPort, BYTE u8SlotNo, LPBYTE pu8ApduData, UINT16 u16ApduLen, LPBYTE pu8Resp, UINT16 *pu16RespLen);

    // Fong 20090629 Start
    RDINTAPI_INT32 RDINT_SamApdu(BYTE u8COMPort, LPBYTE pu8ApduData, UINT16 u16ApduLen, LPBYTE pu8Resp, UINT16 *pu16RespLen);
    // Fong 20090629 End

    RDINTAPI_INT32 RDINT_RC632RegRead(BYTE u8COMPort, BYTE u8RegSelect, LPBYTE pu8RegData);
    RDINTAPI_INT32 RDINT_RC632RegWrite(BYTE u8COMPort, BYTE u8RegSelect, BYTE u8RegData);
    RDINTAPI_INT32 RDINT_RC632Cmd(BYTE u8COMPort, BYTE u8WaitTime, BYTE u8CmdType, LPBYTE pu8Cmd, BYTE u8CmdLen, LPBYTE pu8Recv, LPBYTE pu8RecvLen);
    RDINTAPI_INT32 RDINT_ISO15693Cmd(BYTE u8COMPort, BYTE u8WaitTime, BYTE u8CmdLen, LPBYTE pu8Cmd);

    RDINTAPI_INT32 RDINT_RollCodeEnable(BYTE u8COMPort, BYTE u1Enable);
    RDINTAPI_INT32 RDINT_CmdDispatch(BYTE u8COMPort, LPBYTE pu8SendCmd, BYTE u8SendCmdLen, LPBYTE pu8GRLCmd);

    RDINTAPI_INT32 RDINT_ReaderRespTimeOut(BYTE u8COMPort, BYTE u8TimeOut);

    RDINTAPI_INT32 RDINT_EngineerMode(BYTE u8COMPort, LPBYTE pu8CMD, LPBYTE pu8Recv, UINT32 u32DelayMs);

    RDINTAPI_INT32 RDINT_SetLibAccessCodeA(BYTE u8COMPort, LPSTR strAccessCode);
    RDINTAPI_INT32 RDINT_SetLibAccessCodeW(BYTE u8COMPort, LPWSTR strAccessCode);

    /************************以下為RDINT V2新增演算法未公開函式********************************************/
    RDINTAPI_INT32 RDINTv2_ReaderInfoA(BYTE u8COMPort, LPSTR strSerialNum, LPSTR strFirmware);
    RDINTAPI_INT32 RDINTv2_ReaderInfoW(BYTE u8COMPort, LPWSTR strSerialNum, LPWSTR strFirmware);

    RDINTAPI_INT32 RDINTv2_OpenCardAndVer(BYTE u8COMPort, PUINT32 pu32CardSerial, PUINT16 pu16Atqa);
    //RDINTAPI_INT32 RDINTv2_EngineerModeA(BYTE u8COMPort, LPCSTR strPassword, LPCSTR strCMD, LPSTR strReciveData, UINT32 u32DelayMs);
    //RDINTAPI_INT32 RDINTv2_EngineerModeW(BYTE u8COMPort, LPCWSTR strPassword, LPCWSTR strCMD, LPWSTR strReciveData, UINT32 u32DelayMs);
    RDINTAPI_INT32 RDINTv2_RC632CmdA(BYTE u8COMPort, LPCSTR strPassword, BYTE u8WaitTime, BYTE u8CmdType, LPCSTR strCMD, LPSTR strReciveData);
    RDINTAPI_INT32 RDINTv2_RC632CmdW(BYTE u8COMPort, LPCWSTR strPassword, BYTE u8WaitTime, BYTE u8CmdType, LPCWSTR strCMD, LPWSTR strReciveData);
    RDINTAPI_INT32 RDINTv2_ISO15693CmdA(BYTE u8COMPort, LPCSTR strPassword, BYTE u8WaitTime, LPCSTR strCMD);
    RDINTAPI_INT32 RDINTv2_ISO15693CmdW(BYTE u8COMPort, LPCWSTR strPassword, BYTE u8WaitTime, LPCWSTR strCMD);
    RDINTAPI_INT32 RDINTv2_KeyFileCopyToKeyBoxA(LPCSTR strDestPath);
    RDINTAPI_INT32 RDINTv2_KeyFileCopyToKeyBoxW(LPCWSTR strDestPath);

    RDINTAPI_INT32 RDINTv2_OpenSAMW(BYTE u8COMPort, BYTE u8SlotNo, LPWSTR strPINCode);
    RDINTAPI_INT32 RDINTv2_OpenSAMA(BYTE u8COMPort, BYTE u8SlotNo, LPSTR strPINCode);
    RDINTAPI_INT32 RDINTv2_FindAIDKeyInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, LPBYTE pu8KeyAccessMode, LPBYTE pu8Key);
    RDINTAPI_INT32 RDINTv2_FindFormatKeyInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8MaidType, LPBYTE pu8SecurData);

    RDINTAPI_INT32 RDINTv2_ReadCardDirInSAM(BYTE u8COMPort, BYTE u8SlotNo, PMAID pMadS50, PMAID pMadS70, UINT16 u16Gid);
    RDINTAPI_INT32 RDINTv2_ReadCardFileInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8StartBlk, BYTE u8BlkNo, LPBYTE pu8BlkRead, LPBYTE pu8DataBlks);
    RDINTAPI_INT32 RDINTv2_WriteCardFileInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8StartBlk, LPBYTE pu8DataBlks, BYTE u8BlkNo, LPBYTE pu8BlkWritten);
    RDINTAPI_INT32 RDINTv2_ReadCardInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8StartBlk, BYTE u8BlkNo, LPBYTE pu8BlkRead, LPBYTE pu8DataBlks, UINT16 u16Gid);
    RDINTAPI_INT32 RDINTv2_WriteCardInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8StartBlk, LPBYTE pu8DataBlks, BYTE u8BlkNo, LPBYTE pu8BlkWritten, UINT16 u16Gid);

    RDINTAPI_INT32 RDINTv2_IncrementPurseInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_DecrementPurseInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_ReadPurseInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_BackupPurseInSAM(BYTE u8COMPort, BYTE u8SlotNo, PGMAID pGMaid, BYTE u8SrcBlkNo, BYTE u8DestBlkNo);

    RDINTAPI_INT32 RDINTv2_FormatCardInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, UINT16 u16Gid);
    RDINTAPI_INT32 RDINTv2_UnformatCardInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, BYTE u1DeleteAllFile, UINT16 u16Gid);
    RDINTAPI_INT32 RDINTv2_AddCardFileInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, PGMAID pGMaid, BYTE u8BlkNo, LPBYTE pu8FileContact);
    RDINTAPI_INT32 RDINTv2_AddCardPurseInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_DeleteCardFileInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, PGMAID pGMaid);
    RDINTAPI_INT32 RDINTv2_DeleteAllFileInSAM(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, UINT16 u16Gid);

    RDINTAPI_INT32 RDINTv2_ReadCardInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8StartBlk, BYTE u8BlkNo, LPBYTE pu8BlkRead, LPBYTE pu8DataBlks);
    RDINTAPI_INT32 RDINTv2_WriteCardInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8StartBlk, LPBYTE pu8DataBlks, BYTE u8BlkNo, LPBYTE pu8BlkWritten);

    RDINTAPI_INT32 RDINTv2_IncrementPurseInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_DecrementPurseInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_ReadPurseInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_BackupPurseInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u2KeyType, PGMAID pGMaid, BYTE u8SrcBlkNo, BYTE u8DestBlkNo);

    RDINTAPI_INT32 RDINTv2_AddCardFileInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, LPBYTE pu8AccessCondition, PGMAID pGMaid, BYTE u8Sector, BYTE u8BlkNo, LPBYTE pu8FileContact);
    RDINTAPI_INT32 RDINTv2_AddCardPurseInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, LPBYTE pu8AccessCondition, PGMAID pGMaid, BYTE u8Sector, BYTE u8BlkNo, PUINT32 pu32Value);
    RDINTAPI_INT32 RDINTv2_DeleteCardFileInSAMByGMAID(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CommonKeyType, BYTE u2KeyType, PGMAID pGMaid, BYTE u8Sector);

    RDINTAPI_INT32 RDINTv2_KbdSet(BYTE u8COMPort, BYTE u8KbdStatus);
    RDINTAPI_INT32 RDINTv2_KbdGetStringA(BYTE u8COMPort, LPSTR strKbd);
    RDINTAPI_INT32 RDINTv2_KbdGetStringW(BYTE u8COMPort, LPWSTR strKbd);

    /************************以下為SAM新增讀卡機通訊函式********************************************/
    RDINTAPI_INT32 RDINTsam_ExternalAuthenticate(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8SecLv);
    RDINTAPI_INT32 RDINTsam_GetChallenge(BYTE u8COMPort, BYTE u8SlotNo, BYTE pu8ChallengeCode[8]);
    RDINTAPI_INT32 RDINTsam_GetStatus(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8RefCtrl, SAMAIDINF pAidInf[], LPBYTE pu8AidCount);
    RDINTAPI_INT32 RDINTsam_InitializeUpdate(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8KeySetVers, BYTE u8KeyIndex, BYTE pu8Challenge[8], BYTE pu8KeyAuth[16], BYTE pu8KeyMAC[16]);
    RDINTAPI_INT32 RDINTsam_ReadBinary(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8OffsetMSB, BYTE u8OffsetLSB, BYTE pu8Out[256], LPBYTE pu8OutLen);
    RDINTAPI_INT32 RDINTsam_Select(BYTE u8COMPort, BYTE u8SlotNo, BYTE u16SamFileId, BYTE pu8Out[23], LPBYTE pu8OutLen);
    RDINTAPI_INT32 RDINTsam_SelectApplication(BYTE u8COMPort, BYTE u8SlotNo, BYTE pu8Aid[10], BYTE u8AidLen);
    RDINTAPI_INT32 RDINTsam_VerifyCHV(BYTE u8COMPort, BYTE u8SlotNo, BYTE u8CHV, BYTE pu8PINCode[8]);
    /************************ 以下為 DESFire 新增讀卡機通訊函式 ********************************************/
    RDINTAPI_INT32 RDINTv2_DESFire_Authenticate(BYTE u8COMPort, BYTE u8KeyNo, BYTE pu8Key[16], BYTE u8CryptType);
    RDINTAPI_INT32 RDINTv2_DESFire_ChangeKeySettings(BYTE u8COMPort, BYTE u8KeySet);
    RDINTAPI_INT32 RDINTv2_DESFire_GetKeySettings(BYTE u8COMPort, LPBYTE pu8KeySet, LPBYTE pu8KeyNum);
    RDINTAPI_INT32 RDINTv2_DESFire_ChangeKey(BYTE u8COMPort, BYTE u8KeyNo, BYTE pu8OldKey[16], BYTE pu8NewKey[16]);
    RDINTAPI_INT32 RDINTv2_DESFire_GetKeyVersion(BYTE u8COMPort, BYTE u8KeyNo, LPBYTE pu8KeyVer);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateApplication(BYTE u8COMPort, BYTE pu8AID[3], BYTE u8KeySet, BYTE pu8KeyNum);
    RDINTAPI_INT32 RDINTv2_DESFire_DeleteApplication(BYTE u8COMPort, BYTE pu8AID[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_GetApplicationIDs(BYTE u8COMPort, LPBYTE pu8AID, LPBYTE pu8AIDTotal);
    RDINTAPI_INT32 RDINTv2_DESFire_SelectApplication(BYTE u8COMPort, BYTE pu8AID[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_FormatPICC(BYTE u8COMPort);
    RDINTAPI_INT32 RDINTv2_DESFire_GetVersion(BYTE u8COMPort, Pdesfire_Version pDESfireVersion);
    RDINTAPI_INT32 RDINTv2_DESFire_GetFileIDs(BYTE u8COMPort, LPBYTE pu8FID, LPBYTE pu8FIDTotal);
    RDINTAPI_INT32 RDINTv2_DESFire_GetFileSettings(BYTE u8COMPort, BYTE u8FileNo, Pdesfire_FileSetting pFileSet);
    RDINTAPI_INT32 RDINTv2_DESFire_ChangeFileSettings(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2]);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateStdDataFile(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2], BYTE pu8FileSize[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateBackupDataFile(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2], BYTE pu8FileSize[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateValueFile(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2], BYTE pu8LowerLimit[4], BYTE pu8UpperLimit[4], BYTE pu8Value[4], BYTE u8LmtCreditEnab);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateLinearRecordFile(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2], BYTE pu8RecordSize[3], BYTE pu8MaxNumOfRecords[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_CreateCyclicRecordFile(BYTE u8COMPort, BYTE u8FileNo, BYTE u8ComSet, BYTE pu8AccessRight[2], BYTE pu8RecordSize[3], BYTE pu8MaxNumOfRecords[3]);
    RDINTAPI_INT32 RDINTv2_DESFire_DeleteFile(BYTE u8COMPort, BYTE u8FileNo);
    RDINTAPI_INT32 RDINTv2_DESFire_ReadFile(BYTE u8COMPort, BYTE u8FileNo, int i32Offset, int i32Length, LPBYTE pu8Data);
    RDINTAPI_INT32 RDINTv2_DESFire_WriteFile(BYTE u8COMPort, BYTE u8FileNo, int i32Offset, int i32Length, LPBYTE pu8Data);
    RDINTAPI_INT32 RDINTv2_DESFire_GetValue(BYTE u8COMPort, BYTE u8FileNo, int *i32Value);
    RDINTAPI_INT32 RDINTv2_DESFire_Credit(BYTE u8COMPort, BYTE u8FileNo, int i32Value);
    RDINTAPI_INT32 RDINTv2_DESFire_Dedit(BYTE u8COMPort, BYTE u8FileNo, int i32Value);
    RDINTAPI_INT32 RDINTv2_DESFire_LitmitedCredit(BYTE u8COMPort, BYTE u8FileNo, int i32Value);
    RDINTAPI_INT32 RDINTv2_DESFire_WriteRecord(BYTE u8COMPort, BYTE u8FileNo, int i32Offset, int i32Length, LPBYTE pu8Data);
    RDINTAPI_INT32 RDINTv2_DESFire_ReadRecord(BYTE u8COMPort, BYTE u8FileNo, int i32Offset, int i32Length, LPBYTE pu8Data);
    RDINTAPI_INT32 RDINTv2_DESFire_ClearRecordFile(BYTE u8COMPort, BYTE u8FileNo);
    RDINTAPI_INT32 RDINTv2_DESFire_CommitTransaction(BYTE u8COMPort);
    RDINTAPI_INT32 RDINTv2_DESFire_AbortTransaction(BYTE u8COMPort);
    /************************以下為RDINT V1新增演算法未公開函式********************************************/
    RDINTAPI_INT32 KeyFileCopyToKeyBox(VOID);
    RDINTAPI_INT32 KeyFileCheckExpDate(VOID);
    #ifdef __cplusplus
    }
  #endif 

#endif
