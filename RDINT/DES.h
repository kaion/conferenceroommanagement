#ifndef _DES_H
  #define _DES_H

  #ifndef uint8
    #define uint8  unsigned char
  #endif 

  #ifndef uint32
    #define uint32 unsigned long int
  #endif 

  typedef struct
  {
    uint32 esk[32]; /* DES encryption subkeys */
    uint32 dsk[32]; /* DES decryption subkeys */
  } des_context;

  typedef struct
  {
    uint32 esk[96]; /* Triple-DES encryption subkeys */
    uint32 dsk[96]; /* Triple-DES decryption subkeys */
  } des3_context;

  #ifdef __cplusplus
    extern "C"
    {
    #endif 

    int des_set_key(des_context *ctx, uint8 key[8]);
    void des_encrypt(des_context *ctx, uint8 input[8], uint8 output[8]);
    void des_decrypt(des_context *ctx, uint8 input[8], uint8 output[8]);

    int des3_set_2keys(des3_context *ctx, uint8 key1[8], uint8 key2[8]);
    int des3_set_3keys(des3_context *ctx, uint8 key1[8], uint8 key2[8], uint8 key3[8]);

    void des3_encrypt(des3_context *ctx, uint8 input[8], uint8 output[8]);
    void des3_decrypt(des3_context *ctx, uint8 input[8], uint8 output[8]);

    void mifare_cbc_decrypt_all(unsigned char *pu8SessionKey, unsigned char *pu8Data, int i32DataLen);
    void mifare_cbc_decrypt(unsigned char *key, unsigned char *data, unsigned char *ivect, short direction, short type);
    void mifare_cbc_des(unsigned char *key, unsigned char *data, unsigned char *ivect, short direction);
    void mifare_cbc_3des(unsigned char *key, unsigned char *data, unsigned char *ivect, short direction);

    void DES_Enc_EBC(unsigned char pu8Key[8], unsigned char pu8In[8], unsigned char pu8Out[8]);
    void DES_Enc_CBC(unsigned char pu8Key[8], unsigned char pu8IV[8], unsigned char pu8In[8], unsigned char pu8Out[8]);
    void DES_Dec_EBC(unsigned char pu8Key[8], unsigned char pu8In[8], unsigned char pu8Out[8]);
    void DES_Dec_CBC(unsigned char pu8Key[8], unsigned char pu8IV[8], unsigned char pu8In[8], unsigned char pu8Out[8]);

    void DES3_2KeysEnc_EBC(unsigned char pu8Key[16], unsigned char pu8In[8], unsigned char pu8Out[8]);
    void DES3_2KeysEnc_CBC(unsigned char pu8Key[16], unsigned char pu8IV[8], unsigned char pu8In[8], unsigned char pu8Out[8]);

    void DES3_2KeysDec_EBC(unsigned char pu8Key[16], unsigned char pu8In[8], unsigned char pu8Out[8]);
    void DES3_2KeysDec_CBC(unsigned char pu8Key[16], unsigned char pu8IV[8], unsigned char pu8In[8], unsigned char pu8Out[8]);

    #ifdef __cplusplus
    }
  #endif 

#endif /* des.h */
