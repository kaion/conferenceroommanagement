#ifndef RDNDEFRECORD_H
#define RDNDEFRECORD_H

#define NDEFRECORD_FLAG_MB			( 1 << 7 )
#define NDEFRECORD_FLAG_ME			( 1 << 6 )
#define NDEFRECORD_FLAG_CF			( 1 << 5 )
#define NDEFRECORD_FLAG_SR			( 1 << 4 )
#define NDEFRECORD_FLAG_IL			( 1 << 3 )
#define NDEFRECORD_FLAG_TNF( x )	( x & 0x07 )

#ifdef __cplusplus
extern "C"{
#endif

unsigned short NFC_CreateNdefRecordOfApplication( char *strPackageName );
unsigned short NFC_CreateNdefRecordOfExternal( unsigned char *pu8Ndef, unsigned int *pu32NdefLen
											  , char *domain, char *type, unsigned char *p_data, unsigned char data_len );

#ifdef __cplusplus
}
#endif

#endif // #ifndef RDNDEFRECORD_H
