// dllmain.cpp : 定義 DLL 應用程式的進入點。
#include <windows.h>

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
        #ifdef _DEBUG
            AllocConsole();
        #endif 
        break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
        break;
	case DLL_PROCESS_DETACH:
        #ifdef _DEBUG
            FreeConsole();
        #endif 
		break;
	}
	return TRUE;
}

