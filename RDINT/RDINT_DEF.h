#ifndef __RDINT_DEF__
#define __RDINT_DEF__


enum SECUR_MODE
{
	SECUR_MODE_NONE,
	SECUR_MODE_DEFAULT,
	SECUR_MODE_READER_SEL_NONE,
	SECUR_MODE_READER_SEL_ROLLCODE,
	SECUR_MODE_READER_SEL_DES,
	SECUR_MODE_READER_SEL_3DES
};

enum READER_DEF
{
	PASSWD_LEN = 4,
	KB_LEN = 8,
	READER_SERIAL_LEN = 8,
	FIRMWARE_VER_LEN = 4,
	FIRMWARE_VER_FULL_LEN = 7, //20140402.02	Shawn
	DEVICE_CLOSE_ALL = 0
};

enum OPENCARD_DEF
{
	UID_LEN = 11,
	ATQA_LEN = 2, 

	UID_END = 'M'
};

enum SAK
{
	MIFARE_ULTRA_LIGHT = 0x00,
	MIFARE_S50 = 0x08,
	MIFARE_S70 = 0x18,
	MIFARE_DESFIRE = 0x20 
};

enum ATQA
{
	ATQA_MIFAER_S50 = 0x0400,
	ATQA_MIFAER_S70 = 0x0200,
	ATQA_ULTRA_LIGHT = 0x4400
};

enum MODULE_TYPE
{
	MODULE_TYPE_HID125K = 0x00,
	MODULE_TYPE_EMID,
	MODULE_TYPE_HIDICLASS,
	MODULE_TYPE_RC632,
	MODULE_TYPE_RC531,
	MODULE_TYPE_PN544 = 0x80, //20140401.03 增加 module type
	MODULE_TYPE_RC663 = 0x90  //20140401.03 增加 module type
};

enum COMMUCATION_DEF
{
	R_WEI_26 = (0),
	R_WEI_34 = (1),
	R_WEI_EN = (1 << 1),
	R_BAUD_115200 = (0 << 2),
	R_BAUD_9600 = (1 << 2),
	R_BAUD_19200 = (2 << 2),
	R_BAUD_38400 = (3 << 2),
	R_WEI_KEYPAD = (1 << 4)
};

enum WORKING_TYPE
{
	WT_ISO14443_TYPEA = 'A',
	WT_ISO14443_TYPEB,
	WT_ISO15693 = '1',
	WT_ICODE1 = 'I',
	WT_SR176_SRIX4K = 's',
	WT_FELICA = 'F',
	WT_NFC = 'N'
};

enum ULTRA_LIGHT
{
	ULTRA_LIGHT_WRITE_LEN = 4,
	ULTRA_LIGHT_READ_LEN = (ULTRA_LIGHT_WRITE_LEN *4), 
};

enum ISO15693_DFE
{
	ISO15693_UID_LEN = 8,
	ISO15693_MASK_LEN = 8,
	ISO15693_BLOCK_SECRUTY_LEN = 1,
	ISO15693_BLOCK_LEN = 4,
	ISO15693_SYSINF_LEN = 14
};

typedef struct __MULTI_ISO15693_CARD
{
	unsigned char u8DSFID;
	unsigned char pu8Uid[8];
} MULTI_ISO15693_CARD,  *PMULTI_ISO15693_CARD;

typedef struct __MULTI_ISO15693
{
	unsigned char u8CardCount;
	MULTI_ISO15693_CARD pCard[16];
} MULTI_ISO15693,  *PMULTI_ISO15693;

typedef struct __MULTI_MIFARE_CARD
{
	unsigned long u32CardSerial;
} MULTI_MIFARE_CARD,  *PMULTI_MIFARE_CARD;

typedef struct __MULTI_MIFARE
{
	unsigned char u8CardCount;
	MULTI_MIFARE_CARD pCard[16];
} MULTI_MIFARE,  *PMULTI_MIFARE;

enum SR176_DEF
{
	SR176_BLOCK_LEN = 2
};

enum SRIX4K_DEF
{
	SRIX4K_BLOCK_LEN = 4,
	SRIX4K_RND_LEN = 8,
	SRIX4K_UID_LEN = 8
};

enum ISO14443B_FIND_TYPE
{
	FT_REQB, FT_WUPB
};

enum DEVICE_CONTROL
{
	LED_OFF = 0x00,
	LED_ON = 0x01,
	LED_FLASH = 0x02,
	LED_ON_OFF = 0x03, 

	RED_OFF = (LED_OFF << 0x00),
	RED_ON = (LED_ON << 0x00),
	RED_FLASH = (LED_FLASH << 0x00), 

	GREEN_OFF = (LED_OFF << 0x02),
	GREEN_ON = (LED_ON << 0x02),
	GREEN_FLASH = (LED_FLASH << 0x02), 

	YELLOW_OFF = (LED_OFF << 0x04),
	YELLOW_ON = (LED_ON << 0x04),
	YELLOW_FLASH = (LED_FLASH << 0x04), 

	SOUND_OFF = (LED_OFF << 0x06),
	SOUND_ON = (LED_ON << 0x06),
	SOUND_FLASH = (LED_FLASH << 0x06)
};

enum TURN_ON_OFF
{
	TURN_OFF = 0,  //Turn Off
	TURN_ON  //Turn On
};

enum ANTENNA_SELECT
{
	ANTENNA_SELECT_OFF = 0,
	ANTENNA_SELECT_ON = (1 << 0),
	ANTENNA_SELECT_AUTO_POWER_LOW = (1 << 3),
	ANTENNA_SELECT_POWER_LOW = (1 << 4)
};

enum ANTENNA_SWITCH
{
	ANTENNA_CLOSE_ALL = 0,  //Antenna All Close
	ANTENNA1 = (1 << 0),    //Antenna 1
	ANTENNA2 = (1 << 1),    //Antenna 2
	ANTENNA3 = (1 << 2),    //Antenna 3
	ANTENNA4 = (1 << 3)     //Antenna 4
};

enum DESFIRE_SECURITY
{
	DESFIRE_SECURITY_OFF = 0, DESFIRE_SECURITY_DES = 1, DESFIRE_SECURITY_3DES = 2, 
};

enum LCD_CLEAR
{
	LCD_ROW_1 = 0x01,   //LCD 第一行
	LCD_ROW_2 = 0x02,   //LCD 第二行
	LCD_ROW_3 = 0x04,   //LCD 第三行
	LCD_ROW_4 = 0x08,   //LCD 第四行
	LCD_ROW_ALL = 0x00  //LCD 全部
};

//RAPICOMM.INI
#define RAPICOMM_FILE_NAME				_T("RAPICOMM.INI")
#define RAPICOMM_COMM						_T("COMM")
#define RAPICOMM_READERCOM				_T("ReaderCOM")
#define RAPICOMM_READERBAUD				_T("ReaderBaudrate")
#define RAPICOMM_SECURE_MODE				_T("SecureMode")

#define RAPICOMM_DEFAULT_READERCOM		_T("1")
#define RAPICOMM_DEFAULT_READERBAUD		_T("38400")
#define RAPICOMM_DEFAULT_SECURE_MODE		_T("Yes")
#define RAPICOMM_DEFAULT_READER_MODEL		_T("1")


#define RAPICOMM_SYSTEM					_T("SYSTEM")
#define RAPICOMM_KEY_SET					_T("KeySet")
#define RAPICOMM_KEY_FORMAT				_T("KeyFormat")

#define RAPICOMM_DEFAULT_KEY_SET			_T("1")
#define RAPICOMM_DEFAULT_KEY_FORMAT		_T("FF")


#define RAPICOMM_LOGGING					_T("LOGGING")
#define RAPICOMM_LOG_TO_FILE_ON			_T("LogToFileOn")
#define RAPICOMM_LOG_FILE_NAME			_T("LogFileName")
#define RAPICOMM_LOG_LEVEL				_T("LogLevel")

#define RAPICOMM_DEFAULT_LOG_TO_FILE_ON	_T("No")
#define RAPICOMM_DEFAULT_LOG_FILE_NAME	_T("LOGFILE.TXT")
#define RAPICOMM_DEFAULT_LOG_LEVEL		_T("1")


enum RDINT_LRESULT
{
	LRSUCCESS = 0x00,  //Successful completion of request
	LRSYSTEM = 0x01,  //Unknown error
	LRLASTCARD = 0x02,  //Last Card Still Present
	LRNOCARD = 0x03,  //Card is not present
	LRCTYPE = 0x04,  //Card Type error
	LRPARAM = 0x05,  //Request Parameter error
	LRACCESS = 0x06,  //Card access error
	LRREAD = 0x07,  //Card read error
	LRWRITE = 0x08,  //Card write error
	LRINCR = 0x09,  //Purse increment error
	LRDECR = 0x0A,  //Purse decrement error
	LRTRANSFER = 0x0B,  //Purse value transfer error
	LRRESTORE = 0x0C,  //Purse restore error
	LRPURSE = 0x0D,  //Purse value corrupt
	LRMADERR = 0x0E,  //Card Directory error'
	LRFIXERR = 0x0F,  //Purse fix error
	LRFIXED = 0x10,  //Purse found corrupt but fixed
	LRNOTOPEN = 0x11,  //Card not open
	LRNOFILE = 0x12,  //File not found
	LRBADSIZE = 0x13,  //Bad file size
	LRABORTED = 0x14,  //Request aborted
	LRMANYCARD = 0x15,  //Too many card present
	LRFORMAT = 0x16,  //Card format error
	LRCREATE = 0x17,  //Card file create error
	LRDELETE = 0x18,  //Card file delete error
	LRALREADOPEN = 0x19,  //Card has been opened already
	LRALREADCLOSED = 0x1A,  //Card has been closed already
	LRMSTRKEYLOAD = 0x1B,  //Cannot load master keys
	LRAPPKEYLOAD = 0x1C,  //Cannot load application keys
	LRKEYCARD = 0x1D,  //Keycard Error
	LRUNFORMAT = 0x1E,  //Card has files on it
	LRNOKBDCHAR = 0x20,  //No keyboard character
	LRAPIWRITE = 0x21,  //API data write error
	LRAPIREAD = 0x22,  //API data read error
	LRBLOCKERROR = 0x23,  //Block number error or Continuous processing block fail
	LRWORKTYPEERROR = 0x24,  //Work Type error
	LRDEVICEMODEERROR = 0x25,  //Device Mode error
	LRFELICACMDERROR = 0x26,  //Felica command error
	LRNOTIMPL = 0x7F,  //Function not implemented
	LRUNKNOWN = 0x80,  //Unknown error
	LRCCRBUSY = 0xBB,  //Reader is busy
	LRCHANGEWROKTYPE = 0xEE,  //Change Reader working type error
	LRRDUNKNOWN = 0xEF,  //Reader Unknown error
	LRCRDNOTOPEN = 0xFA,  //Card has not been opened
	LRINUSE = 0xFB,  //Card in use by another applications
	LRAPPLICERR = 0xFC,  //API system error
	LRRDLINKLOST = 0xFD,  //Link to Reader has been lost
	LRBADCOMPORT = 0xFE,  //COM port cannot be accessed
	LRRDNOINIT = 0xFF,  //Reader has not been opened
	LRNOCRYPTBOX = 0xF9,  //Key-Box not found
	LRBADAPPACCESS = 0XF8,  //Invalid Application access code
	LRBADRDACCESS = 0xF7,  //Invalid Reader access code
	LRNOMAIDFILE = 0xF6,  //Cannot open MAID definition file
	LRBOXREAD = 0xF5,  //Cannot read from Key-Box
	LRBOXWRITE = 0xF4,  //Cannot write to Key-Box
	LRBOXNOKEYS = 0xF3,  //No of Keys in Box is zero or invalid
	LRSECURE = 0xF2,  //Comms MAC checking failed
	LRERRSELREADER = 0xF1,  //Cannot change to selected reader
	LRRDNORESP = 0xF0,  //Reader No Response
	LRSAMUNKNOWN = 0xED,  //SAM card unknown error
	//20140731
	LRRDBUFFERNODATA = 0xE0,	// Reader buffer not find event data
	LRMODULENOSUPPORT = 0xE1,	// Module no support command
	LRPARAMETEROVERRANGE = 0xE2	// parameter over range.
};


enum MIFARE_ONE_DEF
{
	CARD_KEY_LEN = 6,
	BLOCK_LEN = 0x10,
	SECTOR_LEN = 0x40, 

	KEY_TOTAL = 16,
	KEY_S50_TOTAL = 16,
	KEY_S70_TOTAL = 24
};

enum CARD_KEY_TYPE
{
	CARD_KEY_A = 0,  //回傳Key A
	CARD_KEY_B,      //回傳Key B
	CARD_KEY_NONE_OR_FAIL = 3,  //回傳失敗 或 不使用Key Type
};

enum COMMON_KEY
{
	COMMON_KEY_FF,  //FF Key
	COMMON_KEY_AB   //AB Key
};

enum MAID_USE
{
	MAID_USE_KEY_INTERNAL,
	MAID_USE_KEY_CUSTMER,
	MAID_INTERNAL_TO_SECTOR,
	MAID_USE_KEY_SAM,
	MAID_USE_KEY_SAM_TO_SECTOR,
	MAID_USE_KEY_SAM_TO_SECTOR_AC
};

enum ISSUE
{
	ISSUE_FORMAT_CARD = 0x01,
	ISSUE_CREATE_FILES = 0x02,
	ISSUE_WRITE_DATA = 0x04
};

enum MAID_TYPE
{
	MAID_TYPE_A,  //一般用途
	MAID_TYPE_P   //電子票證
};

typedef struct __MAID
{
	unsigned char u8Code;
	unsigned char u8Cluster;
} MAID,  *PMAID;

typedef struct __GMAID
{
	MAID Aid;
	unsigned short u16Gid;
} GMAID,  *PGMAID;

typedef struct __SAMAIDINF
{
	unsigned char u8AidLen;
	unsigned char Aid[10];
	unsigned char u8LCS;     // u8LCS ;Life Cycle States
	unsigned char u8AppPrvl; // u8AppPrvl ;Application Privileges
} SAMAIDINF,  *PSAMAIDINF;

typedef struct __FSDIR
{
	MAID Aid[16];
	unsigned char FileLength;
	unsigned char FileAccess;
} FSDIR,  *PFSDIR;

#define SECTOR0_MAID	(0x0003L)
#define NONE_MAID		(0x0000L)
#define NONE_MAID_FF	(0xFFFFL)
#define KEY_CARD_PASSWD_LEN	(8)


typedef struct __ISSUESTRC
{
	unsigned short AID;
	unsigned char Attrib;
	unsigned char BlkTotal;
	unsigned char InitData[96];
} ISSUESTRC,  *PISSUESTRC;

typedef struct __GETKEY
{
	unsigned char KeyType;
	unsigned char Key[6];
	unsigned char KeyA[6]; //唯讀或減值
	unsigned char AccessCondition[4];
	unsigned char KeyB[6]; //寫入或加值
} GETKEY,  *PGETKEY;


/************************ 以下為 DESFire 新增定義 *********************************/
#define NOT_YET_AUTHENTICATED 255

typedef enum
{
	MD_RECEIVE = 0,
	MD_SEND = 1
} MifareDirection;

typedef struct _desfire_Version
{
	union
	{
		struct _frame1
		{
			unsigned char u8VendorID;
			unsigned char u8Type;
			unsigned char u8Subtype;
			unsigned char u8MajorVer;
			unsigned char u8MinorVer;
			unsigned char u8StorageSize;
			unsigned char u8Protocol;
		} frame1;
		unsigned char uf1[7];
	}
	union_frame1;

	union
	{
		struct _frame2
		{
			unsigned char u8VendorID;
			unsigned char u8Type;
			unsigned char u8Subtype;
			unsigned char u8MajorVer;
			unsigned char u8MinorVer;
			unsigned char u8StorageSize;
			unsigned char u8Protocol;
		} frame2;
		unsigned char uf2[7];
	}
	union_frame2;

	union
	{
		struct _frame3
		{
			unsigned char u8UID[7];
			unsigned char u8BatchNo[5];
			unsigned char u8CalendarWeekProd;
			unsigned char u8YearProd;
		} frame3;
		unsigned char uf3[14];
	}
	union_frame3;
}
desfire_Version,  *Pdesfire_Version;

typedef struct _desfire_FileSetting
{
	union
	{
		struct _Snd_Bkp_Data_Files
		{
			unsigned char u8FileType;
			unsigned char u8ComSett;
			unsigned char u8AccessRights[2];
			unsigned char u8FileSize[3];
		} Snd_Bkp_Data_Files;

		struct _Value_Files
		{
			unsigned char u8FileType;
			unsigned char u8ComSett;
			unsigned char u8AccessRights[2];
			unsigned char u8LowerLimit[4];
			unsigned char u8UpperLimit[4];
			unsigned char u8LmtCreditValue[4];
			unsigned char u8LmtCreditEnab;
		} Value_Files;

		struct _Linear_Cyclic_Record_Files
		{
			unsigned char u8FileType;
			unsigned char u8ComSett;
			unsigned char u8AccessRights[2];
			unsigned char u8RecordSize[3];
			unsigned char u8MaxNumberOfRecords[3];
			unsigned char u8CurrentNumberOfRecords[3];
		} Linear_Cyclic_Record_Files;

		unsigned char FS[17];
	}
	union_FileSetting;

	unsigned char FileSettingType;

}
desfire_FileSetting,  *Pdesfire_FileSetting;


#define RECORD_BEGIN				( 0x01 )
#define RECORD_MID					( 0x02 )
#define RECORD_END					( 0x04 )

#define TNF_EMPTY					( 0x00 )
#define	TNF_WELL_KNOWN				( 0x01 )
#define	TNF_MIME_MEDIA				( 0x02 )
#define TNF_ABSOLUTE_URI			( 0x03 )
#define TNF_EXTERNAL_TYPE			( 0x04 )
#define	TNF_UNKNOWN					( 0x05 )
#define	TNF_UNCHANGED				( 0x06 )
#define	TNF_RESERVED				( 0x07 )

#endif
